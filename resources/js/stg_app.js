var App = angular.module('App', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'angular-jquery-validate', 'angularUtils.directives.dirPagination']);

App.baseUrl = function(url) {return '//qa.addfow.com/site/' + url}

App.config(function($jqueryValidateProvider, $routeProvider, $httpProvider) {
					
	$httpProvider.defaults.cache = false;
	
	if (!$httpProvider.defaults.headers.get) {
		$httpProvider.defaults.headers.get = {};
	}
	$httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    
    $routeProvider.when('/', {
        controller: 'UsersController',
        templateUrl: 'resources/views/login.html'
    }).when('/reseller/:resellerId/:brochureId', {
        controller: 'UsersController',
        templateUrl: 'resources/views/login.html'
    }).when('/invite/:brochureId', {
        controller: 'UsersController',
        templateUrl: 'resources/views/login.html'
    }).when('/coachmark', {
        controller: 'CoachMarkController',
        templateUrl: 'resources/views/coachmark.html'
    }).when('/brochures', {
        controller: 'BrochuresController',
        templateUrl: 'resources/views/brochures.html'
    }).when('/brochure/:brochureId', {
        controller: 'BrochureController',
        templateUrl: 'resources/views/brochure.html'
    }).when('/send-brochure', {
        controller: 'SendBrochuresController',
        templateUrl: 'resources/views/send-brochure.html'
    }).when('/cart', {
        controller: 'CartController',
        templateUrl: 'resources/views/cart.html'
    }).when('/orders-by-customers', {
        controller: 'OrdersByCustomersController',
        templateUrl: 'resources/views/orders-by-customers.html'
    }).when('/orders-by-products', {
        controller: 'OrdersByProductsController',
        templateUrl: 'resources/views/orders-by-products.html'
    }).when('/print-orders-by-customers/:parchuseDate', {
        controller: 'PrintOrdersByCustomersController',
        templateUrl: 'resources/views/print-orders-by-customers.html'
    }).when('/print-orders-by-products/:parchuseDate', {
        controller: 'PrintOrdersByProductsController',
        templateUrl: 'resources/views/print-orders-by-products.html'
    }).when('/order-history', {
        controller: 'OrderHistoryController',
        templateUrl: 'resources/views/order-history.html'
    }).when('/client-history', {
        controller: 'ClientHistoryController',
        templateUrl: 'resources/views/client-history.html'
    }).when('/order-detail/:orderDate', {
        controller: 'OrderDetailController',
        templateUrl: 'resources/views/order-detail.html'
    }).otherwise({
        redirectTo: '/'
    });

    $jqueryValidateProvider.setDefaults({
        ignore: ':hidden:not(.form-control)',
        errorElement: 'span',
        errorClass: 'help-block has-error',
        errorPlacement: function(error, element) {
            if (element.parents("label").length > 0) {
                element.parents("label").after(error);
            } else {
                element.after(error);
            }
        },
        highlight: function(label) {
            $(label).closest('.form-group').removeClass('has-error has-success').addClass('has-error');
        },
        success: function(label) {
            label.addClass('valid').closest('.form-group').removeClass('has-error has-success').addClass('has-success');
        },
        onkeyup: function(element) {
            $(element).valid();
        },
        onfocusout: function(element) {
            $(element).valid();
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });

});

App.controller('UsersController', function($rootScope, $scope, $location, $routeParams, $http, $cookies) {

	$scope.init = function() {

		$.vegas({src:App.baseUrl('../resources/img/1.jpg')});

		window.fbAsyncInit = function() {
			FB.init({
		      	appId      : '338346469695953',
		      	status     : true,
		      	xfbml      : true,
		      	version    : 'v2.1'
			});
			
			FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					$scope.getlogin();
				}
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/es_LA/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}

	$scope.login = function() {
		
		//if($('#chkTerms:checked').length == 0){
		//	alert('Debe aceptar los terminos y condiciones.');
		//	return false;
		//}
		
		FB.login(function(response) {
			if (response.authResponse) {
				FB.api('/me?fields=first_name, last_name, picture, email', function(user) {
					if (user) {
						FB.api('/me/picture', function(response) {
		        			if (response && response.data) {
			        			data = new Object();
								if ($routeParams.resellerId) {
									data.UserResellerGroup = new Object();
									data.UserResellerGroup.reseller_user_id = $routeParams.resellerId;
									data.UserClientBrochure = new Object();
									data.UserClientBrochure.shop_brochure_id = $routeParams.brochureId;
								}
								data.User = user;
								data.User.picture = response.data.url;
								$http({
									url: App.baseUrl('users/login'),
									method: 'POST',
									data:  $.param({'data':data}),
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								}).success(function(data, status, headers, config) {
									if (data.login) {
										angular.forEach($cookies, function (k, v) {
										    delete $cookies[v];
										});
										if ($routeParams.brochureId) {
											$cookies.brochureReceived 	= $routeParams.brochureId;
										}
										$cookies.login 					= true;
										$cookies.firstName 				= data.User.first_name;
										$cookies.lastName 				= data.User.last_name;
										$cookies.picture 				= data.User.picture;
										$cookies.userTypeid 			= data.User.user_type_id;
										$cookies.userId 				= data.User.id;
										if (data.UserCart) {
											$cookies.cartTotalQuantity 	= data.UserCart.total_quantity;
										} else {
											$cookies.cartTotalQuantity 	= 0;
										}
										$cookies.ordersByCustomers		= data.User.OrdersByCustomers;
										$cookies.ordersByProducts		= data.User.OrdersByProducts;
										$rootScope.User 				= $cookies;
										$scope.User 					= $cookies;
										
										$location.path(data.created ? '/coachmark' : '/brochures');
									}
								}).error(function(data, status, headers, config) {
									
								});
		        			}
							
						});
					}
				});
			} else {
				bootbox.dialog({
                    title: '¡Atención!',
                    message: 'El usuario canceló el inicio de sesión o no autorizó la aplicación.',
                    buttons: {
                        success: {
                            label: 'Aceptar',
                            className: 'btn-danger'
                        }
                    }
                });
			}
		},{scope:'email, user_friends, public_profile'});

		FB.getLoginStatus(function(response) {
			if (!response.status === 'connected') {
				FB.login();
			} 
		});

		$scope.$on('$destroy', function(e) {
	        $.vegas('destroy', 'background');
	    });
		
	}
	
	$scope.getlogin = function() {
				FB.api('/me', function(user) {
					if (user) {
						FB.api('/me/picture', function(response) {
		        			if (response && response.data) {
			        			data = new Object();
								if ($routeParams.resellerId) {
									data.UserResellerGroup = new Object();
									data.UserResellerGroup.reseller_user_id = $routeParams.resellerId;
									data.UserClientBrochure = new Object();
									data.UserClientBrochure.shop_brochure_id = $routeParams.brochureId;
								}
								data.User = user;
								data.User.picture = response.data.url;
								$http({
									url: App.baseUrl('users/login'),
									method: 'POST',
									data:  $.param({'data':data}),
									headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								}).success(function(data, status, headers, config) {
									if (data.login) {
										angular.forEach($cookies, function (k, v) {
										    delete $cookies[v];
										});
										if ($routeParams.brochureId) {
											$cookies.brochureReceived 	= $routeParams.brochureId;
										}
										$cookies.login 					= true;
										$cookies.firstName 				= data.User.first_name;
										$cookies.lastName 				= data.User.last_name;
										$cookies.picture 				= data.User.picture;
										$cookies.userTypeid 			= data.User.user_type_id;
										$cookies.userId 				= data.User.id;
										if (data.UserCart) {
											$cookies.cartTotalQuantity 	= data.UserCart.total_quantity;
										} else {
											$cookies.cartTotalQuantity 	= 0;
										}
										$cookies.ordersByCustomers		= data.User.OrdersByCustomers;
										$cookies.ordersByProducts		= data.User.OrdersByProducts;
										$rootScope.User 				= $cookies;
										$scope.User 					= $cookies;
										
										$location.path(data.created ? '/coachmark' : '/brochures');
									}
								}).error(function(data, status, headers, config) {
									
								});
		        			}
							
						});
					}
				});
				$scope.$on('$destroy', function(e) {
	        $.vegas('destroy', 'background');
	    });					   
	}
		

	$scope.logout = function() {
		$http({
			url: App.baseUrl('users/logout'),
			method: 'POST',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data.session === false) {
				angular.forEach($cookies, function (k, v) {
				    delete $cookies[v];
				});
				$location.path('/');
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

});

App.controller('CoachMarkController', function($rootScope, $scope, $location, $http, $compile) {

	$('body').css({
		'background':'rgba(0,0,0,0.9)',
		'padding-top': '0',
	});

	var steps = [];
	var stepCero = [];
	var stepOne = [];
	var stepTwo = [];
	var stepThree = [];
	var stepFour = [];

	var buttonComenzar = {
		'name' : 'Button Comenzar',
		'shape': '1',
		'w': '243.5708692247455',
		'h': '57.12624584717608',
		'x': '516.2098668754894',
		'y': '517.1428571428571',
	};
	var circleOne = {
			'name' : 'Circle One',
			'shape': '2',
			'w': '47.110415035238844',
			'h': '49.10852713178294',
			'x': '552.2944400939703',
			'y': '263.58250276854926',
	};
	var circleTwo = {
			'name' : 'Circle Two',
			'shape': '2',
			'w': '49.115113547376666',
			'h': '49.10852713178294',
			'x': '611.433046202036',
			'y': '262.5802879291251',
	};
	var circleThree	= {
			'name' : 'Circle Three',
			'shape': '2',
			'w': '46.108065779169934',
			'h': '45.09966777408638',
			'x': '676.5857478465153',
			'y': '266.5891472868217',
	};
	var buttonEnviar = {
		'name' : 'Button Enviar Pedido',
		'shape': '1',
		'w': '169.39702427564606',
		'h': '36.0797342192691',
		'x': '934.1895066562256',
		'y': '27.059800664451824',
	};
	var buttonCatalogo = {
		'name' : 'Button Ver Catálogo',
		'shape': '1',
		'w': '244.57321848081443',
		'h': '58.12846068660022',
		'x': '516.2098668754894',
		'y': '517.1428571428571',
	};

	stepCero.push(buttonComenzar);
	stepOne.push(circleOne, circleTwo, circleThree);
	stepTwo.push(circleOne, circleTwo, circleThree);
	stepThree.push(circleOne, circleTwo, circleThree, buttonEnviar);
	stepFour.push(buttonCatalogo);
	steps.push(stepCero, stepOne, stepTwo, stepThree, stepFour);
	
	var winH = $(window).height();
	var catalogWidth = ((winH)*1170)/806;
	$('#colcat').css('width',catalogWidth);
	
	$(window).resize(function() {
		var winH = $(window).height();
		var catalogWidth = ((winH)*1170)/806;
		$('#colcat').css('width',catalogWidth);
	});

	$scope.step = function(n) {
		
		if(n == 1){
			$('.glyphicon-chevron-right').css('display', 'none');
			$('.glyphicon-chevron-left').css('display', 'none');
		} else {
			$('.glyphicon-chevron-right').css('display', '');
			$('.glyphicon-chevron-left').css('display', '');
		}

		if (n === 5) $location.path('/brochures');

		$('#coachmark .coachmark').remove();

		var image = new Image();
		image.src = App.baseUrl('../resources/img/coachmark-' + n + '.jpg');

		$(image).load(function() {
			var element = steps[n];
			$(element).each(function(k, v) {
				rw = 1280 / $('#coachmark .item.active img').width();
			    rh = 905 / $('#coachmark .item.active img').height();
			    w = v.w / rw;
			    h = v.h / rh;
			    x = v.x / rw;
			    y = v.y / rh;
			    var link = $('<a class="coachmark" href="javascript:;"></a>').css({
			    	'position': 'absolute',
			        'width': 	w + 'px',
			        'height': 	h + 'px',
			        'left': 	x + 'px',
			        'top': 		y + 'px',
			    });
			    if (v.shape == 2) {
			        link.css({
			            'border-radius': '50%'
			        });
			    }
				if(n==1 || n==2||n==3){
					link.attr({
						'ng-click':'slide('+(k+1)+')',
						'data-name': v.name,
					});
				} else{
					link.attr({
						'ng-click':'slide('+(n+1)+')',
						'data-name': v.name,
					});

				}
			    $('#carousel .item.active').append($compile(link)($scope));
			});
		});
	}

	$scope.slide = function(n) {
		if (n === 5) $location.path('/brochures');
		$('#coachmark .item.active').removeClass('active');
		$('#coachmark .item[data-step="'+n+'"]').addClass('active');
		$scope.step(n);
	}

	$('#carousel').on('slid.bs.carousel', function (e) {
		var $this = $(this);
		if($('.carousel-inner .item:first').hasClass('active')) {
			$this.children('.left.carousel-control').hide();
		} else if($('.carousel-inner .item:last').hasClass('active')) {
			$this.children('.right.carousel-control').hide();
		} else {
			$this.children('.carousel-control').show();
		}
		var n = $('#carousel .item.active').data('step');
		if (n === 5) $location.path('/brochures'); 
		$scope.step(n);
	});

});

App.controller('NavController', function($rootScope, $scope, $log, $location, $http, $cookies) {
	
	$scope.status = {
		isopen: false
	}

	$scope.toggleDropdown = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.status.isopen = !$scope.status.isopen;
	}
	
	$http({
		url: App.baseUrl('users/get_orders_values'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
		var products = 0, customers = 0;
		if(data.User){
			products = data.User.OrdersByProducts;
			customers = data.User.OrdersByCustomers;
		}
		$cookies.ordersByProducts = products;
		$cookies.ordersByCustomers = customers;
		
		$rootScope.User = $cookies;
		$scope.User 	= $cookies;
		
	}).error(function(data, status, headers, config) {
		if(status == 401){
			$location.path('/');
		}
	});
	
	$scope.menuShow = function($event) {
		if($('#btnmob').css('display')=='block'){
			$('#menu-sl').show();
			$('#menu-lg').hide();
		} else {
			$('#menu-sl').hide();
			$('#menu-lg').show();
		}
	}
	$scope.menuShow();
	$(window).resize(function() {
		$scope.menuShow();
	});
});

App.controller('BrochuresController', function($rootScope, $scope, $location, $http, $cookies, $compile) {

	if ($cookies.brochureReceived) {
		var brochureId = $cookies.brochureReceived;
		delete $cookies.brochureReceived;
		return $location.path('/brochure/' + brochureId);
	}

	$('body').css({'background':'#ffffff','padding-top': '84px',});
	
	$.datepicker.regional['es'] = {
	 closeText: 'Cerrar',
	 prevText: '<Ant',
	 nextText: 'Sig>',
	 currentText: 'Hoy',
	 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	 weekHeader: 'Sm',
	 dateFormat: 'dd/mm/yy',
	 firstDay: 1,
	 isRTL: false,
	 showMonthAfterYear: false,
	 yearSuffix: ''
	 };
	 $.datepicker.setDefaults($.datepicker.regional['es']);
	
	$rootScope.User = $cookies;
	$scope.User 	= $cookies;
	
	$scope.ListCampaigns = null;
	
	$scope.validateCampaign = function(index) {
		
		$brochure = $scope.ListCampaigns[index];
		
		bootbox.dialog({closeButton: false,
	                    title: '',
	                    message: '<div id="new-brochure-content"><h1>¡Hay un nuevo catalogo disponible!</h1><div><div id="photo"><img ng-src="'+$brochure.brochure_img+'" src="'+$brochure.brochure_img+'"></div><div id="detail"><h1 class="title">'+$brochure.brochure_name+'</h1><b>'+$brochure.campaign_name+'</b><hr><p>Antes de poder visualizar el nuevo catálogo, debes asignar una fecha de vigencia del mismo para tus clientes. Esta fecha te ayudará a establecer un tiempo válido para las compras.</p><input type="text" id="expirationDate" class="calendarInput" readonly placeholder="dd/mm/yyyy" /><input type="hidden" id="brochure_id" value="'+$brochure.brochure_id+'"><input type="hidden" id="brochure_expdate" value="'+$brochure.expiration_date+'"><span class="popup-msg-error">La fecha debe ser menor o igual a '+$brochure.expiration_date+'.</span></div></div></div><script></script>',
	                    buttons: {
							cancel: {
	                            label: 'Cancelar',
	                            className: 'btn-cancel',
							    callback: function() {
									for(var i=0; i<$scope.Campaigns.length;i++){
										var campaignShopBrochures = $.map($scope.Campaigns[i].CampaignShopBrochure, function(value, index) {
											return [value];
										});
										if(campaignShopBrochures[0].ShopBrochure.id == $scope.ListCampaigns[index].brochure_id){
											$scope.Campaigns.splice(i, 1);
											$scope.$apply();
											break;
										}
									}
									
									index++;
									if(index>=$scope.ListCampaignsCount){
									  $('#campaignsdiv').show();
									} else {
									  $scope.validateCampaign(index);
									}
							    }
	                        },
	                        accept: {
	                            label: 'Aceptar',
	                            className: 'btn-brochure-accept',
							    callback: function() {
									$('#expirationDate').removeClass('input-error');
									if($('#expirationDate').val() == ''){
										$('#expirationDate').addClass('input-error');
										return false;
									}
									
									var expDateArr = $("#expirationDate").val().split("/");
									var expDate = new Date(expDateArr[2], expDateArr[1] - 1, expDateArr[0]);
									
									var bExpDateArr = $("#brochure_expdate").val().split("/");
									var bExpDate = new Date(bExpDateArr[2], bExpDateArr[1] - 1, bExpDateArr[0]);
									
									if(expDate > bExpDate){
										$('#expirationDate').addClass('input-error');
										$('.popup-msg-error').css('display','block');
										return false;
									}
									
									data = new Object();
									data.UserShopBrochure = new Object();
									data.UserShopBrochure.shop_brochure_id = $('#brochure_id').val();
									data.UserShopBrochure.expiration_date = $('#expirationDate').val();
									
									$http({
										url: App.baseUrl('users/addshopbrochure_expdate'),
										method: 'POST',
										data:  $.param({'data':data}),
										headers: {'Content-Type': 'application/x-www-form-urlencoded'},
									}).success(function(data, status, headers, config) {
										if(data.result){
											
											index++;
											if(index>=$scope.ListCampaignsCount){
											  $('#campaignsdiv').show();
											} else {
											  $scope.validateCampaign(index);
											}
											
										} else {
											return false;
										}
										
									}).error(function(data, status, headers, config) {
										return false;
									});
							    }
	                        }
	                    }
	                }).find("div.modal-dialog").addClass("popup-new-brochure");
		
		$(".calendarInput").datepicker({showOtherMonths: true,selectOtherMonths: true,prevText: "",nextText: ""});
	}

	$http({
		url: App.baseUrl('campaigns'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(result, status, headers, config) {

		var data = $.map(result, function(value, index) {
			return [value];
		});
		$scope.Campaigns = data;
		
		$('body').css({'background':'#ffffff','padding-top': '84px',});
		
		var brochures = [];
		
		if($cookies.userTypeid == 1){
			
			for(var i=0;i<data.length;i++){
				
				var campaignShopBrochures = $.map(data[i].CampaignShopBrochure, function(value, index) {
					return [value];
				});
				
				for(var j=0;j<campaignShopBrochures.length;j++){
					if(!campaignShopBrochures[j].ShopBrochureUser){
						var obj = new Object();
						obj.brochure_id = campaignShopBrochures[j].ShopBrochure.id;
						obj.expiration_date = campaignShopBrochures[j].ShopBrochure.expiration_date_es;
						obj.brochure_name = campaignShopBrochures[j].ShopBrochure.name;
						obj.brochure_img = campaignShopBrochures[j].ShopBrochureImage.name;
						obj.campaign_name = data[i].Campaign.name + ' ' + data[i].Campaign.start_date;
						brochures.push(obj);
					}
				}
			}
		}
		
		if(brochures.length == 0){
			$('#campaignsdiv').show();
		} else {
			$scope.ListCampaigns = brochures;
			$scope.ListCampaignsCount = brochures.length;
			
			$scope.validateCampaign(0);
		}
		
	}).error(function(data, status, headers, config) {
		if(status == 401){
			$location.path('/');
		}
	});

});

App.controller('BrochureController', function($rootScope, $scope, $location, $http, $cookies, $routeParams, $timeout, $compile) {

	$rootScope.User = $cookies;
	$scope.User 	= $cookies;
	$('body').css({'background':'#fff',	'padding-top': '84px'});

	$http({
		url: App.baseUrl('shop_brochures/read/' + $routeParams.brochureId),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
		$scope.Brochure = data;
		$scope.CountPages = (Object.keys(data.ShopBrochureImage).length - 1) * 2;
		$scope.isFirefox = navigator.userAgent.indexOf('Firefox') != -1;
		$scope.isChrome = navigator.userAgent.indexOf("Chrome") != -1; 
		
		var winH = $(window).height();
		if(winH < 630){
			$('.col-md-9 h1').css('font-size','10px');
			$('.col-md-9 h2').css('font-size','10px');
			$('.col-md-9 a').css('font-size','10px');
			$('.pagination').css('font-size','10px');
			$('.tools').css('margin-top','40%');
			$('body').css('padding-top','74px');
		} else {
			$('.col-md-9 h1').css('font-size','14px');
			$('.col-md-9 h2').css('font-size','14px');
			$('.col-md-9 a').css('font-size','14px');
			$('.pagination').css('font-size','14px');
			$('.tools').css('margin-top','180%');
			$('body').css('padding-top','84px');
		}
		var catalogWidth = ((winH)*1138)/775;
		$('#colcat').css('width',catalogWidth);
		$('#colcat').css('display', 'block');
		
		$timeout(function() {
			
			resizePagination();
			
			var activeHeight = (parseFloat($('#carousel .item.active').css('width'))*696)/960;
			$('#carousel').css('max-height',activeHeight);
			
			_productsMarkedArea();
			
			$('.carousel-indicators li').sortable({
                cursor: 'move',
                connectWith: '.carousel-indicators li',
                containment: '.carousel-indicators',
                distance: 0,
                revert: false,
                update: function(event, ui) {
					
					$scope.initialZoom = 1;
					$('#carousel .item').css({'transform':'','top':'','left':''});
					
                    $(ui.item[0]).parent().trigger('click');
                	$('.pag-n').remove();
                	var page = ($(ui.item[0]).parent()[0].dataset.page) * 2;
                	page = (page==2?'':(page - 2) + '-') + (page-1);
                	$(ui.item[0]).append('<span class="pag-n">P&aacute;gina '+ page +'</span>');
					
					if(!$scope.isChrome && !$scope.isFirefox){
						$('.pag-n').css('margin-left', '990px');
					}
					
                },
                over: function( event, ui ) {
                },
                start: function( event, ui ) {
					if($scope.isFirefox){
						$('.pag-n').remove();
					}
                }
            });
			
			$('.carousel-indicators li').click(function(){
					
					$scope.initialZoom = 1;
					$('#carousel .item').css({'transform':'','top':'','left':''});
					
					$('.pag-n').remove();
					var li = $('.pag').parent();
					var html = li.html();
					li.html('');
					$(this).html(html);
					
					var n = $(this).data('page');
					var pages = (n==1 ? '' : (n*2-2) + '-') + (n*2-1);
					$(this).append('<span class="pag-n">P&aacute;gina '+ pages +'</span>');
					
					if(!$scope.isChrome && !$scope.isFirefox){
						$('.pag-n').css('margin-left', '990px');
					}
			});
			
			$('#carousel .item').draggable({
				cursor: 'crosshair',
			});
			
		}, 500);
	}).error(function(data, status, headers, config) {
		
	});

	resizePagination = function() {
		$('.carousel-indicators').width('');
		var tw = $('.carousel-indicators').width();
		var c  = $('.carousel-indicators li').length;
		var uw = parseInt(tw / c); 
		var nw = uw * c; 
		$('.carousel-indicators li').width(uw);
		$('.carousel-indicators li div.pag').width(uw);
		$('.carousel-indicators').width(nw);
	}

	$(window).resize(function() {
							  
		$scope.initialZoom = 1;
		$('#carousel .item').css({'transform':'','top':'','left':''});
							  
		var winH = $(window).height();
		if(winH < 630){
			$('.col-md-9 h1').css('font-size','10px');
			$('.col-md-9 h2').css('font-size','10px');
			$('.col-md-9 a').css('font-size','10px');
			$('.pagination').css('font-size','10px');
			$('.tools').css('margin-top','80%');
			$('body').css('padding-top','74px');
		} else {
			$('.col-md-9 h1').css('font-size','14px');
			$('.col-md-9 h2').css('font-size','14px');
			$('.col-md-9 a').css('font-size','14px');
			$('.pagination').css('font-size','14px');
			$('.tools').css('margin-top','180%');
			$('body').css('padding-top','84px');
		}
		var catalogWidth = ((winH)*1138)/775;
		$('#colcat').css('width',catalogWidth);
							  
		resizePagination();
		
		var activeHeight = (parseFloat($('#carousel .item.active').css('width'))*696)/960;
		$('#carousel').css('max-height',activeHeight);
		
		_productsMarkedArea();
	});
	
	$('#carousel').on('slid.bs.carousel', function (e) {
		
		$scope.initialZoom = 1;
		$('#carousel .item').css({'transform':'','top':'','left':''});
		
		var n = $('#carousel .item.active').data('index');
		
		var li = $('.pag').parent();
		var html = li.html();
		li.html('');
		var newli = $(".carousel-indicators").find("[data-page='" + n + "']");
		$(newli).html(html);
		
		$('.pag-n').remove();
		var pages = (n==1?'':(n*2-2) + '-') + (n*2-1);
		$(newli).append('<span class="pag-n">P&aacute;gina '+ pages +'</span>');
		
		if(!$scope.isChrome && !$scope.isFirefox){
			$('.pag-n').css('margin-left', '990px');
		}
		
		_productsMarkedArea();
	});

	_productsMarkedArea = function() {
		
		if($scope.User.userTypeid != 3){
			$image               = $('#carousel .item.active img')[0];
			if($image){
				$imageNaturalWidth   = $image.naturalWidth;
				$imageNaturalHeight  = $image.naturalHeight;
				$imageclientWidth    = $image.clientWidth;
				$imageclientHeight   = $image.clientHeight;
				$http({
					url: App.baseUrl('shop_brochures/read_products/' + $('#carousel .item.active').data('id')),
					method: 'GET',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function(data, status, headers, config) {
					$('.area-product').remove();
						$(data).each(function() {
						rw = $imageNaturalWidth / $imageclientWidth;
						rh = $imageNaturalHeight / $imageclientHeight;
						this.ShopBrochureProduct.coords.w = this.ShopBrochureProduct.coords.w / rw;
						this.ShopBrochureProduct.coords.h = this.ShopBrochureProduct.coords.h / rh;
						this.ShopBrochureProduct.coords.x = this.ShopBrochureProduct.coords.x / rw;
						this.ShopBrochureProduct.coords.y = this.ShopBrochureProduct.coords.y / rh;
						var link = $('<a href="javascript:;" class="area-product"></a>').css({
							'width': this.ShopBrochureProduct.coords.w + 'px',
							'height': this.ShopBrochureProduct.coords.h + 'px',
							'left': this.ShopBrochureProduct.coords.x + 'px',
							'top': this.ShopBrochureProduct.coords.y + 'px',
							'transform': 'rotate('+this.ShopBrochureProduct.coords.angle+'deg)'
						});
						if (this.ShopBrochureProduct.coords.shape == 2) {
							link.css({
								'border-radius': '50%'
							});
						}
						link.attr({
							'ng-controller': 'CartController',
							'ng-click': 'confirmadd("'+this.ShopBrochureProduct.id+'")',
						});
						$('#carousel .item.active').append($compile(link)($scope));
		
					});
				}).error(function(data, status, headers, config) {
					
				});
			}
		}
	}
	
	$scope.initialZoom = 1;
	$scope.zoomSize = 0.18;

	$scope.zoom = function(z) {
		if (z == 'in') {
			$scope.initialZoom += $scope.zoomSize;
		} else {
			if($scope.initialZoom - $scope.zoomSize >= 1){
				$scope.initialZoom -= $scope.zoomSize;
			}
		}
		$('#carousel .item.active').css('transform', 'scale('+$scope.initialZoom+')');
	}

});

App.controller('CartController', function($rootScope, $scope, $location, $http, $cookies, $routeParams, $modal, $timeout) {

	$scope.init = function() {
		
		$rootScope.User = $cookies;
		$scope.User 	= $cookies;

		$('body').css({
			'background':'#eaeaea',
			'padding-top': '84px',
		});

		$http({
			url: App.baseUrl('shop_brochures/cart'),
			method: 'GET',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data && data.result && data.result === true) {
				if(data.UserCartDeteail.length != 0 && data.UserCartDeteail[0].UserCart){
				$cookies.cartTotalQuantity = data.UserCartDeteail[0].UserCart.total_quantity;
				} else {
				$cookies.cartTotalQuantity = 0;
				}
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = data.UserCartDeteail;
				$scope.Reseller = data.Reseller;
			} else {
				$cookies.cartTotalQuantity = 0;
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = {};
			}
			
		}).error(function(data, status, headers, config) {
			
		});
		
		$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
	}
	
	$scope.confirmadd = function(product_id) {
		
		$scope.add_product_id = product_id;
		
		if ($cookies.user_type_id == 3) {
			bootbox.dialog({
                title: '¡Atención!',
                message: 'Para realizar tu pedido, solicitá a una revendedora que te envie los catalogos.',
                buttons: {
                    success: {
                        label: 'Aceptar',
                        className: 'btn-danger'
                    }
                }
            });
		} else {
			
			var modalInstance = $modal.open({
					  	templateUrl: 'resources/views/add-product-confirm.html',
						controller: function ($scope, $modalInstance) {
							$scope.canceladd = function () {
								$modalInstance.dismiss('cancel');
							}
							$scope.acceptadd = function () {
								$modalInstance.dismiss('cancel');

								if (0 < $scope.Products.length) {
									$scope.add($scope.select_product_id);
								} else {
									$scope.add($scope.add_product_id);
								}
							}
							$scope.setProductId = function(id) {
								$scope.select_product_id = id;
								$('#product-id-' + id).closest('ul').find('li').removeClass('add-product-content-select');
								$('#product-id-' + id).addClass('add-product-content-select');
							}
							$http({
								url: App.baseUrl('shop_brochures/read_product/' + $scope.add_product_id),
								method: 'GET',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							}).success(function(data, status, headers, config) {
								$scope.Product = data;
							}).error(function(data, status, headers, config) {});
							$http({
								url: App.baseUrl('shop_brochures/read_versions/' + $scope.add_product_id),
								method: 'GET',
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							}).success(function(data, status, headers, config) {
								$scope.Products = data.map(function(a){
									return a;
								});
								$scope.Products = $scope.Products.length ? $scope.Products : false;
							}).error(function(data, status, headers, config) {});
						},
						scope: $scope,
					  	size: 'lg',
					});
		}
	}

	$scope.add = function(product_id) {
		if ($cookies.user_type_id == 3) {
			bootbox.dialog({
                title: '¡Atención!',
                message: 'Para realizar tu pedido, solicitá a una revendedora que te envie los catalogos.',
                buttons: {
                    success: {
                        label: 'Aceptar',
                        className: 'btn-danger'
                    }
                }
            });
		} else {
			data = new Object();
			data.UserCartDeteail = new Object();
			data.UserCartDeteail.product_id = product_id;
			$http({
				url: App.baseUrl('shop_brochures/cart'),
				method: 'POST',
				data:  $.param({'data':data}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function(data, status, headers, config) {
				if (data && data.result && data.result === true) {
					$cookies.cartTotalQuantity = data.UserCart.total_quantity;
					$scope.User = $cookies;
					$rootScope.User = $cookies;
					var modalInstance = $modal.open({
					  	templateUrl: 'resources/views/add-product.html',
						controller: function ($scope, $modalInstance) {
							$scope.cancel = function () {
								$modalInstance.dismiss('cancel');
							}
							$scope.sendCart = function () {
								$modalInstance.dismiss('cancel');
								$location.path('/cart');
							}
						},
						scope: $scope,
					  	size: 'lg',
					});
				}
			}).error(function(data, status, headers, config) {});
		}
	}
	
	$scope.updateqty = function(v3) {
		data = new Object();
		data.UserCartDetail = new Object();
		data.UserCartDetail.id = v3.id;
		data.UserCartDetail.quantity = v3.quantity;
		
		$http({
			url: App.baseUrl('shop_brochures/cart_update_qty'),
			method: 'POST',
			data:  $.param({'data':data}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if(data.save){
				$cookies.cartTotalQuantity = data.total_quantity;
				
				$http({
					url: App.baseUrl('shop_brochures/cart'),
					method: 'GET',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function(data, status, headers, config) {
					if (data && data.result && data.result === true) {
						if(data.UserCartDeteail.length != 0 && data.UserCartDeteail[0].UserCart){
						$cookies.cartTotalQuantity = data.UserCartDeteail[0].UserCart.total_quantity;
						} else {
						$cookies.cartTotalQuantity = 0;
						}
						$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
						$scope.UserCartDeteail = data.UserCartDeteail;
						$scope.Reseller = data.Reseller;
					} else {
						$cookies.cartTotalQuantity = 0;
						$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
						$scope.UserCartDeteail = {};
					}
					
				}).error(function(data, status, headers, config) {
					
				});
				$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
				
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

	$scope.remove = function(id) {
		$http({
			url: App.baseUrl('shop_brochures/delete_product'),
			method: 'POST',
			data:  $.param({'data':{'id':id}}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data && data.result && data.result === true) {
				$cookies.cartTotalQuantity = data.total_quantity;
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = data.UserCartDeteail;
				$scope.Reseller = data.Reseller;
				
				$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

	$scope.asd = function(id) {
		console.log(id);
		console.log('quantity');
		return;
		$http({
			url: App.baseUrl('shop_brochures/delete_product'),
			method: 'POST',
			data:  $.param({'data':{'id':id}}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data && data.result && data.result === true) {
				$cookies.cartTotalQuantity = data.total_quantity;
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = data.UserCartDeteail;
				$scope.Reseller = data.Reseller;

			}
		}).error(function(data, status, headers, config) {
			
		});
	}

	$scope.selectReseller = function($event) {
		var element = $($event.target)[0];
		if ($('.select-reseller ul').height() > '55') {
			$('.select-reseller ul').height('55');
			$('.select-reseller ul i.icn').after($(element));
		} else {
			$('.select-reseller ul').height('auto');
		}
	}

	$scope.send = function() {
		if ($location.path() == '/cart') {
			if ($cookies.userTypeid == 1) {
				var resellerId = $cookies.userId;
			} else {
				var resellerId = $('.select-reseller ul:first-child li').data('value');
			}
			data = new Object();
			data.UserCart = new Object();
			data.UserCart.reseller_user_id = resellerId;
			$http({
				url: App.baseUrl('shop_brochures/send_cart'),
				method: 'POST',
				data:  $.param({'data':data}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function(data, status, headers, config) {
				if (data && data.result && data.result === true) {
					//console.log(data);
					$cookies.cartTotalQuantity = 0;
					$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
					$cookies.ordersByProducts = data.ordersByProducts;
					$cookies.ordersByCustomers= data.ordersByCustomers;
					$rootScope.User 				= $cookies;
					$scope.User 					= $cookies;
					
					angular.forEach($scope.UserCartDeteail, function (k, v) {
					    delete $scope.UserCartDeteail[v];
					});
					if ($cookies.userTypeid == 1) {
						bootbox.dialog({
						    title: '¡Atención!',
						    message: 'Ha cerrado su pedido.',
						    buttons: {
						        success: {
						            label: 'Aceptar',
						            className: 'btn-danger'
						        }
						    }
						});
					} else {
						bootbox.dialog({
						    title: '¡Atención!',
						    message: 'Su pedido ha sido enviado a la revendedora seleccionada.',
						    buttons: {
						        success: {
						            label: 'Aceptar',
						            className: 'btn-danger'
						        }
						    }
						});
					}
				} else {
					bootbox.dialog({
	                    title: '¡Atención!',
	                    message: 'Su pedido no pudo ser enviado enviado, por favor, intente nuevamente.',
	                    buttons: {
	                        success: {
	                            label: 'Aceptar',
	                            className: 'btn-danger'
	                        }
	                    }
	                });
				}
			}).error(function(data, status, headers, config) {
			});
		} else {
			$location.path('/cart')
		}
	}

});

App.controller('SendBrochuresController', function($rootScope, $scope, $location, $http, $cookies, $timeout) {

	window.fbAsyncInit = function() {
		FB.init({
	      	appId      : '338346469695953',
	      	status     : true,
	      	xfbml      : true,
	      	version    : 'v2.1'
		});
	};

	(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/es_LA/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	$timeout(function() {
		FB.api('/me/friends/', {fields:'picture, name', limit: 10000},function(response) {
			$scope.Friends = response.data;
			$scope.$apply();
		});
	}, 2000);

	$('body').css({
		'background':'#eaeaea',
		'padding-top': '84px',
	});
	
	$rootScope.User = $cookies;
	$scope.User 	= $cookies;

	$http({
		url: App.baseUrl('campaigns'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
		$brochures = [];
		angular.forEach(data, function(v, k) {
			angular.forEach(v, function(v2, k2) {
				if (k2 == 'CampaignShopBrochure') {
					angular.forEach(v2, function(v3, k3) {
				  		$brochures.push({'Brochures':v3, 'Campaigns':v});
					});
				}
			});
		});
		$scope.Brochures = $brochures;
	}).error(function(data, status, headers, config) {
		
	});

	$scope.selectBrochure = function($event) {
		var optionSelected = $event.currentTarget;
		if ($('.select-brochure ul').height() > '55') {
			$('.select-brochure ul').height('55');
			$('.select-brochure ul i.icn').after(optionSelected);
		} else {
			$('.select-brochure ul').height('auto');
		}
	}
	$scope.select = function(select) {
		$('input[type=checkbox]').each(function() {
			if (select == 'select') {
				$(this).prop('checked', true);
			} else {
				$(this).prop('checked', false);
			}
		});
	}
	$scope.sendInvite = function() {
		var brochureId = $('.select-brochure ul:first-child li').data('value');
		
		if ($cookies.userTypeid == 1) {
			FB.ui({
				method: 'send',
				link: 'http://qa.addfow.com/invite.html?r='+$cookies.userId+'&b=' + brochureId,//$location.absUrl().replace('send-brochure', 'invite/' + brochureId),
			},
			function(response) {
				if (response) {
					bootbox.dialog({
						title: '¡Atención!',
						message: 'Su invitación ha sido enviada.',
						buttons: {
							success: {
								label: 'Aceptar',
								className: 'btn-danger'
							}
						}
					});
				}
			});
		} else {
						FB.ui({
				method: 'send',
				link: 'http://qa.addfow.com/invite.html?b=' + brochureId,//$location.absUrl().replace('send-brochure', 'invite/' + brochureId),
			},
			function(response) {
				if (response) {
					bootbox.dialog({
						title: '¡Atención!',
						message: 'Su invitación ha sido enviada.',
						buttons: {
							success: {
								label: 'Aceptar',
								className: 'btn-danger'
							}
						}
					});
				}
			});

		}
	}
	$scope.send = function() {
		/*
		$http({
			url: 'https://graph.facebook.com/oauth/access_token?client_id=640169716119327&client_secret=5a38ebb2ab28ddf70a52d731cd4d9805&grant_type=client_credentials',
			method: 'GET',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			var t = data.split('=');
			var linkurl = 'http://oficina.vnstudios.com/dingdong/';
			
			$http({
				url: 'https://graph.facebook.com/v2.3/825744207494167/notifications?access_token='+t[1]+'&href='+linkurl+'&template=Esto es un test',
				method: 'POST',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function(data, status, headers, config) {
				console.log(data);
			}).error(function(data, status, headers, config) {
				
			});
			
		}).error(function(data, status, headers, config) {
			
		});*/
		
		
		var brochureId = $('.select-brochure ul:first-child li').data('value');
		
		var uids = '';
		$("input:checkbox[name=chk]:checked").each(function()
		{
			uids += (uids == '' ? '':',')+ $(this).val();
		});
		
		if ($cookies.userTypeid == 1) {
			FB.ui({
			  method: 'send',
			  to: uids,
			  link: 'http://qa.addfow.com/brochure.html?r='+$cookies.userId+'&b='+brochureId,
			},
			function(response) {
				if (response) {
					bootbox.dialog({
	                    title: '¡Atención!',
	                    message: 'Su catálogo ha sido enviado.',
	                    buttons: {
	                        success: {
	                            label: 'Aceptar',
	                            className: 'btn-danger'
	                        }
	                    }
	                });
				}
			});
		} else {
			FB.ui({
			    method: 'send',
				to: uids,
			    link: 'http://qa.addfow.com/brochure.html?b=' + brochureId,//$location.absUrl().replace('send-brochure', 'invite/' + brochureId),
			},
			function(response) {
				if (response) {
					bootbox.dialog({
	                    title: '¡Atención!',
	                    message: 'Su catálogo ha sido enviado.',
	                    buttons: {
	                        success: {
	                            label: 'Aceptar',
	                            className: 'btn-danger'
	                        }
	                    }
	                });
				}
			});
		}
	}

});

App.controller('ResellerController', function($rootScope, $scope, $location, $http, $modal) {

	$scope.reseller = function() {

		var modalInstance = $modal.open({
		  	templateUrl: 'resources/views/reseller.html',
			controller: ModalInstanceCtrl,
			scope: $scope,
		  	size: 'lg',
		});
	}

	var ModalInstanceCtrl = function ($scope, $modalInstance) {
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		}
	}

	$scope.signUp = function() {
		if ($('#reseller-form').valid()) {
			data = new Object();
			data.User = new Object();
			data.User.user_type_id = 1;
			data.User.city = $('#reseller-form input[name=city]').val();
			data.User.zone = $('#reseller-form input[name=code]').val();
			$http({
				url: App.baseUrl('users/user_type'),
				method: 'POST',
				data:  $.param({'data':data}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function(data, status, headers, config) {
				if (data && data.result && data.result === true) {
					$scope.cancel();
					$scope.User.userTypeid = 1;
					$rootScope.User.userTypeid = 1;
				}
			}).error(function(data, status, headers, config) {
				
			});
		}
	}

});

App.controller('OrdersByCustomersController', function($rootScope, $scope, $location, $http, $cookies, $timeout,$route) {

	$rootScope.User = $cookies;
	$scope.User 	= $cookies;

	$('body').css({
		'background':'#eaeaea',
		'padding-top': '84px',
	});

	$http({
		url: App.baseUrl('users/orders_by_customers'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
		$scope.UserCart = data.UserCart;
		$scope.UserCart.TotalQuantity = data.total_quantity;
		$scope.UserCart.TotalProducts = data.products;
		$scope.UserCart.TotalPrice = data.total_price;
		
		if($rootScope.UserCart)
			$rootScope.UserCart.TotalQuantity = data.total_quantity;
		
	}).error(function(data, status, headers, config) {
		
	});

	$scope.close = function(id) {
		$http({
			url: App.baseUrl('users/close_order_bycampaign/'+id),
			method: 'POST',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			
			$route.reload();
			
			/*$scope.UserCart = data.UserCart;
			$scope.UserCart.TotalQuantity = data.total_quantity;
			$scope.UserCart.TotalProducts = data.products;
			$scope.UserCart.TotalPrice = data.total_price;
			
			if($rootScope.UserCart)
				$rootScope.UserCart.TotalQuantity = data.total_quantity;
			$cookies.ordersByProducts = 0;
			$cookies.ordersByCustomers = 0;
			$rootScope.User 				= $cookies;
			$scope.User 					= $cookies;*/
			
		}).error(function(data, status, headers, config) {
			
		});
	}

});

App.controller('PrintOrdersByCustomersController', function($rootScope, $scope, $location, $routeParams, $http, $cookies, $timeout) {

	$scope.init = function() {
		$('body').css({
			'background':'#fff',
			'padding-top': '0',
		});
		console.log($routeParams.parchuseDate);
		$http({
			url: App.baseUrl('users/orders_by_customers/' + $routeParams.parchuseDate),
			method: 'GET',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			// if (data && data.result && data.result === true) {
				console.log(data);
				// $cookies.cartTotalQuantity = data.total_quantity;
				// $rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				// $scope.UserCartDeteail = data.UserCartDeteail;
				// $scope.Reseller = data.Reseller;
				$scope.UserCart = data.UserCart;
				$scope.UserCart.parchuse_date = data.parchuse_date;
				$scope.UserCart.total_quantity = data.products;
				$scope.UserCart.total_price = data.total_price;
				$scope.UserCart.customers = data.customers;
			// }
		}).error(function(data, status, headers, config) {
			
		});
	}

});

App.controller('PrintOrdersByProductsController', function($rootScope, $scope, $location, $routeParams, $http, $cookies, $timeout) {

	$scope.init = function() {
		$('body').css({
			'background':'#fff',
			'padding-top': '0',
		});
		//console.log($routeParams.parchuseDate);
		$http({
			url: App.baseUrl('users/orders_by_products/' + $routeParams.parchuseDate),
			method: 'GET',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
				$scope.ShopBrochure = data.ShopBrochure;
				$scope.Order = {};
				$scope.Order.PurchaseDate = data.parchuse_date;
				$scope.Order.TotalQuantity = data.total_quantity;
				$scope.Order.TotalPrice = data.total_price;
		}).error(function(data, status, headers, config) {
			
		});
	}

});

App.controller('OrdersByProductsController', function($rootScope, $scope, $location, $http, $cookies, $timeout) {

	$rootScope.User = $cookies;
	$scope.User 	= $cookies;

	$('body').css({
		'background':'#eaeaea',
		'padding-top': '84px',
	});

	$http({
		url: App.baseUrl('users/orders_by_products'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
		$scope.ShopBrochure = data.ShopBrochure;
		$scope.Order = {};
		$scope.Order.TotalQuantity = data.total_quantity;
		$scope.Order.TotalPrice = data.total_price;
		
		$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
		
	}).error(function(data, status, headers, config) {
		
	});
	
	$scope.updateqty = function(v2) {
		
		data = new Object();
		data.UserCartDetail = new Object();
		data.UserCartDetail.id = v2.UserCartDetail.id;
		data.UserCartDetail.quantity = v2.total_quantity;
		
		$http({
			url: App.baseUrl('shop_brochures/cart_update_qty'),
			method: 'POST',
			data:  $.param({'data':data}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if(data.save){
				$cookies.cartTotalQuantity = data.total_quantity;
				
				//refresh products
				$http({
					url: App.baseUrl('users/orders_by_products'),
					method: 'GET',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function(data, status, headers, config) {
					$scope.ShopBrochure = data.ShopBrochure;
					$scope.Order = {};
					$scope.Order.TotalQuantity = data.total_quantity;
					$scope.Order.TotalPrice = data.total_price;
					$cookies.ordersByProducts = data.total_quantity;
					
					$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
					
				}).error(function(data, status, headers, config) {
					
				});
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

	$scope.remove = function(id) {
		$http({
			url: App.baseUrl('shop_brochures/delete_order_product'),
			method: 'POST',
			data:  $.param({'data':{'id':id}}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data && data.result && data.result === true) {
				$cookies.cartTotalQuantity = data.total_quantity;
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				//$scope.UserCartDeteail = data.UserCartDeteail;
				//$scope.Reseller = data.Reseller;
				
				//$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
				
				//refresh products
				$http({
					url: App.baseUrl('users/orders_by_products'),
					method: 'GET',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				}).success(function(data, status, headers, config) {
					$scope.ShopBrochure = data.ShopBrochure;
					$scope.Order = {};
					$scope.Order.TotalQuantity = data.total_quantity;
					$scope.Order.TotalPrice = data.total_price;
					$cookies.ordersByProducts = data.total_quantity;
					
					$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
					
				}).error(function(data, status, headers, config) {
					
				});
				
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

	$scope.close = function(id) {
				
		$http({
			url: App.baseUrl('users/close_order_bycampaign/'+id),
			method: 'POST',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			
			$scope.ShopBrochure = data.ShopBrochure;
			$scope.Order = {};
			$scope.Order.TotalQuantity = data.total_quantity;
			$scope.Order.TotalPrice = data.total_price;
			
			$http({
				url: App.baseUrl('users/get_orders_values'),
				method: 'GET',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).success(function(data, status, headers, config) {
				var products = 0, customers = 0;
				if(data.User){
					products = data.User.OrdersByProducts;
					customers = data.User.OrdersByCustomers;
				}
				$cookies.ordersByProducts = products;
				$cookies.ordersByCustomers = customers;
				
				$rootScope.User = $cookies;
				$scope.User 	= $cookies;
				
			}).error(function(data, status, headers, config) {
				if(status == 401){
					$location.path('/');
				}
			});
			
		}).error(function(data, status, headers, config) {});
	}

});

App.controller('OrderHistoryController', function($rootScope, $scope, $location, $http, $cookies, $timeout) {

	$('body').css({
		'background':'#eaeaea',
		'padding-top': '84px',
	});

	$rootScope.User 	= $cookies;
	$scope.User 		= $cookies;
	$scope.currentPage 	= 1;
	$scope.pageSize 	= 10;
	$scope.Orders 		= [];

	$http({
		url: App.baseUrl('users/order_history'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
			$scope.Orders = data;
	}).error(function(data, status, headers, config) {
		
	});

});

App.controller('ClientHistoryController', function($rootScope, $scope, $location, $http, $cookies, $timeout) {

	$('body').css({
		'background':'#eaeaea',
		'padding-top': '84px',
	});

	$rootScope.User 	= $cookies;
	$scope.User 		= $cookies;
	$scope.currentPage 	= 1;
	$scope.pageSize 	= 10;
	$scope.Orders 		= [];

	$http({
		url: App.baseUrl('users/client_history'),
		method: 'GET',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	}).success(function(data, status, headers, config) {
			$scope.Orders = data;
	}).error(function(data, status, headers, config) {
		
	});

});

App.controller('OrderDetailController', function($rootScope, $scope, $location, $http, $cookies, $routeParams, $modal, $timeout) {

	$scope.init = function() {
		
		$rootScope.User = $cookies;
		$scope.User 	= $cookies;

		$('body').css({
			'background':'#eaeaea',
			'padding-top': '84px',
		});

		$http({
			url: App.baseUrl('shop_brochures/order_detail/' + $routeParams.orderDate),
			method: 'GET',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data && data.result && data.result === true) {
				if(data.UserCartDeteail.length != 0 && data.UserCartDeteail[0].UserCart){
					$cookies.cartTotalQuantity = data.UserCartDeteail[0].UserCart.total_quantity;
					
					if(data.UserCartDeteail[0].UserCart.purchase_date == null || data.UserCartDeteail[0].UserCart.purchase_date == ''){
						$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
					} else {
						
						$timeout(function() { $('.type-number').attr("readonly", true) }, 1000);
					}
					
				} else {
					$cookies.cartTotalQuantity = 0;					
				}
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = data.UserCartDeteail;
				$scope.Reseller = data.Reseller;
			} else {
				$cookies.cartTotalQuantity = 0;
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = {};
			}
			
		}).error(function(data, status, headers, config) {
			
		});
		
	}
	
	$scope.updateqty = function(v3) {
		data = new Object();
		data.UserCartDetail = new Object();
		data.UserCartDetail.id = v3.id;
		data.UserCartDetail.quantity = v3.quantity;
		
		$http({
			url: App.baseUrl('shop_brochures/cart_update_qty'),
			method: 'POST',
			data:  $.param({'data':data}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if(data.save){
				$cookies.cartTotalQuantity = data.total_quantity;
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

	$scope.remove = function(id) {
		$http({
			url: App.baseUrl('shop_brochures/delete_product'),
			method: 'POST',
			data:  $.param({'data':{'id':id}}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		}).success(function(data, status, headers, config) {
			if (data && data.result && data.result === true) {
				$cookies.cartTotalQuantity = data.total_quantity;
				$rootScope.cartTotalQuantity = $cookies.cartTotalQuantity;
				$scope.UserCartDeteail = data.UserCartDeteail;
				$scope.Reseller = data.Reseller;
				
				$timeout(function() { $('.type-number').spinner({ min: 1, max: 999999, spin: function( event, ui ) { $(event.target).val(ui.value); $(event.target).change();}}); }, 1000);
			}
		}).error(function(data, status, headers, config) {
			
		});
	}

});