<?php
class CampaignsController extends AppController {

	public $uses = array('Campaign', 'CampaignShopBrochure', 'ShopBrochure', 'ShopBrochureImage', 'UserClientBrochure', 'UserShopBrochure');

	public $autoRender = false;

    public function beforeFilter() {
        if (!$this->Session->read('User')) {
            die(json_encode(array('login' => false)));
        }
    }

	public function index() {
		
		$User = $this->Session->read('User');
		if(isset($User)){
		
			if($this->request->is('get')) {
			
				if($User['User']['user_type_id'] == 2){
				
					$brochures = $this->UserClientBrochure->find('all', array('conditions' => array('user_id' => $User['User']['id'])));
					$brochure_ids = [];
					foreach ($brochures as $k => $v) {
						array_push($brochure_ids, $v['UserClientBrochure']['shop_brochure_id']);
					}
					
					if(count($brochure_ids) > 1){
						$Campaigns = $this->Campaign->find('all', array('joins' => array(array('table' => 'campaign_shop_brochures','alias' => 'CampaignShopBrochure','type' => 'INNER','conditions' => array('CampaignShopBrochure.campaign_id = Campaign.id'))),'conditions' => array('start_date <=' => time(), 'end_date >=' => time(), 'CampaignShopBrochure.shop_brochure_id in'=>$brochure_ids)));
					} else {
						$Campaigns = $this->Campaign->find('all', array('joins' => array(array('table' => 'campaign_shop_brochures','alias' => 'CampaignShopBrochure','type' => 'INNER','conditions' => array('CampaignShopBrochure.campaign_id = Campaign.id'))),'conditions' => array('start_date <=' => time(), 'end_date >=' => time(), 'CampaignShopBrochure.shop_brochure_id'=>$brochure_ids)));
					}
				} else {
					$Campaigns = $this->Campaign->find('all', array('conditions' => array('start_date <=' => time(), 'end_date >=' => time())));
				}
				
				$TempCampaigns = [];
	
				foreach ($Campaigns as $k => $v) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == 'CampaignShopBrochure') {
							foreach ($v2 as $k3 => $v3) {
								if(($User['User']['user_type_id'] == 1 || $User['User']['user_type_id'] == 3) || in_array($v3['shop_brochure_id'], $brochure_ids)){
									$ShopBrochure = $this->ShopBrochure->find('first', array('conditions' => array('id' => $v3['shop_brochure_id'])));
									if($ShopBrochure['ShopBrochure']['expiration_date_real'] >= time())
									{
										$Campaigns[$k][$k2][$k3]['ShopBrochure'] = $ShopBrochure['ShopBrochure'];
										$ShopBrochureImage = $this->ShopBrochureImage->find('first', array('conditions' => array('shop_brochure_id' => $v3['shop_brochure_id'], 'order' => 0)));
										$Campaigns[$k][$k2][$k3]['ShopBrochureImage'] = isset($ShopBrochureImage['ShopBrochureImage']) ? $ShopBrochureImage['ShopBrochureImage'] : null;
										
										$ShopBrochureUser = $this->UserShopBrochure->find('first', array('conditions' => array('shop_brochure_id' => $v3['shop_brochure_id'],'user_id' => $User['User']['id'])));
										if($ShopBrochureUser){
											$Campaigns[$k][$k2][$k3]['ShopBrochureUser'] = $ShopBrochureUser['UserShopBrochure'];
											$TempCampaigns[$v3['id']][$k2][$v3['shop_brochure_id']]['ShopBrochureUser'] = $ShopBrochureUser['UserShopBrochure'];
										}
										
										$TempCampaigns[$v3['id']][$k2][$v3['shop_brochure_id']]['ShopBrochure'] = $ShopBrochure['ShopBrochure'];
										$TempCampaigns[$v3['id']][$k2][$v3['shop_brochure_id']]['ShopBrochureImage'] = isset($ShopBrochureImage['ShopBrochureImage']) ? $ShopBrochureImage['ShopBrochureImage'] : null;
										$TempCampaigns[$v3['id']]['Campaign'] = $Campaigns[$k]['Campaign'];
									}
									
								}
								else {
									unset($Campaigns[$k][$k2][$k3]);
								}
							}
						}
					}
				}
				
			}
			die(json_encode($TempCampaigns));
		}else{
			$this->response->statusCode(401);
		}
	}
	
}