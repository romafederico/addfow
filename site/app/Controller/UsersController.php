<?php
class UsersController extends AppController {

	public $uses = array('User', 'UserCart', 'UserCartOffer', 'UserCartOffer', 'UserCartDetail', 'UserResellerGroup', 'UserClientBrochure', 'UserShopBrochure');

	public $autoRender = false;

	// public function beforeFilter() {
	// 	if ($this->request['params']['action'] != 'login') {
	// 		if (!$this->Session->read('User')) {
	// 			die(json_encode(array('login' => false)));
	// 		}
	// 	} 
	// }

	public function login() {
		$data['login'] = false;
		$created = false;
		if ($this->request->is('post')) {
			$this->User->recursive = -1;
			if (!$User = $this->User->findByFacebookUserId($this->request->data['User']['id'])) {
				$User = $this->create($this->request->data['User']);
				$created = true;
			} else {
				$User = $this->User->findByFacebookUserId($this->request->data['User']['id']);
			}
			if (!$User['User']['active']) {
				$data['login'] = false;
				die(json_encode($data));
			}
			$this->UserCart->recursive = -1;
			$UserCart = $this->UserCart->find('first', array(
				'conditions' => array(
					'user_id' => $User['User']['id'],
					'user_cart_status_flags' => 0,
	        		'purchase_date' => null,
	        		'order_date' => null,
					)
				)
			);
			if (!empty($this->request->data['UserResellerGroup'])) {
				$this->UserResellerGroup->recursive = -1;
				$UserResellerGroup['UserResellerGroup'] = $this->request->data['UserResellerGroup'];
				$UserResellerGroup['UserResellerGroup']['user_id'] = $User['User']['id'];
				if (!($UserResellerGroupTemp = $this->UserResellerGroup->find('first', array('conditions' => $UserResellerGroup['UserResellerGroup'])))) {
					$UserResellerGroup = $this->UserResellerGroup->save($UserResellerGroup);
				} else {
					$UserResellerGroup = $UserResellerGroupTemp;
				}
				//guardo el catalogo enviado
				$this->UserClientBrochure->recursive = -1;
				$UserClientBrochure['UserClientBrochure'] = $this->request->data['UserClientBrochure'];
				$UserClientBrochure['UserClientBrochure']['user_id'] = $User['User']['id'];
				$UserClientBrochure['UserClientBrochure']['user_reseller_group_id'] = $UserResellerGroup['UserResellerGroup']['id'];
				if (!$this->UserClientBrochure->find('first', array('conditions' => $UserClientBrochure['UserClientBrochure']))) {
					$UserClientBrochure = $this->UserClientBrochure->save($UserClientBrochure);
				}
				//------<<<
				
				$this->User->id = $User['User']['id'];
				if ($this->User->exists()) {
					if($User['User']['user_type_id'] == 3){
						$this->User->save(array('user_type_id' => 2));
						$User['User']['user_type_id'] = 2;
					}
				}
			}
			$User['User']['OrdersByCustomers'] = $this->get_orders_by_customers(null, $User['User']['id']);
			$User['User']['OrdersByProducts'] = $this->get_orders_by_products(null, $User['User']['id']);
			$User['User']['picture'] = Router::url('/images/users/'.$User['User']['picture'], true);
			$data['User'] = $User['User'];
			if (!empty($UserCart)) {
				$data['UserCart'] = $UserCart['UserCart'];
			}
			if ($this->Session->write('User', $User)) {
				$data['login'] = true;
			}
			
			$data['created'] = $created;
		}
		die(json_encode($data));
	}

	public function logout() {
		if ($this->request->is('post')) {
			$this->Session->destroy();
			die(json_encode(array('session' => false)));
		}
	}

	private function create($request) {
		$request['facebook_user_id'] = $request['id'];
		if ($size = getimagesize($request['picture']) AND is_array($size) AND array_key_exists(2, $size)) {
			$extension = strtolower(image_type_to_extension($size[2]));
			$name = md5(uniqid(mt_rand())) . $extension;
			if (copy($request['picture'], WWW_ROOT . 'images/users/' . $name)) {
				$request['picture'] = $name;
			} else {
				$request['picture'] = 'dafault.jpg';
			}
		} else {
				$request['picture'] = 'dafault.jpg';
		}
		unset($request['id'], $request['gender'], $request['link'], $request['locale'], $request['name'], $request['timezone'], $request['updated_time'], $request['verified']);
		$User['User'] = $request;
		$this->User->create();
		if ($User = $this->User->save($User)) {
			return $User;
		} else {
			return false;
		}
	}

	public function user_type() {
		$data['result'] = false;
		if ($this->request->is('post')) {
			$this->User->id = $this->Session->read('User')['User']['id'];
			if ($this->User->exists() AND $this->User->save($this->request->data)) {
				$data['result'] = true;
			}
		}
		die(json_encode($data));
	}
	
	public function get_orders_by_products($date = null, $user_id){
	
		if($user_id == null){
			$User = $this->Session->read('User');
			$user_id = $User['User']['id'];
		}
		$this->UserCart->Behaviors->load('Containable');
        $this->UserCart->contain(array('User','UserCartDetail' => array('ShopBrochureProduct' => array('Product', 'ShopBrochure' => array('CampaignShopBrochure' => 'Campaign', 'ShopBrochureImage' => array('conditions' => array('ShopBrochureImage.order' => 0)))))));
		$UserCart = $this->UserCart->find('all', array('joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),'conditions' => array('reseller_user_id' => $user_id, 'user_cart_status_flags' => 1,'purchase_date' => $date,'order_date !=' => null,))
        );
        $rows = array();
        foreach ($UserCart as $k => $v) {
			foreach ($v['UserCartDetail'] as $k2 => $v2) {
				if(isset($rows[$v2['ShopBrochureProduct']['shop_brochure_id']][$v2['ShopBrochureProduct']['id']]['total_quantity'])){
					$rows[$v2['ShopBrochureProduct']['shop_brochure_id']][$v2['ShopBrochureProduct']['id']]['total_quantity'] += $v2['quantity'];
					} else {
					$rows[$v2['ShopBrochureProduct']['shop_brochure_id']][$v2['ShopBrochureProduct']['id']]['total_quantity'] = $v2['quantity'];
					}
			}
        }
        $rows = array_values($rows);
        $total_quantity = 0;
        foreach ($rows as $k => $v) {
        	$rows[$k] = array_values($rows[$k]);
        	foreach ($v as $k2 => $v2) {
        		$total_quantity = $total_quantity + $v2['total_quantity'];
        	}
        }
        return $total_quantity;
	}
	
	private function get_orders_by_customers($date = null, $user_id){
		if($user_id == null){
			$User = $this->Session->read('User');
			$user_id = $User['User']['id'];
		}
		$UserCart = $this->UserCart->find('all', array(
			'fields' => array('DISTINCT UserCart.user_id'),
			'joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),
        	'conditions' => array(
        		'reseller_user_id' => $user_id, 
        		'user_cart_status_flags' => 1,
        		'purchase_date' => $date,
        		'order_date !=' => null,
        		),
			'group' => array('UserCart.user_id')
        	)
        );
        return count($UserCart);
	}

	public function orders_by_customers($date = null) {
        $User = $this->Session->read('User');
        $this->UserCart->Behaviors->load('Containable');
        $this->UserCart->contain(array(
			'User',
			'UserCartOffer',
			'UserCartDetail' => array(
				'UserCartDetailOffer' => array('UserCartOffer' => array('Offer')),
				'ShopBrochureProduct' => array(
					'Product', 
					'ShopBrochure' => array(
						'CampaignShopBrochure' => 'Campaign', 
						'ShopBrochureImage' => array(
							'conditions' => array(
								'ShopBrochureImage.order' => 0
								)
							)
						)
					)
				)
        	)
        );
        $UserCart = $this->UserCart->find('all', array(
			'joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),
        	'conditions' => array(
        		'reseller_user_id' => $User['User']['id'], 
        		'user_cart_status_flags' => 1,
        		'purchase_date' => $date,
        		'order_date !=' => null,
        		),
			'order' => array('UserCart.user_id'),
        	)
        );
		
		$UserCartUsers = $this->UserCart->find('all', array(
			'fields' => array('DISTINCT UserCart.user_id'),
			'joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),
        	'conditions' => array(
        		'reseller_user_id' => $User['User']['id'], 
        		'user_cart_status_flags' => 1,
        		'purchase_date' => $date,
        		'order_date !=' => null,
        		),
			'group' => array('UserCart.user_id'),
        	)			
        );
		
        $total_quantity = count($UserCartUsers);
		$products = 0;
		$total_price = 0;
		$offers_price = 0;
		$rows = array();
		
		foreach ($UserCart as $k => $v) {
            $UserCart[$k]['User']['picture'] = Router::url('/images/users/' . $v['User']['picture'], true);
            foreach ($v['UserCartDetail'] as $k2 => $v2) {
			
				$cid = $v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id'];
				$uid = $UserCart[$k]['UserCart']['user_id'];
				$sbid = $v2['ShopBrochureProduct']['shop_brochure_id'];
			
				$rows[$cid]['Campaign'] = $v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign'];
				$rows[$cid]['Users'][$uid]['User'] = $UserCart[$k]['User'];
				$rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProducts'][] = $UserCart[$k]['UserCartDetail'][$k2];

				/*$addq = false;
				if(isset($rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProductsPrices'])){
					foreach ($rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProductsPrices'] as $i => $pp) {
						if($pp['ShopBrochureProduct']['price'] == $UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct']['price']){
							$rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProductsPrices'][$i]['quantity']+= $UserCart[$k]['UserCartDetail'][$k2]['quantity'];
							$addq = true;
							break;
						}
					}
				}
				if(!$addq){
					if(!isset($rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProductsPrices'])){
						$rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProductsPrices'] = [];
					}
					$rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochureProductsPrices'][] = $UserCart[$k]['UserCartDetail'][$k2];
				}*/

				$rows[$cid]['Users'][$uid]['ShopBrochures'][$sbid]['ShopBrochure'] = $v2['ShopBrochureProduct']['ShopBrochure'];
				
				$products += $v2['quantity'];
				$total_price += $v2['quantity_available'] * $v2['ShopBrochureProduct']['price'];
            	unset($UserCart[$k]['UserCartDetail'][$k2]);
            }
			//ofertas
			foreach ($v['UserCartOffer'] as $k2 => $v2) {
				$offers_price += $v2['total_price'];
			}
            unset($UserCart[$k]['User'], $UserCart[$k]['UserCart']);
        }
		
        $data['UserCart'] = array_values($rows);
        $data['total_quantity'] = $total_quantity;
		$data['products'] = $products;
		$data['total_price'] = $total_price + $offers_price;
		
        if ($date) {
	        $data['parchuse_date'] = date('d/m/Y', $date);
	        $data['customers'] = count($data['UserCart']);			
        }
        die(json_encode($data));
    }

    public function orders_by_products($date = null) {
        $User = $this->Session->read('User');
        $this->UserCart->Behaviors->load('Containable');
        $this->UserCart->contain(array(
			'User',
			'UserCartOffer',
			'UserCartDetail' => array(
				'UserCartDetailOffer' => array('UserCartOffer' => array('Offer')),
				'ShopBrochureProduct' => array(
					'Product', 
					'ShopBrochure' => array(
						'CampaignShopBrochure' => 'Campaign', 
						'ShopBrochureImage' => array(
							'conditions' => array(
								'ShopBrochureImage.order' => 0
								)
							)
						)
					)
				)
        	)
        );
        $UserCart = $this->UserCart->find('all', array(
			'joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),
        	'conditions' => array(
        		'reseller_user_id' => $User['User']['id'], 
        		'user_cart_status_flags' => 1,
        		'purchase_date =' => $date,
        		'order_date !=' => null,
        		)
        	)
        );
        $rows = array();
		$offers_price = 0;
        foreach ($UserCart as $k => $v) {
            foreach ($v['UserCartDetail'] as $k2 => $v2) {
			
				$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['Campaign'] = $v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign'];
				
				//products
				$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['Products'][] = $UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct'];
				
				//UserCartDetail
				$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['UserCartDetail'] = $v2;
				
				//UserCartDetailOffer
				$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['UserCartDetailOffers'][] = $UserCart[$k]['UserCartDetail'][$k2]['UserCartDetailOffer'];
				
				//shopbrochureproduct
				$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochure'] = $v2['ShopBrochureProduct']['ShopBrochure'];
				
				//quantities
				if(isset($rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['total_quantity'])){
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['total_quantity'] += $v2['quantity'];
				} else {
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['total_quantity'] = $v2['quantity'];
				}
				
				//quantities available
				if(isset($rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['total_quantity_available'])){
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['total_quantity_available'] += intval($v2['quantity_available']);
				} else {
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['total_quantity_available'] = intval($v2['quantity_available']);
				}
				
				//price
				if(isset($rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['price'])){
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['price'] += $v2['quantity']*$UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct']['price'];
				} else {
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['price'] = $v2['quantity']*$UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct']['price'];
				}
				
				//price offer
				if(isset($rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['price_offer'])){
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['price_offer'] += $v2['quantity_available']*$UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct']['price'];
				} else {
					$rows[$v2['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['campaign_id']]['ShopBrochures'][$v2['ShopBrochureProduct']['shop_brochure_id']]['ShopBrochureProducts'][$v2['ShopBrochureProduct']['id']]['price_offer'] = $v2['quantity_available']*$UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct']['price'];
				}
            }
			//ofertas
			foreach ($v['UserCartOffer'] as $k2 => $v2) {
				$offers_price += $v2['total_price'];
			}
        }
		
        $rows = array_values($rows);
        $total_quantity = 0;
		$total_price = $offers_price;
        foreach ($rows as $k => $v) {
        	//$rows[$k] = array_values($rows[$k]);
        	foreach ($v['ShopBrochures'] as $k2 => $v2) {
			
				foreach ($v2['ShopBrochureProducts'] as $k3 => $v3) {
        			$total_quantity += $v3['total_quantity'];
					$total_price += $v3['price_offer'];
				}
				
        	}
        }
        $data['total_quantity'] = $total_quantity;
		$data['total_price'] = $total_price;
        $data['ShopBrochure'] = $rows;
        if ($date) {
	        $data['parchuse_date'] = date('d/m/Y', $date);
        }
        die(json_encode($data));
    }

    public function close_order() {
    	if ($this->request->is('post')) {
    		$User = $this->Session->read('User');
    		$User = $User['User'];
    		$this->UserCart->updateAll(
    			array(
    				'UserCart.purchase_date' => time(),
    			),
    			array(
    				'UserCart.reseller_user_id' => $User['id'],
    				'UserCart.user_cart_status_flags' => 1,
    				'UserCart.purchase_date' => null,
    				'UserCart.order_date !=' => null,
    			)
    		);
    	}
        $this->orders_by_customers();
    }
	
	public function close_order_bycampaign($id) {
    	if ($this->request->is('post')) {
    		$User = $this->Session->read('User');
    		$User = $User['User'];
			
			
			$UserCarts = $this->UserCart->find('all', array(
				'fields' => array('DISTINCT UserCart.id'),
				'joins' => array(
					array('table' => 'user_cart_details','alias' => 'UCD','type' => 'INNER','conditions' => array('UCD.user_cart_id = UserCart.id')),
					array('table' => 'shop_brochure_products','alias' => 'SBP','type' => 'INNER','conditions' => array('SBP.id = UCD.product_id')),
					array('table' => 'campaign_shop_brochures','alias' => 'CSB','type' => 'INNER','conditions' => array('CSB.shop_brochure_id = SBP.shop_brochure_id'))
				),
				'conditions' => array(
					'CSB.campaign_id' => $id,
					'UserCart.reseller_user_id' => $User['id'],
    				'UserCart.user_cart_status_flags' => 1,
    				'UserCart.purchase_date' => null,
    				'UserCart.order_date !=' => null
					)
				)
			);
			
			$usercart_ids = [];
			foreach ($UserCarts as $k => $v) {
				array_push($usercart_ids, $v['UserCart']['id']);
			}
			
			if(count($usercart_ids) > 1){
				$this->UserCart->updateAll( array('UserCart.purchase_date' => time(),), array('UserCart.id in ' => $usercart_ids) );
			} else {
				$this->UserCart->updateAll( array('UserCart.purchase_date' => time(),), array('UserCart.id' => $usercart_ids) );
			}
    		
    	}
        $this->orders_by_products();
    }

    public function order_history() {
        $User = $this->Session->read('User');
    	$User = $User['User'];
        $UserCart = $this->UserCart->find('all', array(
			'joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),
        	'conditions' => array(
        		'reseller_user_id' => $User['id'], 
        		'user_cart_status_flags' => 1,
        		'purchase_date !=' => null,
        		'order_date !=' => null,
        		),
			'order' => array('UserCart.purchase_date DESC')
        	)
        );
		$this->loadModel('ShopBrochureProduct');
        $data = array();
        foreach ($UserCart as $k => $v) {
        	$data[$v['UserCart']['purchase_date']][] = $v;
        }
        $data = array_values($data);
        foreach ($data as $k => $v) {
        	$total_quantity = 0;
        	foreach ($v as $k2 => $v2) {
        		$total_quantity = $v2['UserCart']['total_quantity'] + $total_quantity;
        		$data[$k]['purchase_date'] = date('d/m/Y H:m:s', $data[$k][$k2]['UserCart']['purchase_date']);
        		$data[$k][$k2]['UserCart']['purchase_date_unix'] = $data[$k][$k2]['UserCart']['purchase_date'];
        		$data[$k][$k2]['UserCart']['purchase_date'] = date('d/m/Y H:m:s', $data[$k][$k2]['UserCart']['purchase_date']);
        	}
			
			$Campaign = $this->ShopBrochureProduct->find('first', array(
				'fields' => array('DISTINCT C.id, C.name'),
				'joins' => array(
					array('table' => 'campaign_shop_brochures','alias' => 'CSB','type' => 'INNER','conditions' => array('CSB.shop_brochure_id = ShopBrochureProduct.shop_brochure_id')),
					array('table' => 'campaigns','alias' => 'C','type' => 'INNER','conditions' => array('CSB.campaign_id = C.id'))
				),
				'conditions' => array(
					'ShopBrochureProduct.id' => $data[$k][0]['UserCartDetail'][0]['product_id']
					)
				)
			);
			
			$data[$k]['campaign'] = $Campaign['C']['name'];
			
        	$data[$k]['total_quantity'] = $total_quantity;
        }
        $data = array_values($data);
        die(json_encode($data));
    }
	
	public function client_history() {
        $User = $this->Session->read('User');
    	$User = $User['User'];
        $UserCart = $this->UserCart->find('all', array(
			'joins' => array(array('table' => 'users','alias' => 'U','type' => 'INNER','conditions' => array('U.id = UserCart.user_id'))),
        	'conditions' => array(
        		'user_id' => $User['id'], 
        		'user_cart_status_flags' => 1,
        		'order_date !=' => null,
        		),
			'order' => array('UserCart.order_date DESC')
        	)
        );
		$this->loadModel('ShopBrochureProduct');
        $data = array();
        foreach ($UserCart as $k => $v) {
        	$data[$v['UserCart']['order_date']][] = $v;
        }
        $data = array_values($data);
        foreach ($data as $k => $v) {
        	$total_quantity = 0;
        	foreach ($v as $k2 => $v2) {
        		$total_quantity = $v2['UserCart']['total_quantity'] + $total_quantity;
        		$data[$k]['order_date'] = date('d/m/Y H:m:s', $data[$k][$k2]['UserCart']['order_date']);
        		$data[$k][$k2]['UserCart']['order_date_unix'] = $data[$k][$k2]['UserCart']['order_date'];
        		$data[$k][$k2]['UserCart']['order_date'] = date('d/m/Y H:m:s', $data[$k][$k2]['UserCart']['order_date']);
        	}
			
			$Campaign = $this->ShopBrochureProduct->find('first', array(
				'fields' => array('DISTINCT C.id, C.name'),
				'joins' => array(
					array('table' => 'campaign_shop_brochures','alias' => 'CSB','type' => 'INNER','conditions' => array('CSB.shop_brochure_id = ShopBrochureProduct.shop_brochure_id')),
					array('table' => 'campaigns','alias' => 'C','type' => 'INNER','conditions' => array('CSB.campaign_id = C.id'))
				),
				'conditions' => array(
					'ShopBrochureProduct.id' => $data[$k][0]['UserCartDetail'][0]['product_id']
					)
				)
			);
			
			$data[$k]['campaign'] = $Campaign['C']['name'];
			
        	$data[$k]['total_quantity'] = $total_quantity;
        }
        $data = array_values($data);
        die(json_encode($data));
    }
	
	public function get_orders_values() {
        $User = $this->Session->read('User');
		if(isset($User)){
			$data['User']['OrdersByCustomers'] = $this->get_orders_by_customers(null, $User['User']['id']);
			$data['User']['OrdersByProducts'] = $this->get_orders_by_products(null, $User['User']['id']);
			die(json_encode($data));
		}
		else{
			$this->response->statusCode(401);
		}
    }
	
	public function addshopbrochure_expdate() {
		if($this->request->is('post')) {
			$User = $this->Session->read('User');
			$brochure_id = intval($this->request->data['UserShopBrochure']['shop_brochure_id']);
			$date = strtotime(str_replace('/', '-',$this->request->data['UserShopBrochure']['expiration_date']));
			$expiration_date = date('Y-m-d',$date);
			
			if ($UserShopBrochure = $this->UserShopBrochure->find('first', array('conditions' => array('shop_brochure_id' => $brochure_id, 'user_id' => $User['User']['id'] )))) {
                $this->UserShopBrochure->save(array('id' => $UserShopBrochure['id'], 'expiration_date' => $expiration_date));
				$data["result"] = true;
            }else{
				$UserShopBrochure['UserShopBrochure']['user_id'] = $User['User']['id'];
				$UserShopBrochure['UserShopBrochure']['shop_brochure_id'] = $brochure_id;
				$UserShopBrochure['UserShopBrochure']['expiration_date'] = $expiration_date;
				$UserShopBrochure['UserShopBrochure']['created'] = date('Y-m-d H:i:s');
				
				$this->UserShopBrochure->create();
				if ($UserShopBrochure = $this->UserShopBrochure->save($UserShopBrochure)) {
					$data["result"] = true;
				} else {
					$data["result"] = false;
				}
			}
		} else {
			$data["result"] = false;
		}
		die(json_encode($data));
	}
	
}