<?php

class ShopBrochuresController extends AppController {
	
	public $uses = array('ShopBrochure', 'Offer', 'OfferProductGroup', 'UserCartOffer', 'UserCartDetailOffer', 'ShopBrochureImage', 'ShopBrochureProduct', 'Product', 'Campaign', 'CampaignShopBrochure', 'UserCart', 'UserCartDetail', 'UserResellerGroup');

	public $autoRender = false;

    public function beforeFilter() {
        if (!$this->Session->read('User')) {
            die(json_encode(array('login' => false)));
        }
    }

    public function read($id = null) {
        if ($this->request->is('get') AND $id AND is_numeric($id)) {
            $ShopBrochure = $this->ShopBrochure->find('first', array('conditions'=> array('ShopBrochure.id' => $id)));
            foreach ($ShopBrochure['ShopBrochureImage'] as $k => $v) {
				if($v['order'] == 0){
				$ShopBrochure['ShopBrochureImage'][$k]['url'] = str_replace('small', 'original', $v['name']);
				} else {
                $ShopBrochure['ShopBrochureImage'][$k]['url'] = str_replace('site', 'cms', Router::url('/images/brochure_' . $id . '/original/' . $v['name'], true));
				}
            }
            $Campaign = $this->CampaignShopBrochure->find('first', array('conditions' => array('shop_brochure_id' => $id)));
            $ShopBrochure['Campaign'] = $Campaign['Campaign'];
            die(json_encode($ShopBrochure));
        }
    }

    public function read_product($id = null) {
        if ($this->request->is('get') AND $id AND is_numeric($id)) {
            $ShopBrochureProduct = $this->ShopBrochureProduct->find('first', array('conditions'=> array('ShopBrochureProduct.id' => $id)));
            die(json_encode($ShopBrochureProduct));
        }
    }

	public function read_products($id = null) {
        if($this->request->is('get') AND $id AND is_numeric($id)) {
            $ShopBrochureProduct = $this->ShopBrochureProduct->find('all', array('conditions' => array('shop_brochure_image_id' => $id, 'ShopBrochureProduct.father_id' => null)));
            foreach ($ShopBrochureProduct as $k => $v) {
                $ShopBrochureProduct[$k]['ShopBrochureProduct']['coords'] = json_decode($v['ShopBrochureProduct']['coords']);
            }
            die(json_encode($ShopBrochureProduct));
        }
    }

    public function read_versions($id) {
        if($this->request->is('get') AND $id AND is_numeric($id)) {
            $ShopBrochureProduct = $this->ShopBrochureProduct->find('all', array('conditions' => array('ShopBrochureProduct.father_id' => $id), 'order' => 'ShopBrochureProduct.id'));
            die(json_encode($ShopBrochureProduct));
        }
    }

    public function delete_product() {
        $data['result'] = false;
        if($this->request->is('post') AND $this->request->data('id') AND is_numeric($this->request->data('id'))) {
			if($UserCartDetail = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $this->request->data['id'])))){
			
				//consulto si tiene una oferta aplicada para recalcular las cantidades
				$this->UserCartOffer->Behaviors->load('Containable');
				$this->UserCartOffer->contain(array('UserCartDetailOffer'));
				$UserCartOffers = $this->UserCartOffer->find('all', array(
					'joins' => array(
						array('table' => 'user_cart_detail_offers','alias' => 'D','type' => 'INNER','conditions' => array('D.user_cart_offer_id = UserCartOffer.id')),
						array('table' => 'offers','alias' => 'O','type' => 'INNER','conditions' => array('O.id = UserCartOffer.offer_id'))
					),
					'fields' => array('DISTINCT UserCartOffer.*'),
					'conditions' => array('D.user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'])));
				
				if(count($UserCartOffers) > 0){
					$tempquantity = 0;
					foreach ($UserCartOffers as $k => $v) {
						foreach ($v['UserCartDetailOffer'] as $k2 => $v2) {
							
							$tempquantity = $v2['quantity'];
							if($DetailTemp = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $v2['user_cart_detail_id'])))){
								$DetailTemp['UserCartDetail']['quantity_available'] += $tempquantity;
								$this->UserCartDetail->save($DetailTemp);	
							}
							
							$this->UserCartDetailOffer->query('delete from user_cart_detail_offers where id = '.$v2['id']);
						}
						$this->UserCartDetailOffer->query('delete from user_cart_offers where id = '.$v['UserCartOffer']['id']);
					}
				}
			
				$user_cart_id = $UserCartDetail['UserCartDetail']['user_cart_id'];
				if ($this->UserCartDetail->delete($this->request->data('id'))) {
					$data = $this->read_cart();
					$data["total_quantity"] = $this->refreshUserTotalQuantity($user_cart_id);
				}
			}
        }
        die(json_encode($data));
    }
	
	public function delete_order_product() {
        $data['result'] = false;
        if($this->request->is('post') AND $this->request->data('id') AND is_numeric($this->request->data('id'))) {
			if($UserCartDetail = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $this->request->data['id'])))){
			
				//consulto si tiene una oferta aplicada para recalcular las cantidades
				$this->UserCartOffer->Behaviors->load('Containable');
				$this->UserCartOffer->contain(array('UserCartDetailOffer'));
				$UserCartOffers = $this->UserCartOffer->find('all', array(
					'joins' => array(
						array('table' => 'user_cart_detail_offers','alias' => 'D','type' => 'INNER','conditions' => array('D.user_cart_offer_id = UserCartOffer.id')),
						array('table' => 'offers','alias' => 'O','type' => 'INNER','conditions' => array('O.id = UserCartOffer.offer_id'))
					),
					'fields' => array('DISTINCT UserCartOffer.*'),
					'conditions' => array('D.user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'])));
				
				if(count($UserCartOffers) > 0){
					$tempquantity = 0;
					foreach ($UserCartOffers as $k => $v) {
						foreach ($v['UserCartDetailOffer'] as $k2 => $v2) {
							
							$tempquantity = $v2['quantity'];
							if($DetailTemp = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $v2['user_cart_detail_id'])))){
								$DetailTemp['UserCartDetail']['quantity_available'] += $tempquantity;
								$this->UserCartDetail->save($DetailTemp);	
							}
							
							$this->UserCartDetailOffer->query('delete from user_cart_detail_offers where id = '.$v2['id']);
						}
						$this->UserCartDetailOffer->query('delete from user_cart_offers where id = '.$v['UserCartOffer']['id']);
					}
				}
				$user_cart_id = $UserCartDetail['UserCartDetail']['user_cart_id'];
				if ($this->UserCartDetail->delete($this->request->data('id'))) {
					$data['result'] = true;
					$data["total_quantity"] = $this->refreshUserTotalQuantity($user_cart_id);
				}
				
			}
        }
        die(json_encode($data));
    }
	
	private function refreshUserTotalQuantity($user_cart_id){
	
		$this->UserCart->Behaviors->load('Containable');
		$this->UserCart->contain(array(
			'UserCartDetail' => array(
				'ShopBrochureProduct'
				)
			));
		$UserCart = $this->UserCart->find('all', array(
        	'conditions' => array(
        		'user_id' => $this->Session->read('User')['User']['id'], 
        		'user_cart_status_flags' => 0
        		)
        	)
        );
        $total_quantity = 0;
		$total_price = 0;
		$quantity = 0;
		$price = 0;
        foreach ($UserCart as $k => $v) {
			$quantity = 0;
			$price = 0;
            foreach ($v['UserCartDetail'] as $k2 => $v2) {
            	$quantity += $UserCart[$k]['UserCartDetail'][$k2]['quantity'];
				$price += ($UserCart[$k]['UserCartDetail'][$k2]['quantity'] * $UserCart[$k]['UserCartDetail'][$k2]['ShopBrochureProduct']['price']);
            }
			$total_quantity += $quantity;
			$total_price += $price;
			
			$this->UserCart->id = $v['UserCart']['id'];
			$this->UserCart->save(array('total_quantity' => $quantity, 'total_price' => $price));
        }
		
		return $total_quantity;
	}

	public function cart_update_qty() {
		if($this->request->is('post')) {
			$qty = intval($this->request->data['UserCartDetail']['quantity']);
			if ($UserCartDetail = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $this->request->data['UserCartDetail']['id'])))) {
                
				$old_qty = $UserCartDetail['UserCartDetail']['quantity'];
				$add_qty = $qty - $old_qty;
				
				$this->UserCartDetail->save(array('id' => $this->request->data['UserCartDetail']['id'], 'quantity' => $qty, 'quantity_available' => $add_qty+$UserCartDetail['UserCartDetail']['quantity_available']));
				
				$UserCartDetail['UserCartDetail']['quantity'] = $qty;
				$UserCartDetail['UserCartDetail']['quantity_available'] = $add_qty+$UserCartDetail['UserCartDetail']['quantity_available'];
				
				$data["UserCartDetail"] = $UserCartDetail;
				
				//validar ofertas / recalcular ofertas
				$data = $this->offerValidate($UserCartDetail, $qty > $old_qty, $data);
				
				$data["save"] = true;
				$data["total_quantity"] = $this->refreshUserTotalQuantity($UserCartDetail['UserCartDetail']['user_cart_id']);
				
            }
			else{
				$data["save"] = false;
			}
		} else {
			$data["save"] = false;
		}
		die(json_encode($data));
	}
	
	private function offerValidate($UserCartDetail, $addqty, $data){
		
		
		//consulto si tiene una oferta aplicada para recalcular las cantidades
		$this->UserCartOffer->Behaviors->load('Containable');
		$this->UserCartOffer->contain(array('UserCartDetailOffer'));
		$UserCartOffers = $this->UserCartOffer->find('all', array(
			'joins' => array(
				array('table' => 'user_cart_detail_offers','alias' => 'D','type' => 'INNER','conditions' => array('D.user_cart_offer_id = UserCartOffer.id')),
				array('table' => 'offers','alias' => 'O','type' => 'INNER','conditions' => array('O.id = UserCartOffer.offer_id'))
			),
			'fields' => array('DISTINCT UserCartOffer.*'),
			'conditions' => array('D.user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'])));
		
		//$data['TempUserCartOffersDel'] = $UserCartOffers;
		
		if(count($UserCartOffers) > 0){
			$tempquantity = 0;
			foreach ($UserCartOffers as $k => $v) {
				foreach ($v['UserCartDetailOffer'] as $k2 => $v2) {
					
					$tempquantity = $v2['quantity'];
					if($DetailTemp = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $v2['user_cart_detail_id'])))){
						$DetailTemp['UserCartDetail']['quantity_available'] += $tempquantity;
						$this->UserCartDetail->save($DetailTemp);	
					}
					
					$this->UserCartDetailOffer->query('delete from user_cart_detail_offers where id = '.$v2['id']);
				}
				$this->UserCartDetailOffer->query('delete from user_cart_offers where id = '.$v['UserCartOffer']['id']);
			}
		}
		//reload data
		$UserCartDetail = $this->UserCartDetail->find('first', array('conditions' => array('UserCartDetail.id' => $UserCartDetail['UserCartDetail']['id'])));
		//$data['TempUserCartDetail'] = $UserCartDetail;
	
		//verifica oferta
		$this->Offer->Behaviors->load('Containable');
		$this->Offer->contain(array('OfferProductGroup','OfferProductGroupContingency'));
		$Offers = $this->Offer->find('all', array(
			'fields' => array('DISTINCT Offer.*'),
			'joins' => array(array('table' => 'offer_product_groups','alias' => 'G','type' => 'INNER','conditions' => array('G.offer_id = Offer.id'))),
			'conditions' => array('G.shop_brochure_product_id' => $UserCartDetail['UserCartDetail']['product_id'])));
		
		//$data['TempOffers'] = $Offers;
		
		if(count($Offers) > 0){
		
			$ShopBrochureProduct = $this->ShopBrochureProduct->find('first', array('conditions' => array('ShopBrochureProduct.id' => $UserCartDetail['UserCartDetail']['product_id'])));
		
			foreach ($Offers as $k => $Offer){
			
				if(intval($Offer['Offer']['offer_type_id']) == 1 || intval($Offer['Offer']['offer_type_id']) == 2){//Oferta 3x2 y Oferta segundo con descuento
					
					$offer_count = 3; $offer_price = 2*$ShopBrochureProduct['ShopBrochureProduct']['price'];
					if(intval($Offer['Offer']['offer_type_id']) == 2){
						$offer_count = 2;
						$offer_price = $ShopBrochureProduct['ShopBrochureProduct']['price']+$Offer['OfferProductGroup'][0]['price'];
					}
					
						if($UserCartDetail['UserCartDetail']['quantity_available'] >= $offer_count){
							while($UserCartDetail['UserCartDetail']['quantity_available'] >= $offer_count){
								$this->UserCartOffer->create();
								$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => $offer_count,'price' => $ShopBrochureProduct['ShopBrochureProduct']['price'],'total_price' => $offer_price));
								
								$pricedetail = 0;
								if(intval($Offer['Offer']['offer_type_id']) == 1){
									$pricedetail = $offer_price/3;
								} else {
									$pricedetail = $ShopBrochureProduct['ShopBrochureProduct']['price'];
								}
								
								$this->UserCartDetailOffer->create();
								$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => (intval($Offer['Offer']['offer_type_id']) == 2 ? 1 : $offer_count), 'price' => $pricedetail));
								
								if(intval($Offer['Offer']['offer_type_id']) == 2){
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $Offer['OfferProductGroup'][0]['price']));
								}
																	
								$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] - $offer_count;
							}
							$this->UserCartDetail->save($UserCartDetail);
						}
						
						if(!($Offer['Offer']['todos']) && $UserCartDetail['UserCartDetail']['quantity_available'] > 0){
						
							//Si aplica con productos distintos y le quedan productos disponibles							
							$product_ids = [];
							foreach ($Offer['OfferProductGroup'] as $k => $v) {
								if($v['shop_brochure_product_id'] != $UserCartDetail['UserCartDetail']['product_id']){
									array_push($product_ids, $v['shop_brochure_product_id']);
								}
							}
							$product_condition = 'UserCartDetail.product_id' . (count($product_ids) > 1 ? ' in' : '');
							//consulta todos los productos cargados para el carrito que corresponden con los de la oferta y que tienen productos disponibles (que no se les aplic� oferta)
							$UserCartDetails = $this->UserCartDetail->find('all', array('conditions' => array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],$product_condition => $product_ids, 'quantity_available >' => 0 )));
							
							//$data['UserCartDetails'] = $UserCartDetails;
							//$data['UserCartDetailsIDs'] = $product_ids;
							
							if(count($UserCartDetails) > 0){
									
								//obtengo los productos para la oferta 3x2
								$quantity = $UserCartDetail['UserCartDetail']['quantity_available'];
								$products = [];
								$completed = false;
								foreach ($UserCartDetails as $k => $v) {
									$tempquantity = 0;
									$tempquantity_available = 0;
									if($quantity + $v['UserCartDetail']['quantity_available'] >= $offer_count){
										$tempquantity =  $offer_count - $quantity;// total de productos a descontar por oferta
										$tempquantity_available = $v['UserCartDetail']['quantity_available'] - $tempquantity;
										$completed = true;
									} else {
										$tempquantity = $v['UserCartDetail']['quantity_available'];
										$tempquantity_available = $v['UserCartDetail']['quantity_available'];
										$quantity += $v['UserCartDetail']['quantity_available'];
									}										
									array_push($products, array($v['UserCartDetail']['id'], $tempquantity, $tempquantity_available));										
									if($completed){	break; }
								}
								
								if($completed){										
									//Guardo la oferta
									$this->UserCartOffer->create();
									$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => $offer_count,'price' => $ShopBrochureProduct['ShopBrochureProduct']['price'],'total_price' => $offer_price));
									
									$pricedetail = 0;
									if(intval($Offer['Offer']['offer_type_id']) == 1){
										$pricedetail = $offer_price/3;
									} else {
										$pricedetail = $ShopBrochureProduct['ShopBrochureProduct']['price'];
									}
									
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => $UserCartDetail['UserCartDetail']['quantity_available'], 'price' => $pricedetail));
									
									//guardo el detalle de la oferta
									foreach ($products as $k => $v) {
									
										$pricedetail = 0;
										if(intval($Offer['Offer']['offer_type_id']) == 1){
											$pricedetail = $offer_price/3;
										} else {
											$pricedetail = $Offer['OfferProductGroup'][0]['price'];
										}
									
										$this->UserCartDetailOffer->create();
										$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $v[0],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => $v[1], 'price' =>$pricedetail));
										//actualizo la cantidad disponible del producto
										$this->UserCartDetail->save(array('id' => $v[0],'quantity_available' => $v[2]));											
									}
									
									//actualizo la cantidad disponible del producto actual
									$UserCartDetail['UserCartDetail']['quantity_available'] = 0;
									$this->UserCartDetail->save($UserCartDetail);
									
								}//en if completed
								
							}
						}//end if todos
					}// end if 3x2 y 2x1
					else if(intval($Offer['Offer']['offer_type_id']) == 4){//Oferta multiple
						
						//aplico la oferta segun la cantidad
						$offer_qtys = [];
						foreach ($Offer['OfferProductGroup'] as $k => $v) {
							$valid = true;
							foreach ($offer_qtys as $q) {
								if($q[0] == $v['quantity']){
									$valid = false;
									break;
								}
							}
							if($valid){
								array_push($offer_qtys, array($v['quantity'], $v['price']));
							}
						}
						$tmp = Array(); 
						foreach($offer_qtys as &$ma) $tmp[] = &$ma[0]; 
						array_multisort($tmp, SORT_DESC, SORT_NUMERIC, $offer_qtys);
						$data['Offerqtys'] = $offer_qtys;
						
						$apply_offer = true;
						while($UserCartDetail['UserCartDetail']['quantity_available'] > 0 && $apply_offer){
							$apply_offer = false;
							foreach($offer_qtys as $oq) {
								if($UserCartDetail['UserCartDetail']['quantity_available'] >= $oq[0]){
									$this->UserCartOffer->create();
									$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => $oq[0],'price' => $oq[1],'total_price' => $oq[0]*$oq[1]));
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => $oq[0],'price' => $oq[1]));
									
									$UserCartDetail['UserCartDetail']['quantity_available'] -= $oq[0];
									$this->UserCartDetail->save($UserCartDetail);
									$apply_offer = true;
									break;
								}
							}
						}
						
					} //end oferta multiple
					else if(intval($Offer['Offer']['offer_type_id']) == 3){//Oferta tipo contingencia
					
						//primero se verifica en que grupo esta el producto agregado (group o conringency)
						$product_A = false;
						$products_A = [];
						$product_B = false;
						$products_B = [];
						foreach ($Offer['OfferProductGroup'] as $k => $v) {
							if($v['shop_brochure_product_id'] == $UserCartDetail['UserCartDetail']['product_id']){
								$product_A = true;
							}
							array_push($products_A, $v['shop_brochure_product_id']);
						}							
						foreach ($Offer['OfferProductGroupContingency'] as $k => $v) {
							if(!$product_A){
								if($v['shop_brochure_product_id'] == $UserCartDetail['UserCartDetail']['product_id']){
									$product_B = true;
								}
							}
							array_push($products_B, $v['shop_brochure_product_id']);
						}

						if($product_A){
							while ($UserCartDetail['UserCartDetail']['quantity_available'] > 0 && $detail_product_available = $this->UserCartDetail->find('first', array('conditions' => array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'], 'UserCartDetail.quantity_available >' => 0, 'UserCartDetail.product_id'.(count($products_B) > 1 ? ' IN' : '') => $products_B)))) {

								$this->UserCartOffer->create();
								$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => 2,'price' => $Offer['OfferProductGroup'][0]['price'],'total_price' => $Offer['OfferProductGroup'][0]['price']+$ShopBrochureProduct['ShopBrochureProduct']['price']));
								
								//productA
								$this->UserCartDetailOffer->create();
								$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $ShopBrochureProduct['ShopBrochureProduct']['price']));

								$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] - 1;
								$this->UserCartDetail->save($UserCartDetail);
								
								//productB - discount
								$this->UserCartDetailOffer->create();
								$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $detail_product_available['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $Offer['OfferProductGroup'][0]['price']));
								$detail_product_available['UserCartDetail']['quantity_available'] = $detail_product_available['UserCartDetail']['quantity_available'] - 1;
								$this->UserCartDetail->save($detail_product_available);
							}

						} else if($product_B){
							while ($UserCartDetail['UserCartDetail']['quantity_available'] > 0 && $detail_product_available = $this->UserCartDetail->find('first', array('conditions' => array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'], 'UserCartDetail.quantity_available >' => 0, 'UserCartDetail.product_id'.(count($products_A) > 1 ? ' IN' : '') => $products_A)))) {

								//get price productA
								$shopbrochureproductA = $this->ShopBrochureProduct->find('first', array('conditions' => array('ShopBrochureProduct.id' => $detail_product_available['UserCartDetail']['product_id'])));

								$this->UserCartOffer->create();
								$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => 2,'price' => $Offer['OfferProductGroup'][0]['price'],'total_price' => $Offer['OfferProductGroup'][0]['price']+$shopbrochureproductA['ShopBrochureProduct']['price']));
								
								//productA
								$this->UserCartDetailOffer->create();
								$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $detail_product_available['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $shopbrochureproductA['ShopBrochureProduct']['price']));
								$detail_product_available['UserCartDetail']['quantity_available'] = $detail_product_available['UserCartDetail']['quantity_available'] - 1;
								$this->UserCartDetail->save($detail_product_available);

								//productB - discount
								$this->UserCartDetailOffer->create();
								$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $Offer['OfferProductGroup'][0]['price']));

								$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] - 1;
								$this->UserCartDetail->save($UserCartDetail);
								
							}
						}
					
					}
			}	
		}
		return $data;
	}

    public function cart() {
        $data['result'] = false;
        if($this->request->is('post')) {
		
			//post
            $User = $this->Session->read('User');
            $this->UserCart->Behaviors->load('Containable');
            $this->UserCart->contain(array('UserCartDetail' => array('ShopBrochureProduct')));
			
			//get campaign entity
			$Campaign = $this->ShopBrochureProduct->find('first', array(
				'fields' => array('DISTINCT CSB.campaign_id'),
				'joins' => array(array('table' => 'campaign_shop_brochures','alias' => 'CSB','type' => 'INNER','conditions' => array('CSB.shop_brochure_id = ShopBrochureProduct.shop_brochure_id'))),
				'conditions' => array('ShopBrochureProduct.id' => $this->request->data['UserCartDeteail']['product_id']))
			);
			
			//trae el carrito por campa�a si ya est� creado
			$UserCart = $this->UserCart->find('first', array(
					'joins' => array(
						array('table' => 'user_cart_details','alias' => 'UCD','type' => 'INNER','conditions' => array('UCD.user_cart_id = UserCart.id')),
						array('table' => 'shop_brochure_products','alias' => 'SBP','type' => 'INNER','conditions' => array('SBP.id = UCD.product_id')),
						array('table' => 'campaign_shop_brochures','alias' => 'CSB','type' => 'INNER','conditions' => array('CSB.shop_brochure_id = SBP.shop_brochure_id'))
					),
					'conditions' => array(
						'CSB.campaign_id' => $Campaign['CSB']['campaign_id'],
						'user_id' => $User['User']['id'],
						'user_cart_status_flags' => 0
					)
				)
			);
			
			//si el carrito no existe lo crea
            if (!$UserCart) {			
				$UserCart = $this->UserCart->find('first', array(
						'conditions' => array(
							'total_quantity' => 0,
							'total_price' => 0,
							'user_id' => $User['User']['id'],
							'user_cart_status_flags' => 0
						)
					)
				);			
				if (!$UserCart) {
                	$this->UserCart->create();
                	$UserCart = $this->UserCart->save(array('user_id' => $User['User']['id']));
				}
            }
			
			//consulta si existe el producto en details, sino existe lo crea
            if (!$UserCartDetail = $this->UserCartDetail->find('first', array('conditions' => array('user_cart_id' => $UserCart['UserCart']['id'], 'UserCartDetail.product_id' => $this->request->data['UserCartDeteail']['product_id'])))) {
                $this->UserCartDetail->create();
                if ($UserCartDetail = $this->UserCartDetail->save(array('user_cart_id' => $UserCart['UserCart']['id'], 'product_id' => $this->request->data['UserCartDeteail']['product_id']))) {
                    $data['result'] = true;
                }
            } else {
                $this->UserCartDetail->id = $UserCartDetail['UserCartDetail']['id'];
                $UserCartDetail['UserCartDetail']['quantity'] = $UserCartDetail['UserCartDetail']['quantity'] + 1;				
				$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] + 1;
                if ($this->UserCartDetail->exists() AND $this->UserCartDetail->save($UserCartDetail)) {
                    $data['result'] = true;
                }
            }
			
			//verifica oferta
			$this->Offer->Behaviors->load('Containable');
            $this->Offer->contain(array('OfferProductGroup','OfferProductGroupContingency'));
			$Offers = $this->Offer->find('all', array(
				'fields' => array('DISTINCT Offer.*'),
				'joins' => array(array('table' => 'offer_product_groups','alias' => 'G','type' => 'INNER','conditions' => array('G.offer_id = Offer.id'))
					,array('table' => 'offer_product_group_contingencies','alias' => 'C','type' => 'LEFT','conditions' => array('C.offer_id = Offer.id'))),
				'conditions' => array('OR' => array('G.shop_brochure_product_id' => $this->request->data['UserCartDeteail']['product_id'], 'C.shop_brochure_product_id'=> $this->request->data['UserCartDeteail']['product_id']) )));
			
			$data['Offers'] = $Offers;
			
			if(count($Offers) > 0){
			
				$ShopBrochureProduct = $this->ShopBrochureProduct->find('first', array('conditions' => array('ShopBrochureProduct.id' => $UserCartDetail['UserCartDetail']['product_id'])));
			
				foreach ($Offers as $k => $Offer){
				
					if(intval($Offer['Offer']['offer_type_id']) == 1 || intval($Offer['Offer']['offer_type_id']) == 2){//Oferta 3x2 y Oferta segundo con descuento
						
						$offer_count = 3; $offer_price = 2*$ShopBrochureProduct['ShopBrochureProduct']['price'];
						if(intval($Offer['Offer']['offer_type_id']) == 2){
							$offer_count = 2;
							$offer_price = $ShopBrochureProduct['ShopBrochureProduct']['price']+$Offer['OfferProductGroup'][0]['price'];
						}
						
							if($UserCartDetail['UserCartDetail']['quantity_available'] >= $offer_count){
								while($UserCartDetail['UserCartDetail']['quantity_available'] >= $offer_count){
									$this->UserCartOffer->create();
                					$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => $offer_count,'price' => $ShopBrochureProduct['ShopBrochureProduct']['price'],'total_price' => $offer_price));
									
									$pricedetail = 0;
									if(intval($Offer['Offer']['offer_type_id']) == 1){
										$pricedetail = $offer_price/3;
									} else {
										$pricedetail = $ShopBrochureProduct['ShopBrochureProduct']['price'];
									}
									
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => (intval($Offer['Offer']['offer_type_id']) == 2 ? 1 : $offer_count), 'price' => $pricedetail));
									
									if(intval($Offer['Offer']['offer_type_id']) == 2){
										$this->UserCartDetailOffer->create();
										$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $Offer['OfferProductGroup'][0]['price']));
									}									
																	
									$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] - $offer_count;
								}
								$this->UserCartDetail->save($UserCartDetail);
							}
							
							if(!($Offer['Offer']['todos']) && $UserCartDetail['UserCartDetail']['quantity_available'] > 0){
							
								//Si aplica con productos distintos y le quedan productos disponibles							
								$product_ids = [];
								foreach ($Offer['OfferProductGroup'] as $k => $v) {
									if($v['shop_brochure_product_id'] != $UserCartDetail['UserCartDetail']['product_id']){
										array_push($product_ids, $v['shop_brochure_product_id']);
									}
								}
								$product_condition = 'UserCartDetail.product_id' . (count($product_ids) > 1 ? ' in' : '');
								//consulta todos los productos cargados para el carrito que corresponden con los de la oferta y que tienen productos disponibles (que no se les aplic� oferta)
								$UserCartDetails = $this->UserCartDetail->find('all', array('conditions' => array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],$product_condition => $product_ids)));
								
								//$data['UserCartDetails'] = $UserCartDetails;
								//$data['UserCartDetailsIDs'] = $product_ids;
								
								if(count($UserCartDetails) > 0){
										
									//obtengo los productos para la oferta 3x2
									$quantity = $UserCartDetail['UserCartDetail']['quantity_available'];
									$products = [];
									$completed = false;
									foreach ($UserCartDetails as $k => $v) {
										$tempquantity = 0;
										$tempquantity_available = 0;
										if($quantity + $v['UserCartDetail']['quantity_available'] >= $offer_count){
											$tempquantity =  $offer_count - $quantity;// total de productos a descontar por oferta
											$tempquantity_available = $v['UserCartDetail']['quantity_available'] - $tempquantity;
											$completed = true;
										} else {
											$tempquantity = $v['UserCartDetail']['quantity_available'];
											$tempquantity_available = $v['UserCartDetail']['quantity_available'];
											$quantity += $v['UserCartDetail']['quantity_available'];
										}										
										array_push($products, array($v['UserCartDetail']['id'], $tempquantity, $tempquantity_available));										
										if($completed){	break; }
									}
									
									if($completed){										
										//Guardo la oferta
										$this->UserCartOffer->create();
										$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => $offer_count,'price' => $ShopBrochureProduct['ShopBrochureProduct']['price'],'total_price' => $offer_price));
										
										$pricedetail = 0;
										if(intval($Offer['Offer']['offer_type_id']) == 1){
											$pricedetail = $offer_price/3;
										} else {
											$pricedetail = $ShopBrochureProduct['ShopBrochureProduct']['price'];
										}
										
										$this->UserCartDetailOffer->create();
										$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => $UserCartDetail['UserCartDetail']['quantity_available'], 'price' => $pricedetail));
										
										//guardo el detalle de la oferta
										foreach ($products as $k => $v) {
										
											$pricedetail = 0;
											if(intval($Offer['Offer']['offer_type_id']) == 1){
												$pricedetail = $offer_price/3;
											} else {
												$pricedetail = $Offer['OfferProductGroup'][0]['price'];
											}
										
											$this->UserCartDetailOffer->create();
											$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $v[0],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => $v[1], 'price' =>$pricedetail ));
											//actualizo la cantidad disponible del producto
											$this->UserCartDetail->save(array('id' => $v[0],'quantity_available' => $v[2]));											
										}
										
										//actualizo la cantidad disponible del producto actual
										$UserCartDetail['UserCartDetail']['quantity_available'] = 0;
										$this->UserCartDetail->save($UserCartDetail);
										
									}//en if completed
									
								}
							}//end if todos
						}// end if 3x2 y 2x1
						else if(intval($Offer['Offer']['offer_type_id']) == 4){//Oferta multiple
						
							//consulto si tiene una oferta multiple aplicada para recalcular las cantidades
							$this->UserCartOffer->Behaviors->load('Containable');
        					$this->UserCartOffer->contain(array('UserCartDetailOffer'));
							$UserCartOffers = $this->UserCartOffer->find('all', array(
								'joins' => array(
									array('table' => 'user_cart_detail_offers','alias' => 'D','type' => 'INNER','conditions' => array('D.user_cart_offer_id = UserCartOffer.id')),
									array('table' => 'offers','alias' => 'O','type' => 'INNER','conditions' => array('O.id = UserCartOffer.offer_id'))
								),
								'conditions' => array('O.offer_type_id' => 4,'D.user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'])));
							
							$data['UserCartOffers'] = $UserCartOffers;
							
							if(count($UserCartOffers) > 0){
								$tempquantity = 0;
								foreach ($UserCartOffers as $k => $v) {
									foreach ($v['UserCartDetailOffer'] as $k2 => $v2) {
										$tempquantity += $v2['quantity'];
										$this->UserCartDetailOffer->query('delete from user_cart_detail_offers where id = '.$v2['id']);
									}
									$this->UserCartDetailOffer->query('delete from user_cart_offers where id = '.$v['UserCartOffer']['id']);
								}
								if($tempquantity > 0){
									$UserCartDetail['UserCartDetail']['quantity_available'] += $tempquantity;
									$this->UserCartDetail->save($UserCartDetail);
								}
							}
							
							//aplico la oferta segun la cantidad
							$offer_qtys = [];
							foreach ($Offer['OfferProductGroup'] as $k => $v) {
								$valid = true;
								foreach ($offer_qtys as $q) {
									if($q[0] == $v['quantity']){
										$valid = false;
										break;
									}
								}
								if($valid){
									array_push($offer_qtys, array($v['quantity'], $v['price']));
								}
							}
							$tmp = Array(); 
							foreach($offer_qtys as &$ma) $tmp[] = &$ma[0]; 
							array_multisort($tmp, SORT_DESC, SORT_NUMERIC, $offer_qtys);
							$data['Offerqtys'] = $offer_qtys;
							
							$apply_offer = true;
							while($UserCartDetail['UserCartDetail']['quantity_available'] > 0 && $apply_offer){
								$apply_offer = false;
								foreach($offer_qtys as $oq) {
									if($UserCartDetail['UserCartDetail']['quantity_available'] >= $oq[0]){
										$this->UserCartOffer->create();
										$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => $oq[0],'price' => $oq[1],'total_price' => $oq[0]*$oq[1]));
										$this->UserCartDetailOffer->create();
										$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => $oq[0],'price' => $oq[1]));
										
										$UserCartDetail['UserCartDetail']['quantity_available'] -= $oq[0];
										$this->UserCartDetail->save($UserCartDetail);
										$apply_offer = true;
										break;
									}
								}
							}
							
						} //end oferta multiple
						else if(intval($Offer['Offer']['offer_type_id']) == 3){//Oferta tipo contingencia

							//primero se verifica en que grupo esta el producto agregado (group o conringency)
							$product_A = false;
							$products_A = [];
							$product_B = false;
							$products_B = [];
							foreach ($Offer['OfferProductGroup'] as $k => $v) {
								if($v['shop_brochure_product_id'] == $this->request->data['UserCartDeteail']['product_id']){
									$product_A = true;
								}
								array_push($products_A, $v['shop_brochure_product_id']);
							}							
							foreach ($Offer['OfferProductGroupContingency'] as $k => $v) {
								if(!$product_A){
									if($v['shop_brochure_product_id'] == $this->request->data['UserCartDeteail']['product_id']){
										$product_B = true;
									}
								}
								array_push($products_B, $v['shop_brochure_product_id']);
							}

							if($product_A){
								while ($UserCartDetail['UserCartDetail']['quantity_available'] > 0 && $detail_product_available = $this->UserCartDetail->find('first', array('conditions' => array('user_cart_id' => $UserCart['UserCart']['id'], 'UserCartDetail.quantity_available >' => 0, 'UserCartDetail.product_id'.(count($products_B) > 1 ? ' IN' : '') => $products_B)))) {

									$this->UserCartOffer->create();
									$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => 2,'price' => $Offer['OfferProductGroup'][0]['price'],'total_price' => $Offer['OfferProductGroup'][0]['price']+$ShopBrochureProduct['ShopBrochureProduct']['price']));
									
									//productA
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $ShopBrochureProduct['ShopBrochureProduct']['price']));

									$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] - 1;
									$this->UserCartDetail->save($UserCartDetail);
									
									//productB - discount
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $detail_product_available['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $Offer['OfferProductGroup'][0]['price']));
									$detail_product_available['UserCartDetail']['quantity_available'] = $detail_product_available['UserCartDetail']['quantity_available'] - 1;
									$this->UserCartDetail->save($detail_product_available);
								}

							} else if($product_B){
								while ($UserCartDetail['UserCartDetail']['quantity_available'] > 0 && $detail_product_available = $this->UserCartDetail->find('first', array('conditions' => array('user_cart_id' => $UserCart['UserCart']['id'], 'UserCartDetail.quantity_available >' => 0, 'UserCartDetail.product_id'.(count($products_A) > 1 ? ' IN' : '') => $products_A)))) {

									//get price productA
									$shopbrochureproductA = $this->ShopBrochureProduct->find('first', array('conditions' => array('ShopBrochureProduct.id' => $detail_product_available['UserCartDetail']['product_id'])));

									$this->UserCartOffer->create();
									$UserCartOffer = $this->UserCartOffer->save(array('user_cart_id' => $UserCartDetail['UserCartDetail']['user_cart_id'],'offer_id' => $Offer['Offer']['id'],'quantity' => 2,'price' => $Offer['OfferProductGroup'][0]['price'],'total_price' => $Offer['OfferProductGroup'][0]['price']+$shopbrochureproductA['ShopBrochureProduct']['price']));
									
									//productA
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $detail_product_available['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $shopbrochureproductA['ShopBrochureProduct']['price']));
									$detail_product_available['UserCartDetail']['quantity_available'] = $detail_product_available['UserCartDetail']['quantity_available'] - 1;
									$this->UserCartDetail->save($detail_product_available);

									//productB - discount
									$this->UserCartDetailOffer->create();
									$UserCartDetailOffers = $this->UserCartDetailOffer->save(array('user_cart_detail_id' => $UserCartDetail['UserCartDetail']['id'],'user_cart_offer_id' => $UserCartOffer['UserCartOffer']['id'], 'quantity' => 1, 'price' => $Offer['OfferProductGroup'][0]['price']));

									$UserCartDetail['UserCartDetail']['quantity_available'] = $UserCartDetail['UserCartDetail']['quantity_available'] - 1;
									$this->UserCartDetail->save($UserCartDetail);
									
								}
							}
						
						}//End tipo contingencia
									
				}
				
			}
			
			//recalcula precios y cantidades
            $quantity = 0;
            $price = 0;
			$total_quantity = 0;
            $total_price = 0;
            $this->UserCart->Behaviors->load('Containable');
            $this->UserCart->contain(array('UserCartDetail' => array('ShopBrochureProduct')));
			
            $UserCarts = $this->UserCart->find('all', array('conditions' => array('user_id' => $User['User']['id'], 'user_cart_status_flags' => 0)));
			foreach ($UserCarts as $k1 => $UserCart) {
				$quantity = 0;
 		        $price = 0;
				foreach ($UserCart['UserCartDetail'] as $k => $v) {
					$quantity = $quantity + $v['quantity'];
					$price = $price + ($v['quantity'] * $v['ShopBrochureProduct']['price']);
				}
				$this->UserCart->id = $UserCart['UserCart']['id'];
            	$this->UserCart->save(array('total_quantity' => $quantity, 'total_price' => $price))['UserCart'];
				$total_quantity += $quantity;
            	$total_price += $price;
			} 
			$data['UserCart']['total_quantity'] = $total_quantity;
			$data['UserCart']['total_price'] = $total_price;
			         
        } else if ($this->request->is('get')) {
			//get
            $data = $this->read_cart();
        }
        die(json_encode($data));
    }

    private function read_cart() {
        $data['result'] = false;
        $data['UserCartDeteail'] = array(); 
        //$data['Reseller'] = array();
        $User = $this->Session->read('User');
        $this->UserCart->Behaviors->load('Containable');
        $this->UserCart->contain( array(
            'UserCartDetail' => array(
				'UserCartDetailOffer' => array('UserCartOffer' => array('Offer')),
                'ShopBrochureProduct' => array(
                    'Product', 'ShopBrochure' => array(
                        'CampaignShopBrochure' => 'Campaign', 'ShopBrochureImage' => array(
                            'conditions' => array(
                                'ShopBrochureImage.order' => 0
                                )
                            )
                        )
                    )
                ),
            )
        );
        if ($UserCarts = $this->UserCart->find('all', array('conditions' => array('user_id' => $User['User']['id'], 'user_cart_status_flags' => 0)))) {
		
            $filter = array();
            $total_quantity = 0;
            
			foreach ($UserCarts as $k1 => $UserCart) {
			
				foreach ($UserCart['UserCartDetail'] as $k => $v) {
					$total_quantity = $total_quantity + $v['quantity'];
					$filter[$UserCart['UserCartDetail'][$k]['ShopBrochureProduct']['shop_brochure_id']]['UserCartDetail'][] = $v;
					$filter[$UserCart['UserCartDetail'][$k]['ShopBrochureProduct']['shop_brochure_id']]['UserCart'] = $UserCart['UserCart'];
					
					$Reseller = $this->UserResellerGroup->find('all',array(
					'fields' => array('DISTINCT UserResellerGroup.*, User.*'),
					'joins' => array(
									array('table' => 'user_client_brochures','alias' => 'B','type' => 'INNER','conditions' => array('UserResellerGroup.id = B.user_reseller_group_id')),
									array('table' => 'users','alias' => 'User','type' => 'INNER','conditions' => array('UserResellerGroup.reseller_user_id = User.id'))
									),
					'conditions' => array('B.user_id' => $User['User']['id'], 'B.shop_brochure_id' => $UserCart['UserCartDetail'][$k]['ShopBrochureProduct']['shop_brochure_id'])
					));
					
					if($Reseller){
						foreach ($Reseller as $kr => $vr) {
							if(isset($Reseller[$kr]['User']) && isset($Reseller[$kr]['User']['picture'])){
								$Reseller[$kr]['User']['picture'] = Router::url('/images/users/' . $vr['User']['picture'], true);
							}
						}
						$filter[$UserCart['UserCartDetail'][$k]['ShopBrochureProduct']['shop_brochure_id']]['Reseller'] = $Reseller;
					}
				}
			
			}
			
            $data['total_quantity'] = $total_quantity;
            $data['result'] = true;
            $data['UserCartDeteail'] = array_values($filter);
            
            
        }
        return $data;
    }

    public function send_cart() {
        $data['result'] = false;
        if($this->request->is('post')) {
            $User = $this->Session->read('User');
            $User = $User['User'];
            $this->UserCart->recursive = -1;
			
            $UserCarts = $this->UserCart->find('all', array(			
				'fields' => array('DISTINCT UserCart.*'),
				'joins' => array(
					array('table' => 'user_cart_details','alias' => 'D','type' => 'INNER','conditions' => array('UserCart.id = D.user_cart_id')),
					array('table' => 'shop_brochure_products','alias' => 'BP','type' => 'INNER','conditions' => array('D.product_id = BP.id'))
					),
                'conditions' => array(
					'BP.shop_brochure_id' => $this->request->data['ShopBrochureId'],
                    'user_id' => $User['id'],
                    'reseller_user_id' => null,
                    'user_cart_status_flags' => 0,
                    'purchase_date' => null,
                    'order_date' => null,
                    )
                )
            );
			
			foreach ($UserCarts as $k1 => $UserCart) {	
				$data = array(
					'reseller_user_id' => $this->request->data['UserCart']['reseller_user_id'],
					'user_cart_status_flags' => 1,
					'order_date' => time(),
				);
				$this->UserCart->id = $UserCart['UserCart']['id'];
				if ($this->UserCart->exists() AND $this->UserCart->save($data)) {
				}
			}
			$data = $this->read_cart();
			$data['result'] = true;
			$data['ordersByProducts'] = $this->get_orders_by_products(null, $User['id']);
			$data['ordersByCustomers'] = $this->get_orders_by_customers(null, $User['id']);
        }
        die(json_encode($data));
    }
	
	public function order_detail($order_date) {
        $data['result'] = false;
        $data['UserCartDeteail'] = array(); 
        
        $User = $this->Session->read('User');
        $this->UserCart->Behaviors->load('Containable');
        $this->UserCart->contain( array(
            'UserCartDetail' => array(
                'ShopBrochureProduct' => array(
                    'Product', 'ShopBrochure' => array(
                        'CampaignShopBrochure' => 'Campaign', 'ShopBrochureImage' => array(
                            'conditions' => array(
                                'ShopBrochureImage.order' => 0
                                )
                            )
                        )
                    )
                ),
            )
        );
        if ($UserCarts = $this->UserCart->find('all', array('conditions' => array('user_id' => $User['User']['id'], 'user_cart_status_flags' => 1, 'order_date' => $order_date)))) {
            $filter = array();
            $total_quantity = 0;
            
			foreach ($UserCarts as $k1 => $UserCart) {
			
				foreach ($UserCart['UserCartDetail'] as $k => $v) {
						$total_quantity = $total_quantity + $v['quantity'];
						$filter[$UserCart['UserCartDetail'][$k]['ShopBrochureProduct']['shop_brochure_id']]['UserCartDetail'][] = $v;
						$filter[$UserCart['UserCartDetail'][$k]['ShopBrochureProduct']['shop_brochure_id']]['UserCart'] = $UserCart['UserCart'];
				}
			
			}
			
            $data['total_quantity'] = $total_quantity;
            $data['result'] = true;
            $data['UserCartDeteail'] = array_values($filter);
            $this->UserResellerGroup->recursive = -1;
            $this->UserResellerGroup->Behaviors->load('Containable');
            $this->UserResellerGroup->contain(array(
                'User' => array(
                    )
                )
            );            
        }
        die(json_encode($data));
    }
	
	public function get_orders_by_products($date = null, $user_id){
	
		if($user_id == null){
			$User = $this->Session->read('User');
			$user_id = $User['User']['id'];
		}
		$this->UserCart->Behaviors->load('Containable');
        $this->UserCart->contain(array('User','UserCartDetail' => array('ShopBrochureProduct' => array('Product', 'ShopBrochure' => array('CampaignShopBrochure' => 'Campaign', 'ShopBrochureImage' => array('conditions' => array('ShopBrochureImage.order' => 0)))))));
		$UserCart = $this->UserCart->find('all', array('conditions' => array('reseller_user_id' => $user_id, 'user_cart_status_flags' => 1,'purchase_date' => $date,'order_date !=' => null,))
        );
        $rows = array();
        foreach ($UserCart as $k => $v) {
        foreach ($v['UserCartDetail'] as $k2 => $v2) {
       	if(isset($rows[$v2['ShopBrochureProduct']['shop_brochure_id']][$v2['ShopBrochureProduct']['id']]['total_quantity'])){
				$rows[$v2['ShopBrochureProduct']['shop_brochure_id']][$v2['ShopBrochureProduct']['id']]['total_quantity'] += $v2['quantity'];
				} else {
				$rows[$v2['ShopBrochureProduct']['shop_brochure_id']][$v2['ShopBrochureProduct']['id']]['total_quantity'] = $v2['quantity'];
				}
        }
        }
        $rows = array_values($rows);
        $total_quantity = 0;
        foreach ($rows as $k => $v) {
        	$rows[$k] = array_values($rows[$k]);
        	foreach ($v as $k2 => $v2) {
        		$total_quantity = $total_quantity + $v2['total_quantity'];
        	}
        }
        return $total_quantity;
	}
	private function get_orders_by_customers($date = null, $user_id){
		if($user_id == null){
			$User = $this->Session->read('User');
			$user_id = $User['User']['id'];
		}
		$UserCart = $this->UserCart->find('all', array('conditions' => array('reseller_user_id' => $user_id, 'user_cart_status_flags' => 1,'purchase_date' => $date, 'order_date !=' => null,))
        );
        return count($UserCart);
	}
	
}