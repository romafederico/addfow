<?php
class UserShopBrochure extends AppModel {

	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'user_id'
			),
		'ShopBrochure' => array(
			'foreignKey' => 'shop_brochure_id'
			),
		);

}