<?php
class UserResellerGroup extends AppModel {

	public $belongsTo = array(
		'Reseller' => array(
			'className' => 'User',
			'foreignKey' => 'reseller_user_id'
			)
		);

}