<?php
class UserCartDetail extends AppModel {	

	public $actsAs = array('Containable');

	public $hasMany = array('UserCartDetailOffer');

	public $belongsTo = array(
		'ShopBrochureProduct' => array(
			'foreignKey' => 'product_id'
			)
		);
	
	public function afterFind($data = array(), $options = array()) {
		foreach ($data as $k => $v) {
			$data[$k][$this->alias]['quantity'] = intval($data[$k][$this->alias]['quantity']);
		}
		return $data;
	}
}