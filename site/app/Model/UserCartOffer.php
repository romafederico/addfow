<?php
class UserCartOffer extends AppModel {

	public $hasMany = array('UserCartDetailOffer');

	public $belongsTo = array('UserCart', 'Offer');
	
}