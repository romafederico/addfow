<?php
class ShopBrochure extends AppModel {

	public $hasMany = array(
		'ShopBrochureImage' => array(
			'order' => array('order' => 'ASC')
			),
		'CampaignShopBrochure' => array( // Edit 25/03 => "Test"
			)
		);

	public function afterFind($data = array(), $options = array()) {
		foreach ($data as $k => $v) {
			if (array_key_exists('release_date', $data[$k][$this->alias])) {
				$data[$k][$this->alias]['release_date'] = date('m/Y', $data[$k][$this->alias]['release_date']);
			}
			if (array_key_exists('expiration_date', $data[$k][$this->alias])) {
			
				$mes = date('n',$data[$k][$this->alias]['expiration_date']);
				$mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
				
				$data[$k][$this->alias]['expiration_date_es'] =  date('d/m/Y', $data[$k][$this->alias]['expiration_date']);
				$data[$k][$this->alias]['expiration_date_real'] =  $data[$k][$this->alias]['expiration_date'];
				
				$data[$k][$this->alias]['expiration_date'] = date('d \d\e ', $data[$k][$this->alias]['expiration_date']).$mesesN[$mes].date(' \d\e Y', $data[$k][$this->alias]['expiration_date']);
				
				
			}
		}
		return $data;
	}

}