<?php
class UserCart extends AppModel {

	public $hasMany = array('UserCartDetail', 'UserCartOffer');

	public $belongsTo = array('User');
	
}