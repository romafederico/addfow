<?php $this->Html->addCrumb('Marcas', array('controller' => 'brands', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Agregar', array('controller' => 'brands', 'action' => 'create')) ?>
<div class="box">
    <div class="box-title">
        <h3>
            <i class="fa fa-calendar"></i>
            Marca
        </h3>
    </div>
    <div class="box-content">
        <?php echo $this->Form->create('Brand', array('class' => 'form-horizontal form-validate')) ?>
        <?php echo $this->Form->input('Brand.id', array('type' => 'hidden')) ?>
        <div class="form-group">
            <label for="BrandName" class="control-label col-sm-2">Nombre</label>
            <div class="col-sm-10">
                <?php echo $this->Form->input('Brand.name', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-actions">
            <?php echo $this->Form->submit('Guardar', array('div' => false, 'class' => 'btn btn-primary')) ?>
            <a href="<?php echo $this->request->referer() ?>" class="btn">Cancelar</a>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>