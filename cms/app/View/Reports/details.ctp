<?php $this->Html->addCrumb('Reports', array('controller' => 'reports', 'action' => 'index')) ?>
<br>
<br>
<?php setlocale(LC_MONETARY, 'es_AR.UTF-8'); ?>
<div class="box box-color box-bordered">
    <div class="box-title">
        <h3>
            <i class="fa fa-shopping-cart"></i>
            Detalle de <reporte></reporte>
        </h3>
    </div>
    <div class="box-content nopadding">
        <table class="table table-hover table-nomargin">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('campaign_name', 'Campaña') ?></th>
                <th><?php echo $this->Paginator->sort('shop_brochure_name', 'Catálogo') ?></th>
                <th><?php echo $this->Paginator->sort('fsc', 'FSC') ?></th>
                <th><?php echo $this->Paginator->sort('code', 'Código') ?></th>
                <th><?php echo $this->Paginator->sort('description', 'Descripción') ?></th>
                <th><?php echo $this->Paginator->sort('quantity', 'Cantidad') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $k => $v) { ?>
                <tr>
                    <td><?php echo $v['ReportDetail']['campaign_name'] ?></td>
                    <td><?php echo $v['ReportDetail']['shop_brochure_name'] ?></td>
                    <td><?php echo $v['ReportDetail']['fsc'] ?></td>
                    <td><?php echo $v['ReportDetail']['code'] ?></td>
                    <td><?php echo $v['ReportDetail']['description'] ?></td>
                    <td><?php echo $v['ReportDetail']['quantity'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="paginator-counter pull-left">
            <?php echo $this->Paginator->counter('Página {:page} de {:pages}, mostrando {:current} registros de {:count} total, comienza en registro {:start}, finaliza en {:end}') ?>
        </div>
        <div class="table-pagination">
            <?php echo $this->Paginator->first('Primera', array('tag' => false)) ?>
            <?php echo $this->Paginator->prev('Anterior', array('tag' => false), null, array('tag' => 'a', 'class' => 'disabled')) ?>
            <?php echo $this->Paginator->numbers(array('before' => '<span>', 'after' => '</span>', 'tag' => 'a', 'currentClass' => 'active', 'separator' => false)) ?>
            <?php echo $this->Paginator->next('Siguiente', array('tag' => false), null, array('tag' => 'a', 'class' => 'disabled')) ?>
            <?php echo $this->Paginator->last('Ultima', array('tag' => false)) ?>
        </div>
    </div>
</div>