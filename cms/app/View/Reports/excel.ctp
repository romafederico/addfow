<?php

$objPHPExcel = new PHPExcel();

$i = 1;
foreach ($reports as $report){
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $report['code']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$i, $report['fsc']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$i, $report['description']);
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $report['quantity']);

    $i += 1;
}

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="report.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
