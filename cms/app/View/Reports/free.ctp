<?php $this->Html->addCrumb('Reports', array('controller' => 'reports', 'action' => 'index')) ?>
<br>
<br>
<?php setlocale(LC_MONETARY, 'es_AR.UTF-8'); ?>
<div class="box box-color box-bordered">
    <div class="box-title">
        <h3>
            <i class="fa fa-shopping-cart"></i>
            Reportes free
        </h3>
    </div>
    <div class="box-content nopadding">
        <table class="table table-hover table-nomargin">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('campaign_name', 'Campaña') ?></th>
                <th><?php echo $this->Paginator->sort('billing', 'Facturación') ?></th>
                <th><?php echo $this->Paginator->sort('quantity', 'Unidades') ?></th>
                <th><?php echo $this->Paginator->sort('resellers', 'Revendedoras') ?></th>
                <th><?php echo $this->Paginator->sort('clients', 'Clientes') ?></th>
                <th>Acciónes</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $k => $v) { ?>
                <tr>
                    <td><?php echo $v['Report']['campaign_name'] ?></td>
                    <td><?php echo money_format('%.2n', $v['Report']['billing']) ?></td>
                    <td><a href="<?php echo h(Router::url('/reports/details/'.$v['Report']['campaign_id'], true)) ?>"><?php echo $v['Report']['quantity'] ?></a></td>
                    <td><?php echo $v['Report']['resellers'] ?></td>
                    <td><?php echo $v['Report']['clients'] ?></td>
                    <td>
                        <a href="<?php echo h(Router::url('/reports/excel/'.$v['Report']['campaign_id'], true)) ?>" class="btn" rel="tooltip" title="" data-original-title="Excel"><i class="fa fa-file-excel-o"></i></a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="paginator-counter pull-left">
            <?php echo $this->Paginator->counter('Página {:page} de {:pages}, mostrando {:current} registros de {:count} total, comienza en registro {:start}, finaliza en {:end}') ?>
        </div>
        <div class="table-pagination">
            <?php echo $this->Paginator->first('Primera', array('tag' => false)) ?>
            <?php echo $this->Paginator->prev('Anterior', array('tag' => false), null, array('tag' => 'a', 'class' => 'disabled')) ?>
            <?php echo $this->Paginator->numbers(array('before' => '<span>', 'after' => '</span>', 'tag' => 'a', 'currentClass' => 'active', 'separator' => false)) ?>
            <?php echo $this->Paginator->next('Siguiente', array('tag' => false), null, array('tag' => 'a', 'class' => 'disabled')) ?>
            <?php echo $this->Paginator->last('Ultima', array('tag' => false)) ?>
        </div>
    </div>
</div>

<a href="<?php echo h(Router::url('/reports/historic', true)) ?>">Historicos</a>