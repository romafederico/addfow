<div class="box">
    <div class="box-content">
        <?php echo $this->Form->create('ShopBrochureProduct', array('class' => 'form-horizontal form-validate', 'type' => 'file')) ?>
        <div class="form-group">
            <label for="ShopBrochureProductProduct" class="control-label col-sm-2">Descripción</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('ShopBrochureProduct.product', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductCode" class="control-label col-sm-2">Código de línea</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('ShopBrochureProduct.code', array('readonly', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductPrice" class="control-label col-sm-2">Precio</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('ShopBrochureProduct.price', array('readonly', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductFsc" class="control-label col-sm-2">FSC</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('ShopBrochureProduct.fsc', array('readonly', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductBrand" class="control-label col-sm-2">Marca</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('ShopBrochureProduct.brand', array('readonly', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductCategory" class="control-label col-sm-2">Categoria</label>

            <div class="col-sm-8">
                <?php echo $this->Form->input('ShopBrochureProduct.category', array('readonly', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductImage" class="control-label col-sm-2">Imagen</label>

            <div class="col-sm-10">
                <?php echo $this->Form->file('ShopBrochureProduct.image_product', array('div' => false, 'label' => false, 'class' => '', 'data-rule-required' => false)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ShopBrochureProductNotes" class="control-label col-sm-2">Notas</label>

            <div class="col-sm-10">
                <?php echo $this->Form->textarea('ShopBrochureProduct.notes', array('readonly', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>

<style>
    .ui-autocomplete{z-index: 999999}
</style>

<script>
    (function ($) {
        $('#ShopBrochureProductProduct').autocomplete({
            source: App.baseUrl("shop_brochures/products"),
            select: function(event, ui) {
                $.get( App.baseUrl("shop_brochures/product"), { product_id: ui.item.id } )
                .done(function( res ) {
                    console.log(res);

                    $('#ShopBrochureProductCode').val(res.Product.code);
                    $('#ShopBrochureProductPrice').val(res.Product.price);
                    $('#ShopBrochureProductFsc').val(res.Product.fsc);

                    $('#ShopBrochureProductBrand').val(res.Brand.name);

                    $.get( App.baseUrl("shop_brochures/category_tree"), { category: res.Category.name } )
                    .done(function( tree ) {
                        $('#ShopBrochureProductCategory').val(tree);
                    });

                    $('#ShopBrochureProductNotes').val(res.Product.notes);
                });
            }
        });
        $('#ShopBrochureProductBrand').autocomplete({
            source: App.baseUrl("shop_brochures/brands")
        });
        $('#ShopBrochureProductCategory').autocomplete({
            source: App.baseUrl("shop_brochures/categories"),
            select: function(event, ui) {
                $.get( App.baseUrl("shop_brochures/category_tree"), { category: ui.item.value } )
                .done(function( tree ) {
                    $('#ShopBrochureProductCategory').val(tree);
                });
            }
        });
    })(jQuery);
</script>