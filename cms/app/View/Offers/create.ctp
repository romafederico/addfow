<?php $this->Html->addCrumb('Ofertas', array('controller' => 'offers', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Agregar', array('controller' => 'offers', 'action' => 'create')) ?>
<div class="box">
    <div class="box-title">
        <h3>
            <i class="fa fa-tags"></i>
            Oferta
        </h3>
    </div>
    <div class="box-content">
        <?php echo $this->Form->create('Offer', array('class' => 'form-horizontal form-validate')) ?>
        <?php echo $this->Form->input('Offer.id', array('type' => 'hidden')) ?>
        <div class="form-group">
            <label for="OfferName" class="control-label col-sm-2">Nombre</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('Offer.name', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="OfferDescription" class="control-label col-sm-2">Descripción</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('Offer.description', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="OfferOfferTypeId" class="control-label col-sm-2">Oferta</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('Offer.offer_type_id', array('title' => 'Este campo es obligatorio.', 'div' => false, 'label' => false, 'data-rule-required' => true, 'options' => $OffersTypes, 'empty' => 'Seleccione...')) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="OfferProductGroupShopBrochureId" class="control-label col-sm-2">Catalogo</label>

            <div class="col-sm-10">
                <?php echo $this->Form->input('OfferProductGroup.0.shop_brochure_id', array('title' => 'Este campo es obligatorio.', 'div' => false, 'label' => false, 'data-rule-required' => true, 'options' => $ShopBrochures, 'empty' => 'Seleccione...')) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="OfferTodos" class="control-label col-sm-2">Todos Iguales</label>

            <div class="col-sm-10">
                <?php echo $this->Form->checkbox('Offer.todos', array('div' => false, 'label' => false, 'class' => 'form-control ignore', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group" id="offer-price">
            <label for="OfferProductGroupPrice" class="control-label col-sm-2">Precio</label>

            <div class="col-sm-2">
                <?php echo $this->Form->input('OfferProductGroup.0.price', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'form-control price', 'data-rule-required' => true)) ?>
            </div>
        </div>
        <div class="form-group">
            <label for="OfferProductGroupProductId" class="control-label col-sm-2">Productos</label>

            <div class="col-sm-10">
                <select multiple="multiple" id="OfferProductGroupShopBrochureProductId"
                        name="data[OfferProductGroup][shop_brochure_product_id][]">
                </select>
				
				<div style="display:none" id="OfferProductGroupContingencyContainer">
					<br>
					<select data-rule-required="true" multiple="multiple" id="OfferProductGroupContingencyShopBrochureProductId"
							name="data[OfferProductGroupContingency][shop_brochure_product_id][]">
					</select>
				</div>
            </div>
        </div>
        <div class="form-group" id="offer-code">
            <label for="OfferProductGroupCode" class="control-label col-sm-2">Código</label>

            <div class="col-sm-2">
                <?php echo $this->Form->input('OfferProductGroup.0.code', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
            </div>
        </div>

        <div class="col-sm-offset-2 input_fields_wrap" id="offer-multi">
            <button class="add_field_button">+</button>
            <div class="row">
                <div class="col-md-3">Cantidad</div>
                <div class="col-md-3">Precio</div>
                <div class="col-md-3">Codigo</div>
                <div class="col-md-3"></div>
            </div>

            <?php if (isset($OfferProductGroup)): ?>
                <?php foreach($OfferProductGroup as $k => $v): ?>
                    <div class="row">
                        <div class="col-md-3">
                            <?php echo $this->Form->input('OfferProductGroup.multi.'.($k+1).'.quantity', array('value' => $v['OfferProductGroup']['quantity'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo $this->Form->input('OfferProductGroup.multi.'.($k+1).'.price', array('value' => $v['OfferProductGroup']['price'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo $this->Form->input('OfferProductGroup.multi.'.($k+1).'.code', array('value' => $v['OfferProductGroup']['code'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
                        </div>
                        <div class="col-md-3"><?php echo ($k+1) > 1 ? '<a href="#" class="remove_field">X</a>' : '' ?></div>
                    </div>
                <?php endforeach ?>
            <?php else: ?>
                <div class="row">
                    <div class="col-md-3">
                        <?php echo $this->Form->input('OfferProductGroup.multi.1.quantity', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('OfferProductGroup.multi.1.price', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('OfferProductGroup.multi.1.code', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            <?php endif ?>
        </div>

        <div class="form-actions">
            <?php echo $this->Form->submit('Guardar', array('div' => false, 'class' => 'btn btn-primary')) ?>
            <a href="<?php echo $this->request->referer() ?>" class="btn">Cancelar</a>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        var max_fields = 5;
        var wrapper = $(".input_fields_wrap");
        var add_button = $(".add_field_button");

        var x = <?php echo isset($OfferProductGroup) ? count($OfferProductGroup) : 1 ?>;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                var html = '<div class="row">';
                html += '<div class="col-md-3"><input name="data[OfferProductGroup][multi][' + x + '][quantity]" class="form-control" data-rule-required="1" type="text" id="OfferProductGroup0Quantity" aria-required="true"></div>';
                html += '<div class="col-md-3"><input name="data[OfferProductGroup][multi][' + x + '][price]" class="form-control" data-rule-required="1" step="0.01" type="number" id="OfferProductGroup0Price" aria-required="true"></div>';
                html += '<div class="col-md-3"><input name="data[OfferProductGroup][multi][' + x + '][code]" class="form-control" data-rule-required="1" maxlength="50" type="text" id="OfferProductGroup0Code" aria-required="true"></div>';
                html += '<div class="col-md-3"><a href="#" class="remove_field">X</a></div>';
                html += '</div>';
                $(wrapper).append(html);
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) {
            e.preventDefault();
            $(this).parent('div').parent('div').remove();
            x--;
        })
    });
</script>