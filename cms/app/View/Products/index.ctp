<?php $this->Html->addCrumb('Productos', array('controller' => 'products', 'action' => 'index')) ?>
<br>
<br>
<div class="box-content">
	<?php echo $this->Form->create('Product', array('class' => 'form-horizontal form-validate', 'action'=>'index', 'novalidate' => true)) ?>
	<div class="form-group">
		<label for="SearchProductCampaign" class="control-label col-sm-2">Campaña</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('Search.Product.campaign', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-one')) ?>
		</div>
	</div>
	<div class="form-group">
		<label for="SearchProductCode" class="control-label col-sm-2">Código de línea</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('Search.Product.code', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-one')) ?>
		</div>
	</div>
	<div class="form-group">
		<label for="SearchProductDescription" class="control-label col-sm-2">Descripción</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('Search.Product.description', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-one')) ?>
		</div>
	</div>
	<div class="form-actions">
		<?php echo $this->Form->submit('Buscar', array('div' => false, 'class' => 'btn btn-primary')) ?>
		<a href="<?php echo h(Router::url('/products/create', true)) ?>" class="btn btn-primary pull-right">Nueva Producto</a>
	</div>
	<?php echo $this->Form->end() ?>
</div>
<div class="box box-color box-bordered">
	<div class="box-title">
		<h3>
			<i class="fa fa-shopping-cart"></i>
			Productos
		</h3>
	</div>
	<div class="box-content nopadding">
		<table class="table table-hover table-nomargin">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id', 'ID') ?></th>
					<th><?php echo $this->Paginator->sort('campaign', 'Campaña') ?></th>
					<th><?php echo $this->Paginator->sort('code', 'Código de línea') ?></th>
					<th><?php echo $this->Paginator->sort('description', 'Descripción') ?></th>
					<th><?php echo $this->Paginator->sort('price', 'Precio') ?></th>
					<th>Acciónes</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($rows as $k => $v) { ?>
					<tr>
						<td><?php echo $v['Product']['id'] ?></td>
						<td><?php echo $v['Campaign']['name'] ?></td>
						<td><?php echo $v['Product']['code'] ?></td>
						<td><?php echo $v['Product']['description'] ?></td>
						<td><?php echo $v['Product']['price'] ?></td>
						<td>
							<a href="<?php echo h(Router::url('/products/update/'.$v['Product']['id'], true)) ?>" class="btn" rel="tooltip" title="" data-original-title="Editar"><i class="fa fa-edit"></i></a>
							<a href="<?php echo h(Router::url('/products/delete/'.$v['Product']['id'], true)) ?>" class="btn delete" rel="tooltip" title="" data-original-title="Eliminar"><i class="fa fa-times"></i></a>
							<a href="<?php echo h(Router::url('/products/duplicate/'.$v['Product']['id'], true)) ?>" class="btn" rel="tooltip" title="" data-original-title="Duplicar"><i class="fa fa-plus"></i></a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<div class="paginator-counter pull-left">
			<?php echo $this->Paginator->counter('Página {:page} de {:pages}, mostrando {:current} registros de {:count} total, comienza en registro {:start}, finaliza en {:end}') ?>
		</div>
		<div class="table-pagination">
			<?php echo $this->Paginator->first('Primera', array('tag' => false)) ?>
			<?php echo $this->Paginator->prev('Anterior', array('tag' => false), null, array('tag' => 'a', 'class' => 'disabled')) ?>
			<?php echo $this->Paginator->numbers(array('before' => '<span>', 'after' => '</span>', 'tag' => 'a', 'currentClass' => 'active', 'separator' => false)) ?>
			<?php echo $this->Paginator->next('Siguiente', array('tag' => false), null, array('tag' => 'a', 'class' => 'disabled')) ?>
			<?php echo $this->Paginator->last('Ultima', array('tag' => false)) ?>
		</div>
	</div>
</div>

<script>
    $(function () {
		$.validator.addMethod("one", function(value, element, param) {
			if (!$('#SearchProductCampaign').val() && !$('#SearchProductCode').val() && !$('#SearchProductDescription').val()) {
				return false;
			}
			$('#ProductIndexForm input[type=text]').map(function(){
				$(this).closest('.form-group').removeClass('has-error').addClass('has-success').find('span').remove();
			});
			return true;
		}, 'Complete alguno de los filtros');
	});
</script>