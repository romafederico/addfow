<?php $this->Html->addCrumb('Productos', array('controller' => 'products', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Agregar', array('controller' => 'products', 'action' => 'create')) ?>
<div class="box">
	<div class="box-title">
		<h3>
			<i class="fa fa-shopping-cart"></i>
			Producto
		</h3>
	</div>
	<div class="box-content">
		<?php
			$array = array('class' => 'form-horizontal form-validate');

			if ('duplicate' == $this->request->params['action']) {
				$array['action'] = 'create';
			}

			echo $this->Form->create('Product', $array);
		?>
			<div class="form-group">
				<label for="ProductDescription" class="control-label col-sm-2">Descripción</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.description', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductCode" class="control-label col-sm-2">Código de línea</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.code', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductGroupMulti" class="control-label col-sm-2">Multiples Propiedades</label>
				<div class="col-sm-10">
					<?php echo $this->Form->checkbox('Product.multi', array('div' => false, 'label' => false, 'class' => 'form-control ignore', 'data-rule-required' => true)) ?>
				</div>
			</div>
			<div class="col-sm-offset-2 input_fields_wrap" id="product-multi">
				<div class="form-group">
					<div class="col-md-4">Propiedad</div>
					<div class="col-md-2">FSC</div>
					<div class="col-md-2">Precio</div>
					<div class="col-md-2">Código de línea</div>
					<div class="col-md-2"><button class="add_field_button">+</button></div>
				</div>

				<?php if (!empty($ProductVersion)): ?>
					<?php foreach($ProductVersion as $k => $v): ?>
						<div class="form-group">
							<?php echo $this->Form->hidden('ProductVersion.multi.'.($k+1).'.id', array('value' => $v['Product']['id'])); ?>
							<div class="col-md-4">
								<?php echo $this->Form->input('ProductVersion.multi.'.($k+1).'.property', array('value' => $v['Product']['property'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('ProductVersion.multi.'.($k+1).'.fsc', array('value' => $v['Product']['fsc'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('ProductVersion.multi.'.($k+1).'.price', array('value' => $v['Product']['price'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('ProductVersion.multi.'.($k+1).'.code', array('value' => $v['Product']['code'], 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
							</div>
							<div class="col-md-2"><?php echo ($k+1) > 1 ? '<a href="#" class="remove_field">X</a>' : '' ?></div>
						</div>
					<?php endforeach ?>
				<?php else: ?>
					<div class="form-group">
						<div class="col-md-4">
							<?php echo $this->Form->input('ProductVersion.multi.0.property', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
						</div>
						<div class="col-md-2">
							<?php echo $this->Form->input('ProductVersion.multi.0.fsc', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
						</div>
						<div class="col-md-2">
							<?php echo $this->Form->input('ProductVersion.multi.0.price', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
						</div>
						<div class="col-md-2">
							<?php echo $this->Form->input('ProductVersion.multi.0.code', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
						</div>
						<div class="col-md-2"></div>
					</div>
				<?php endif ?>
			</div>
			<div class="form-group">
				<label for="ProductCampaign" class="control-label col-sm-2">Campaña</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.campaign_id', array('options' => $Campaigns, 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true, 'empty' => 'Seleccione...')) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductFsc" class="control-label col-sm-2">FSC</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.fsc', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductPrice" class="control-label col-sm-2">Precio</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.price', array('div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductBrand" class="control-label col-sm-2">Marca</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.brand_id', array('options' => $Brands, 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true, 'empty' => 'Seleccione...')) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductNotes" class="control-label col-sm-2">Notas</label>
				<div class="col-sm-10">
					<?php echo $this->Form->input('Product.notes', array('div' => false, 'label' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="ProductCategoryId" class="control-label col-sm-1">Categoría</label>
				<a href="javascript:;" class="btn btn-primary col-sm-1 assign-category">Asignar Categoría</a>
				<div class="col-sm-10">
					<input id="ProductCategoryName" <?php if ($ProductCategoryName) echo 'value="'.$ProductCategoryName.'"'?>type="text" class="form-control" data-rule-required="true">
					<?php echo $this->Form->input('Product.category_id', array('type' => 'hidden', 'div' => false, 'label' => false, 'class' => 'form-control', 'data-rule-required' => true)) ?>
				</div>
			</div>
			<div class="form-actions">
				<?php echo $this->Form->submit('Guardar', array('div' => false, 'class' => 'btn btn-primary')) ?>
				<?php echo $this->Form->submit('Guardar y Dublicar', array('name' => 'duplicate', 'div' => false, 'class' => 'btn btn-primary')) ?>
				<a href="<?php echo $this->request->referer() ?>" class="btn">Cancelar</a>
			</div>
		<?php echo $this->Form->end() ?>
	</div>
</div>



<script>
	$(document).ready(function () {

		if ($('#ProductMulti').is(':checked')) {
			$('#ProductFsc').closest('.form-group').hide();
			$('#ProductCode').closest('.form-group').hide();
			$('#ProductPrice').closest('.form-group').hide();
			$('#product-multi').show();
		}


		$('#ProductMulti').click(function(){
			if ($(this).is(':checked')) {
				$('#ProductFsc').closest('.form-group').hide();
				$('#ProductCode').closest('.form-group').hide();
				$('#ProductPrice').closest('.form-group').hide();
				$('#product-multi').show();
			} else {
				$('#ProductFsc').closest('.form-group').show();
				$('#ProductCode').closest('.form-group').show();
				$('#ProductPrice').closest('.form-group').show();
				$('#product-multi').hide();
			}
		});

		var max_fields = 51;
		var wrapper = $(".input_fields_wrap");
		var add_button = $(".add_field_button");

		var x = <?php echo isset($ProductVersion) ? count($ProductVersion) : 1 ?>;
		$(add_button).click(function (e) {
			e.preventDefault();
			if ($('#product-multi .form-group').length < max_fields) {
				x++;
				var html = '<div class="form-group">';
				html += '<div class="col-md-4"><input name="data[ProductVersion][multi][' + x + '][property]" class="form-control" data-rule-required="1" type="text" id="ProductVersion0Property" aria-required="true"></div>';
				html += '<div class="col-md-2"><input name="data[ProductVersion][multi][' + x + '][fsc]" class="form-control" data-rule-required="1" step="0.01" type="text" id="ProductVersion0Fsc" aria-required="true"></div>';
				html += '<div class="col-md-2"><input name="data[ProductVersion][multi][' + x + '][price]" class="form-control" data-rule-required="1" step="0.01" type="number" id="ProductVersion0Price" aria-required="true"></div>';
				html += '<div class="col-md-2"><input name="data[ProductVersion][multi][' + x + '][code]" class="form-control" data-rule-required="1" maxlength="50" type="text" id="ProductVersion0Code" aria-required="true"></div>';
				html += '<div class="col-md-2"><button href="#" class="remove_field">-</button></div>';
				html += '</div>';
				$(wrapper).append(html);
			}
		});

		$(wrapper).on("click", ".remove_field", function (e) {
			e.preventDefault();
			$(this).parent('div').parent('div').remove();
			//x--;
		})

		$('#ProductCategoryName').autocomplete({
			source: App.baseUrl("products/categories"),
			select: function(event, ui) {
				$('#ProductCategoryId').val(ui.item.id);
				$.get( App.baseUrl("products/category_tree"), { category: ui.item.value } )
					.done(function( tree ) {
						$('#ProductCategoryName').val(tree);
					});
			}
		});
	});
</script>