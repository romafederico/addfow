<?php

class OffersController extends AppController
{

    public $uses = array('Offer', 'OfferType', 'ShopBrochure', 'ShopBrochureImage', 'ShopBrochureProduct', 'OfferProductGroup', 'OfferProductGroupContingency');

    public $components = array('Paginator');

    public function beforeRender()
    {
        $this->set('Module', 'Ofertas');
    }

    public function read_products($id = null)
    {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = array();
        if ($this->request->is('ajax') AND $this->request->is('get') AND $id) {
            
            if($ShopBrochureProduct = $this->ShopBrochureProduct->find('all', array(
                'joins' => array(array('table' => 'shop_brochure_images','alias' => 'ShopBrochureImage','type' => 'INNER','conditions' => array('ShopBrochureImage.id = ShopBrochureProduct.shop_brochure_image_id'))),
                'fields' => array('DISTINCT ShopBrochureProduct.*, Product.*'),
                'conditions' => array('ShopBrochureProduct.father_id' => null, 'ShopBrochureProduct.shop_brochure_id' => $id)))){
                $data['products'] = $ShopBrochureProduct;
            }
        }
        die(json_encode($data));
    }

    public function read_offer_product($id = null)
    {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = array();
        if ($this->request->is('ajax') AND $this->request->is('get') AND $id AND $OfferProductGroup = $this->OfferProductGroup->find('list', array('fields' => array('shop_brochure_product_id'), 'conditions' => array('offer_id' => $id)))) {
            $data['products'] = array_values($OfferProductGroup);
        }
        die(json_encode($data));
    }
	
	public function read_offer_product_contingency($id = null)
    {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = array();
        if ($this->request->is('ajax') AND $this->request->is('get') AND $id AND $OfferProductGroupContingency = $this->OfferProductGroupContingency->find('list', array('fields' => array('shop_brochure_product_id'), 'conditions' => array('offer_id' => $id)))) {
            $data['products'] = array_values($OfferProductGroupContingency);
        }
        die(json_encode($data));
    }

    public function index()
    {
        $this->set('OfferTypes', $this->OfferType->find('list', array('fields' => array('id', 'name'))));
        if ($this->request->is('post')) {
            $conditions = array();
            if ($this->data['Search']['Offer']['first_name']) {
                $conditions['Offer.first_name LIKE'] = '%' . $this->data['Search']['Offer']['first_name'] . '%';
            }
            if ($this->data['Search']['Offer']['user_type_id'] > 0) {
                $conditions['Offer.user_type_id ='] = $this->data['Search']['Offer']['user_type_id'];
            }
            $this->Paginator->settings = array(
                'conditions' => $conditions
            );
            $this->set('rows', $this->paginate('Offer'));
        } else if ($this->request->is('get')) {
            $this->set('rows', $this->paginate());
        }
    }

    public function create()
    {
        if ($this->request->is('post')) {
            //var_dump($this->request->data); die;
            $this->Offer->create();
            if ($Offer = $this->Offer->save($this->request->data)) {
                foreach ($this->request->data['OfferProductGroup']['shop_brochure_product_id'] as $k => $v) {
                    if (4 != $this->request->data['Offer']['offer_type_id']) {
                        $this->OfferProductGroup->create();
                        $this->OfferProductGroup->save(array(
                                'shop_brochure_product_id' => $v,
                                'shop_brochure_id' => $this->request->data['OfferProductGroup'][0]['shop_brochure_id'],
                                //'quantity' => $this->request->data['OfferProductGroup'][0]['quantity'],
                                'price' => $this->request->data['OfferProductGroup'][0]['price'],
                                'code' => $this->request->data['OfferProductGroup'][0]['code'],
                                'offer_id' => $Offer['Offer']['id'],
                            )
                        );
                    } else {
                        foreach ($this->request->data['OfferProductGroup']['multi'] as $key => $value) {
                            $this->OfferProductGroup->create();
                            $this->OfferProductGroup->save(array(
                                    'shop_brochure_product_id' => $v,
                                    'shop_brochure_id' => $this->request->data['OfferProductGroup'][0]['shop_brochure_id'],
                                    'quantity' => $value['quantity'],
                                    'price' => $value['price'],
                                    'code' => $value['code'],
                                    'offer_id' => $Offer['Offer']['id']
                                )
                            );
                        }
                    }
                }
				if($this->request->data['Offer']['offer_type_id'] == 3){
					foreach ($this->request->data['OfferProductGroupContingency']['shop_brochure_product_id'] as $k => $v) {
						$this->OfferProductGroupContingency->create();
						$this->OfferProductGroupContingency->save(array(
								'shop_brochure_product_id' => $v,
								'shop_brochure_id' => $this->request->data['OfferProductGroup'][0]['shop_brochure_id'],
								'offer_id' => $Offer['Offer']['id'],
							)
						);
					}
				}
                $this->Session->setFlash('Registro guardado', 'flash');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
        }
        $this->set('ShopBrochures', $this->ShopBrochure->find('list', array('fields' => array('id', 'name'), 'conditions' => array('name !=' => null))));
        $this->set('OffersTypes', $this->OfferType->find('list', array('fields' => array('id', 'name'))));
    }

    public function update($id = null)
    {
        $this->Offer->id = $id;
        if (!$this->Offer->exists()) {
            $this->Session->setFlash('Registro Invalido', 'flash');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($Offer = $this->Offer->save($this->request->data)) {
                $this->OfferProductGroup->deleteAll(array('offer_id' => $id));
                foreach ($this->request->data['OfferProductGroup']['shop_brochure_product_id'] as $k => $v) {
                    if (4 != $this->request->data['Offer']['offer_type_id']) {
                        $this->OfferProductGroup->create();
                        $this->OfferProductGroup->save(array(
                                'shop_brochure_product_id' => $v,
                                'shop_brochure_id' => $this->request->data['OfferProductGroup'][0]['shop_brochure_id'],
                                //'quantity' => $this->request->data['OfferProductGroup'][0]['quantity'],
                                'price' => $this->request->data['OfferProductGroup'][0]['price'],
                                'code' => $this->request->data['OfferProductGroup'][0]['code'],
                                'offer_id' => $Offer['Offer']['id'],
                            )
                        );
                    } else {
                        foreach ($this->request->data['OfferProductGroup']['multi'] as $key => $value) {
                            $this->OfferProductGroup->create();
                            $this->OfferProductGroup->save(array(
                                    'shop_brochure_product_id' => $v,
                                    'shop_brochure_id' => $this->request->data['OfferProductGroup'][0]['shop_brochure_id'],
                                    'quantity' => $value['quantity'],
                                    'price' => $value['price'],
                                    'code' => $value['code'],
                                    'offer_id' => $Offer['Offer']['id'],
                                )
                            );
                        }
                    }
                }
				
				if($this->request->data['Offer']['offer_type_id'] == 3){
					$this->OfferProductGroupContingency->deleteAll(array('offer_id' => $id));
					foreach ($this->request->data['OfferProductGroupContingency']['shop_brochure_product_id'] as $k => $v) {
						$this->OfferProductGroupContingency->create();
						$this->OfferProductGroupContingency->save(array(
								'shop_brochure_product_id' => $v,
								'shop_brochure_id' => $this->request->data['OfferProductGroup'][0]['shop_brochure_id'],
								'offer_id' => $Offer['Offer']['id'],
							)
						);
					}
				}
				
                $this->Session->setFlash('Registro guardado', 'flash');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
        } else {
            $this->request->data = $this->Offer->read(null, $id);
            $this->set('ShopBrochures', $this->ShopBrochure->find('list', array('fields' => array('id', 'name'), 'conditions' => array('name !=' => null))));
            $this->set('OffersTypes', $this->OfferType->find('list', array('fields' => array('id', 'name'))));

            $offerproductgroup = $this->OfferProductGroup->find('first', array('fields' => array('DISTINCT OfferProductGroup.*'),'conditions' => array('offer_id' => $id), 'order' => array('OfferProductGroup.id')));

            $this->set('OfferProductGroup', $this->OfferProductGroup->find('all', array('fields' => array('DISTINCT OfferProductGroup.*'),'conditions' => array('offer_id' => $id, 'shop_brochure_product_id' => $offerproductgroup['OfferProductGroup']['shop_brochure_product_id']), 'order' => array('OfferProductGroup.id'))));
            $this->render('create');
        }
    }

    public function delete($id = null)
    {
        $this->Offer->id = $id;
        if (!$this->Offer->exists()) {
            throw new NotFoundException('Registro Invalido', 'flash');
        }
        if ($this->Offer->delete()) {
            $this->Session->setFlash('Registro Eliminado', 'flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('Regitro no Eliminado', 'flash');
        return $this->redirect(array('action' => 'index'));
    }

}