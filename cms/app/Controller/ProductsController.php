<?php
class ProductsController extends AppController {

	public $uses = array('Product', 'Category', 'Brand', 'Campaign', 'ProductVersion');

	public $components = array('Paginator'); 

	public function beforeRender() {
		$this->set('Module', 'Productos');
	}
	
	public function index() {
		if ($this->request->is('post')) {
			$conditions = array('Product.father_id' => null);
			if ($this->data['Search']['Product']['campaign']) {
				if ($campaign = $this->Campaign->findByName($this->data['Search']['Product']['campaign'])) {
					$conditions['Product.campaign_id ='] = $campaign['Campaign']['id'];
				}
			}
			if ($this->data['Search']['Product']['code']) {
				$conditions['Product.code ='] = $this->data['Search']['Product']['code'];
			}
			if ($this->data['Search']['Product']['description']) {
				$conditions['Product.description LIKE '] = '%'.$this->data['Search']['Product']['description'].'%';
			}
			$this->Paginator->settings = array(
				'conditions'	=> $conditions
			);
			$this->set('rows', $this->paginate('Product'));
		} else if ($this->request->is('get')) {
			$this->Paginator->settings = array(
				'conditions'	=> array('Product.father_id' => null)
			);
			$this->set('rows', $this->paginate());
		}
	}

	public function categories() {
		if ($this->request->is('ajax')) {
			$term = $this->request->query('term');
			if(!empty($term)) {
				$items = $this->Category->find('all', array(
					'conditions' => array('name LIKE' => trim($term) . '%'),
					'fields' => array('id', 'name as label')
				));
			}
			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode(Set::extract('/Category/.', $items)));
		}
	}

	private function _category_tree($category = null) {
		if (!is_null($category)) {
			$this->request->query['category'] = $category;
		}

		$row = $this->Category->find('first', array(
			'conditions' => array('name' => $this->request->query['category'])
		));

		$tree = $row['Category']['name'];

		while(!empty($row)) {
			$row = $this->Category->findById($row['Category']['parent_category_id']);

			if ($row) {
				$tree = $row['Category']['name'] .'/' . $tree;
			}
		}

		return $tree;
	}

	public function category_tree() {
		if ($this->request->is('ajax')) {

			$tree = $this->_category_tree();

			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode($tree));
		}
	}

	public function create() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				if ($this->request->data['Product']['multi']) {
					$this->request->data['Product']['multi'] = 0;
					$this->request->data['Product']['father_id'] = $this->Product->id;
					foreach ($this->request->data['ProductVersion']['multi'] as $item) {
						$this->request->data['Product']['property'] = $item['property'];
						$this->request->data['Product']['fsc'] = $item['fsc'];
						$this->request->data['Product']['price'] = $item['price'];
						$this->request->data['Product']['code'] = $item['code'];
						$this->Product->create();
						$this->Product->save($this->request->data);
					}
				}
				$this->Session->setFlash('Registro guardado', 'flash');
				if (isset($this->request->data['duplicate'])) {
					return $this->redirect(array('action' => 'duplicate', $this->Product->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
			$this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
		}
		$this->set('ProductCategoryName', '');
		$this->set('Brands', $this->Brand->find('list', array('fields' => array('id', 'name'))));
		$this->set('Campaigns', $this->Campaign->find('list', array(
				'fields' => array('id', 'name'),
				'order' => array('end_date DESC')
		)));
	}

	public function update($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			$this->Session->setFlash('Registro Invalido', 'flash');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Product->save($this->request->data)) {
				if ($this->request->data['Product']['multi']) {
					$ids = array();
					$this->request->data['Product']['multi'] = 0;
					$this->request->data['Product']['father_id'] = $id;
					foreach ($this->request->data['ProductVersion']['multi'] as $item) {
						if (isset($item['id'])) {
							$ids[] = intval($item['id']);
							$this->request->data['Product']['id'] = $item['id'];
							$this->request->data['Product']['property'] = $item['property'];
							$this->request->data['Product']['fsc'] = $item['fsc'];
							$this->request->data['Product']['price'] = $item['price'];
							$this->request->data['Product']['code'] = $item['code'];
							$this->Product->save($this->request->data);
						} else {
							unset($this->request->data['Product']['id']) ;
							$this->request->data['Product']['property'] = $item['property'];
							$this->request->data['Product']['property'] = $item['property'];
							$this->request->data['Product']['fsc'] = $item['fsc'];
							$this->request->data['Product']['price'] = $item['price'];
							$this->request->data['Product']['code'] = $item['code'];
							$this->Product->create();
							$this->Product->save($this->request->data);
							$ids[] = intval($this->Product->id);
						}
					}
					$this->Product->deleteAll(array('father_id' => $id, 'NOT' => array('Product.id' => $ids)));
				} else {
					$this->Product->deleteAll(array('father_id' => $id));
				}
				$this->Session->setFlash('Registro guardado', 'flash');
				if (isset($this->request->data['duplicate'])) {
					return $this->redirect(array('action' => 'duplicate', $this->Product->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
			$this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
		} else {
			$this->request->data = $this->Product->read(null, $id);
			$this->set('ProductVersion', $this->Product->find('all', array('conditions' => array('father_id' => $id), 'order' => array('Product.id ASC'))));
			$this->set('ProductCategoryName', $this->read_categories($this->request->data['Product']['category_id']));
			$this->set('Brands', $this->Brand->find('list', array('fields' => array('id', 'name'))));
			$this->set('Campaigns', $this->Campaign->find('list', array(
					'fields' => array('id', 'name'),
					'order' => array('end_date DESC')
			)));
			$this->render('create');
		}
	}

	public function duplicate($id = null)
	{
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			$this->Session->setFlash('Registro Invalido', 'flash');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->data = $this->Product->read(null, $id);
		$this->set('ProductVersion', $this->Product->find('all', array('conditions' => array('father_id' => $id))));
		$this->set('ProductCategoryName', $this->read_categories($this->request->data['Product']['category_id']));
		$this->set('Brands', $this->Brand->find('list', array('fields' => array('id', 'name'))));
		$this->set('Campaigns', $this->Campaign->find('list', array('fields' => array('id', 'name'))));

		unset($this->request->data['Product']['id']);

		$this->request->data['Product']['description'] = 'Copia ' . $this->request->data['Product']['description'];

		$this->render('create');
	}

	private function read_categories($id = null) {
		$this->autoRender = false;
		$categories = $this->read_parent($id);
		$categories = explode('/', $categories);
		$categories = array_reverse($categories);
		foreach ($categories as $k => $v) {
			if (!$v) {
				unset($categories[$k]);
			}
		}
		$categories = implode('/', $categories);
        return $categories;
	}

	private function read_parent($id) {
        $row = $this->Category->findById($id);
        $menu = '/';
        if ($row) {
	        $menu .= $row['Category']['name'];
	        $menu .= $this->read_parent($row['Category']['parent_category_id']);
        }
        return $menu;
    }

	public function delete($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException('Registro Invalido', 'flash');
		}
		if ($this->Product->delete()) {
			$this->Product->deleteAll(array('father_id' => $id));
			$this->Session->setFlash('Registro Eliminado', 'flash');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Regitro no Eliminado', 'flash');
		return $this->redirect(array('action' => 'index'));
	}

	public function download_template() {
		$this->autoRender = false; 
		$file = Router::url('/files/plantilla.xls', true);
		header("Content-disposition: attachment; filename=plantilla.xls");
		header("Content-type: application/octet-stream");
		readfile($file);
	}

	public function import() {
		if ($this->request->is('post')) {

			if (isset($this->request['data']['Product']['file']) AND $this->request['data']['Product']['file']['error'] === 0) {

				if (!in_array($this->request['data']['Product']['file']['type'], array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel', 'application/octet-stream'))) {
					$this->Session->setFlash('Extensión no permitida', 'flash');
					return;
				}

				App::uses('PHPExcel', 'Lib/PHPExcel');
				if (!class_exists('PHPExcel')) {
					throw new CakeException('Lib Class PHPExcel not found.');
					return;
				}
				PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
				$excel 									= PHPExcel_IOFactory::load($this->request['data']['Product']['file']['tmp_name']);
				$rows  									= $excel->getSheet()->toArray(null,true,true,false);
				foreach ($rows as $k => $v) {	
					$rows[$k] 							= array_filter($rows[$k]);
				}
				$rows 									= array_filter($rows);

				unset($rows[0]);

				// echo '<pre>';
				// print_r($rows);
				// echo '</pre>';
				// exit;

				foreach ($rows as $k => $v) {

					if (array_key_exists(7, $v)) {
						if ($Category = $this->Category->findByName($v[7])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[7];
							$this->Category->parent_category_id = 0;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(8, $v)) {
						if ($Category = $this->Category->findByName($v[8])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[8];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(9, $v)) {
						if ($Category = $this->Category->findByName($v[9])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[9];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(10, $v)) {
						if ($Category = $this->Category->findByName($v[10])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[10];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(11, $v)) {
						if ($Category = $this->Category->findByName($v[11])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[11];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(12, $v)) {
						if ($Category = $this->Category->findByName($v[12])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[12];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(13, $v)) {
						if ($Category = $this->Category->findByName($v[13])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[13];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(14, $v)) {
						if ($Category = $this->Category->findByName($v[14])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[14];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(15, $v)) {
						if ($Category = $this->Category->findByName($v[15])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[15];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (array_key_exists(16, $v)) {
						if ($Category = $this->Category->findByName($v[16])) {
							$parent_category_id = $Category['Category']['id'];
							$category_id 						= $parent_category_id;
						} else {
							$this->Category->create();
							$this->Category->name		 		= $v[16];
							$this->Category->parent_category_id = $parent_category_id;
							$this->Category->save($this->Category);
							$parent_category_id                 = $this->Category->getInsertID();
							$category_id 						= $parent_category_id;
						}
					}

					if (!$this->Product->findByDescription($v[0])) {
						$this->Product->create();

						if ($campaign = $this->Campaign->findByName($v[2])) {
							$this->Product->campaign_id	= $campaign['Campaign']['id'];
						}
						if ($brand = $this->Brand->findByName($v[5])) {
							$this->Product->brand_id 	= $brand['Brand']['id'];
						} else {
							$this->Brand->create();
							$this->Brand->name			= $v[5];
							$this->Brand->save($this->Brand);
							$this->Product->brand_id	= $this->Brand->getInsertID();
						}

						$this->Product->description = $v[0];
						$this->Product->code 		= $v[1];
						$this->Product->fsc 		= $v[3];
						$this->Product->price 		= $v[4];
						$this->Product->notes 		= $v[6];

						//var_dump($v[2], $campaign['Campaign']['id'], $v[5], $brand['Brand']['id']);

						$this->Product->category_id = $category_id;
						$this->Product->save($this->Product);
					}
				}	
				$this->Session->setFlash('Excel importado con éxito', 'flash');
				return;
			}
			$this->Session->setFlash('No seleccionó ningun archivo', 'flash');
		}
	}
	
}