<?php

class BrandsController extends AppController
{

    public $components = array('Paginator');

    public function beforeRender() {
        $this->set('Module', 'Marcas');
    }

    public function index()
    {
        $this->set('rows', $this->paginate());
    }

    public function create() {
        if ($this->request->is('post')) {
            $this->Brand->create();
            if ($this->Brand->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado', 'flash');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
        }
    }

    public function update($id = null) {
        $this->Brand->id = $id;
        if (!$this->Brand->exists()) {
            $this->Session->setFlash('Registro Invalido', 'flash');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Brand->save($this->request->data)) {
                $this->Session->setFlash('Registro guardado', 'flash');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
        } else {
            $this->request->data = $this->Brand->read(null, $id);
            $this->render('create');
        }
    }

    public function delete($id = null) {
        $this->Brand->id = $id;
        if (!$this->Brand->exists()) {
            throw new NotFoundException('Registro Invalido', 'flash');
        }
        if ($this->Brand->delete()) {
            $this->Session->setFlash('Registro Eliminado', 'flash');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('Regitro no Eliminado', 'flash');
        return $this->redirect(array('action' => 'index'));
    }
}