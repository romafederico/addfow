<?php
class ShopBrochuresController extends AppController {

	public $uses = array('ShopBrochure', 'ShopBrochureImage', 'ShopBrochureProduct', 'Product', 'Campaign', 'Brand', 'Category');
	
	public $components = array('Paginator', 'RequestHandler');

	public function beforeRender() {
		$this->set('Module', 'Catálogos');
	}

	public function products() {
		if ($this->request->is('ajax')) {
			$term = $this->request->query('term');
			if(!empty($term)) {
				$items = $this->Product->find('all', array(
						'conditions' => array('Product.description LIKE' => trim($term) . '%', 'Product.father_id' => null),
						'fields' => array('id', 'Product.description as label')
				));
			}
			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode(Set::extract('/Product/.', $items)));
		}
	}

	public function product() {
		if ($this->request->is('ajax')) {
			$product = $this->Product->find('first', array(
					'conditions' => array('Product.id' => $this->request->query['product_id'])
			));

			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode($product));
		}
	}

	public function brands()
	{
		if ($this->request->is('ajax')) {
			$term = $this->request->query('term');
			if (!empty($term)) {
				$items = $this->Brand->find('all', array(
						'conditions' => array('name LIKE' => trim($term) . '%'),
						'fields' => array('name as label')
				));
			}
			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode(Set::extract('/Brand/.', $items)));
		}
	}

	public function categories() {
		if ($this->request->is('ajax')) {
			$term = $this->request->query('term');
			if(!empty($term)) {
				$items = $this->Category->find('all', array(
						'conditions' => array('name LIKE' => trim($term) . '%'),
						'fields' => array('name as label')
				));
			}
			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode(Set::extract('/Category/.', $items)));
		}
	}

	private function _category_tree($category = null) {
		// TODO: veridicar por que ambos son null
		if (@is_null($this->request->query['category']) && is_null($category)) {
			return null;
		}

		if (!is_null($category)) {
			$this->request->query['category'] = $category;
		}

		$row = $this->Category->find('first', array(
				'conditions' => array('name' => $this->request->query['category'])
		));

		$tree = $row['Category']['name'];

		while(!empty($row)) {
			$row = $this->Category->findById($row['Category']['parent_category_id']);

			if ($row) {
				$tree = $row['Category']['name'] .' > ' . $tree;
			}
		}

		return $tree;
	}

	public function category_tree() {
		if ($this->request->is('ajax')) {

			$tree = $this->_category_tree();

			$this->autoRender = false;
			$this->response->type('json');
			$this->response->body(json_encode($tree));
		}
	}
	
	public function index() {
		$this->read();
	}

	public function create() {
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->ShopBrochure->id = $this->request->data['ShopBrochure']['id'];
			if (!$this->ShopBrochure->exists()) {
				$this->Session->setFlash('Registro Invalido', 'flash');
				$this->redirect(array('action' => 'index'));
			}
			$this->ShopBrochure->create();
			if ($this->ShopBrochure->save($this->request->data)) {
				$this->Session->setFlash('Registro guardado', 'flash');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
		} else {
			$this->ShopBrochure->create();
			$this->request->data = $this->ShopBrochure->save(array('name' => null));
			$this->set('Campaigns', $this->Campaign->find('list', array(
					'fields' => array('id', 'name'),
					'order' => array('end_date DESC')
			)));
		}
	}

	public function read() {
		$this->Paginator->settings = array('conditions'	=> array('ShopBrochure.name !=' => null));
		$this->set('rows', $this->paginate());
	}

	public function update($id = null) {
		$this->ShopBrochure->id = $id;
		if (!$this->ShopBrochure->exists()) {
			$this->Session->setFlash('Registro Invalido', 'flash');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ShopBrochure->save($this->request->data)) {
				$this->Session->setFlash('Registro guardado', 'flash');
				return $this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash('El registro no puede guardarse. Por favor, vuelva a intentarlo.', 'flash');
		} else {
			$this->request->data = $this->ShopBrochure->read(null, $id);
			$this->set('Campaigns', $this->Campaign->find('list', array(
					'fields' => array('id', 'name'),
					'order' => array('end_date DESC')
			)));
			$this->render('create');
		}
	}

	public function delete($id = null) {
		$this->ShopBrochure->id = $id;
		if (!$this->ShopBrochure->exists()) {
			throw new NotFoundException('Registro Invalido', 'flash');
		} else {
			$this->delete_folder(WWW_ROOT . 'images/brochure_'.$id);
			$this->ShopBrochure->delete();
			$this->Session->setFlash('Registro Eliminado', 'flash');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Regitro no Eliminado', 'flash');
		return $this->redirect(array('action' => 'index'));
	}

	private function delete_folder($path) {
	    if (is_dir($path) === true) {
	        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);
	        foreach ($files as $file) {
	            if (in_array($file->getBasename(), array('.', '..')) !== true) {
	                if ($file->isDir() === true) {
	                    rmdir($file->getPathName());
	                } else if (($file->isFile() === true) || ($file->isLink() === true)) {
	                    unlink($file->getPathname());
	                }
	            }
	        }
	        return rmdir($path);
	    } else if ((is_file($path) === true) || (is_link($path) === true)) {
	        return unlink($path);
	    }
	    return false;
	}

	public function files($id = null) {
		$this->autoRender = false;
		$this->layout = 'ajax';
		if ($this->request->is('post') AND $id) {
			$image = $this->request->data['ShopBrochureImage'];
			if ($image['error'] == 0) {
				$ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
				$name = md5(uniqid(mt_rand())) . '.' . $ext;

					if (!file_exists(WWW_ROOT . 'images/brochure_'.$id.'/original')) {
						mkdir(WWW_ROOT . 'images/brochure_'.$id.'/original', 0774, true);
					}
					if (!file_exists(WWW_ROOT . 'images/brochure_'.$id.'/small')) {
						mkdir(WWW_ROOT . 'images/brochure_'.$id.'/small', 0774, true);
					}
					if (!file_exists(WWW_ROOT . 'images/brochure_'.$id.'/products')) {
						mkdir(WWW_ROOT . 'images/brochure_'.$id.'/products', 0774, true);
					}

				if (move_uploaded_file($image['tmp_name'], WWW_ROOT . 'images/brochure_'.$id.'/original/' . $name)) {
					$order = $this->ShopBrochureImage->find('count', array(
						'conditions' => array('ShopBrochureImage.shop_brochure_id' => $id)
					));
					$this->ShopBrochureImage->create();
					$image_save = $this->ShopBrochureImage->save(array('shop_brochure_id' => $id, 'name' => $name, 'order' => $order));
					$info 					= new stdClass();
					$info->name 			= $name;
					$info->id 				= $image_save['ShopBrochureImage']['id'];
					$this->resize_small(WWW_ROOT . 'images/brochure_'.$id, $info);
					$info->type             = $image['type']; 
	                $info->ext              = $ext;
	                $info->url              = h(Router::url('/images/brochure_'.$id.'/original/'.$name, true));
	                $info->thumbnailUrl 	= h(Router::url('/images/brochure_'.$id.'/small/'.$name, true));
	                $info->deleteUrl        = h(Router::url('/shop_brochures/delete_file/'. $image_save['ShopBrochureImage']['id'], true));
	                $info->deleteType       = 'POST';
	                $info                   = array($info);
	                $result                 = new stdClass($info);
	                $result->files          = $info;
	                die(json_encode($result));
				}
			}
		} else if ($this->request->is('get') AND $id) {
			$data = array();
			$files = $this->ShopBrochureImage->find('all', array(
				'conditions' => array('shop_brochure_id' => $id),
				'order' => array('order' => 'asc'),
	        	)
	        );
            foreach($files as $k => $v) {
                $data[$k] = (object) 
                array(
                    'thumbnailUrl'  => h(Router::url('/images/brochure_'.$id.'/small/'    . $v['ShopBrochureImage']['name'], true)),
                    'url'           => h(Router::url('/images/brochure_'.$id.'/original/' . $v['ShopBrochureImage']['name'], true)),
                    'deleteUrl'     => h(Router::url('/shop_brochures/delete_file/' . $v['ShopBrochureImage']['id'], true)),
                    'name'          => $v['ShopBrochureImage']['name'],
                    'order'         => $v['ShopBrochureImage']['order'],
                    'id'            => $v['ShopBrochureImage']['id'],
                    'deleteType'    => 'POST'
                );
            }
            $data['files']          = $data;
            die(json_encode($data));
		}			
	}

	private function resize_small($route, $image) {
		$this->autoRender = false;
    	App::uses('WideImage', 'Lib/wideimage');
        $new_image = WideImage::load($route . '/original/' . $image->name);
        $new_image->resize(200, 200)->saveToFile($route . '/small/' . $image->name);
    }

    public function update_order() {
    	$this->autoRender = false;
    	foreach ($this->request['data']['order'] as $k => $v) {
    		$this->ShopBrochureImage->save(array('order' => $k, 'id' => $v));
    	}
	}

	public function delete_file($id = null) {
		$this->autoRender = false;
		$info          	  = new stdClass();
		$info->sucess     = false;
        if($this->request->is('ajax') AND $id) {
        	$this->ShopBrochureImage->id = $id;
        	if ($this->ShopBrochureImage->exists()) {
        		$file = $this->ShopBrochureImage->findById($id);
        		$name = $file['ShopBrochureImage']['name'];
        		$images_products = $this->ShopBrochureProduct->find('all', array('conditions' => array('shop_brochure_image_id' => $id)));
        		foreach ($images_products as $k => $v) {
        			@unlink(WWW_ROOT . 'images/brochure_'.$file['ShopBrochureImage']['shop_brochure_id'].'/products/' . $images_products[$k]['ShopBrochureProduct']['image']);
        		}
				@unlink(WWW_ROOT . 'images/brochure_'.$file['ShopBrochureImage']['shop_brochure_id'].'/original/' . $name);
				@unlink(WWW_ROOT . 'images/brochure_'.$file['ShopBrochureImage']['shop_brochure_id'].'/small/'    . $name);
            	if ($this->ShopBrochureImage->delete()) {
					$info->sucess  = true;
            	}
        	}
        }
        $info          = array($info);
		$result        = new stdClass();
		$result->files = $info;
		die(json_encode($result));
    }

    public function create_product() {
		if ($this->request->is('ajax') AND $this->request->is('post')) {

			if(!empty($this->request->data['ShopBrochureProduct']['image_product']['tmp_name'])){
				$image_product = $this->request->data['ShopBrochureProduct']['image_product'];
				$image_product_ext = strtolower(pathinfo($image_product['name'], PATHINFO_EXTENSION));
				$image_product_name = md5(uniqid(mt_rand())) . '.' . $image_product_ext;
				move_uploaded_file($image_product['tmp_name'], WWW_ROOT . 'images/brochure_'.$this->request->data['ShopBrochureProduct']['shop_brochure_id'].'/products/' . $image_product_name);
				$this->request->data['ShopBrochureProduct']['image_product'] = $image_product_name;
			} else {
				unset($this->request->data['ShopBrochureProduct']['image_product']);
			}

			$product = $this->Product->find('first', array(
					'conditions' => array(
						'Product.description LIKE' => $this->request->data['ShopBrochureProduct']['product'],
						'Product.father_id' => null
					)
			));

			$brand = $this->Brand->find('first', array(
					'conditions' => array('name LIKE' => $this->request->data['ShopBrochureProduct']['brand'])
			));

			$this->request->data['ShopBrochureProduct']['category'] = end(explode(" > ", $this->request->data['ShopBrochureProduct']['category']));

			$category = $this->Category->find('first', array(
					'conditions' => array('name LIKE' => $this->request->data['ShopBrochureProduct']['category'])
			));

			$this->request->data['ShopBrochureProduct']['product_id'] = $product['Product']['id'];
			$this->request->data['ShopBrochureProduct']['brand_id'] = $brand['Brand']['id'];
			$this->request->data['ShopBrochureProduct']['category_id'] = $category['Category']['id'];

			$image = $this->request->data['ShopBrochureProduct']['coords'];
			$image['name'] = $this->ShopBrochureImage->findById($this->request->data['ShopBrochureProduct']['shop_brochure_image_id'])['ShopBrochureImage']['name'];
			
			$ext = strtolower(pathinfo(WWW_ROOT . 'images/brochure_' . $this->request->data['ShopBrochureProduct']['shop_brochure_id'].'/original/'.$image['name'], PATHINFO_EXTENSION));
			$name = md5(uniqid(mt_rand())) . '.' . $ext;
			$image['new_name'] = $name;

			$this->crop(WWW_ROOT . 'images/brochure_' . $this->request->data['ShopBrochureProduct']['shop_brochure_id'], $image);
			$this->request->data['ShopBrochureProduct']['coords'] = json_encode($this->request->data['ShopBrochureProduct']['coords']);
			$this->request->data['ShopBrochureProduct']['image'] = $image['new_name'];
			$this->ShopBrochureProduct->create();
			if ($this->ShopBrochureProduct->save($this->request->data)) {
				$products = $this->Product->find('all', array('conditions' => array('father_id' => $product['Product']['id']), 'order' => 'Product.id'));
				if (!empty($products)) {
					$father_id = $this->ShopBrochureProduct->id;
					foreach($products as $product) {
						$this->request->data['ShopBrochureProduct']['father_id'] = $father_id;
						$this->request->data['ShopBrochureProduct']['product_id'] = $product['Product']['id'];
						$this->request->data['ShopBrochureProduct']['price'] = $product['Product']['price'];
						$this->request->data['ShopBrochureProduct']['code'] = $product['Product']['code'];
						$this->request->data['ShopBrochureProduct']['fsc'] = $product['Product']['fsc'];

						$this->ShopBrochureProduct->create();
						$this->ShopBrochureProduct->save($this->request->data);
					}
				}
				$this->autoRender = false;
				die(json_encode(array('result' => 1)));
			} else {
				die(json_encode(array('result' => 0)));
			}
		} else {
			$this->layout= 'ajax';
			$this->render('products');
		}
	}

	private function crop($route, $image) {
		$this->autoRender = false;
        App::uses('WideImage', 'Lib/wideimage');
        $new_image = WideImage::load($route . '/original/' .$image['name']);
        $new_image
        ->crop(round($image['x']), round($image['y']), round($image['w']), round($image['h']))
        ->resize(100, 100)
        ->saveToFile($route . '/products/' .$image['new_name']);
        $this->padding($route, $image);
    }

    private function padding($route, $image) {
    	$info 			= getimagesize($route . '/products/' .$image['new_name']); 
    	$image['w'] 	= $info[0];
    	$image['h'] 	= $info[1];
        $height         = ($image['w'] / 100) * 100;
        $width          = ($image['h'] / 100) * 100;
        App::uses('WideImage', 'Lib/wideimage');
        $new_image = WideImage::load($route . '/products/' .$image['new_name']);
        $color          = $new_image->allocateColor(255, 255, 255);
        if($height > $image['h']) {
            $diferencia = $height - $image['h'];
            $new_image->resizeCanvas('100%', '100%+'.$diferencia, 0, $diferencia/2, $color)
            ->saveToFile($route . '/products/' .$image['new_name']);
        } else {
            $diferencia = $width  - $image['w'];
            $new_image->resizeCanvas('100%+'.$diferencia, '100%', $diferencia/2, 0, $color)
            ->saveToFile($route . '/products/' .$image['new_name']);
        }
    }

	public function read_product($id = null) {
		$data = array();
		$this->autoRender = false;
        if($this->request->is('ajax') AND $id) {
        	$this->Product->id = $id;
        	if ($this->Product->exists()) {
        		$data = $this->Product->findById($id);
        	}
        }
        die(json_encode($data));
	}

	public function read_products($id = null) {
		$data = array();
		$this->autoRender = false;
        if($this->request->is('ajax') AND $id) {
        	$products = $this->ShopBrochureProduct->find('all', array('conditions' => array('shop_brochure_image_id' => $id, 'ShopBrochureProduct.father_id' => null)));
        	foreach ($products as $k => $v) {
        		$products[$k]['ShopBrochureProduct']['coords'] = json_decode($v['ShopBrochureProduct']['coords']);
        	}
        	$data['products'] = $products;
        }
		//var_dump($data); die;
        die(json_encode($data));
	}
	
	public function update_product_form($id = null) {
		
			if(!empty($this->request->data['ShopBrochureProduct']['image_product']['tmp_name'])){
				$image_product = $this->request->data['ShopBrochureProduct']['image_product'];
				$image_product_ext = strtolower(pathinfo($image_product['name'], PATHINFO_EXTENSION));
				$image_product_name = md5(uniqid(mt_rand())) . '.' . $image_product_ext;
				move_uploaded_file($image_product['tmp_name'], WWW_ROOT . 'images/brochure_'.$this->request->data['ShopBrochureProduct']['shop_brochure_id'].'/products/' . $image_product_name);
				$this->request->data['ShopBrochureProduct']['image_product'] = $image_product_name;
			} else {
				unset($this->request->data['ShopBrochureProduct']['image_product']);
			}

			$product = $this->Product->find('first', array(
					'conditions' => array(
						'Product.description LIKE' => $this->request->data['ShopBrochureProduct']['product'],
						'Product.father_id' => null
					)
			));

			$brand = $this->Brand->find('first', array(
					'conditions' => array('name LIKE' => $this->request->data['ShopBrochureProduct']['brand'])
			));

			$this->request->data['ShopBrochureProduct']['category'] = end(explode(" > ", $this->request->data['ShopBrochureProduct']['category']));

			$category = $this->Category->find('first', array(
					'conditions' => array('name LIKE' => $this->request->data['ShopBrochureProduct']['category'])
			));

			$this->request->data['ShopBrochureProduct']['product_id'] = $product['Product']['id'];
			$this->request->data['ShopBrochureProduct']['brand_id'] = $brand['Brand']['id'];
			$this->request->data['ShopBrochureProduct']['category_id'] = $category['Category']['id'];

			$this->ShopBrochureProduct->id = $id;
			if ($this->ShopBrochureProduct->exists()) {
				if ($this->ShopBrochureProduct->save($this->request->data)) {
					die(json_encode(array('result' => 1)));
				} else {
					die(json_encode(array('result' => 0)));
				}
			}
	}

	public function update_product($id = null) {
		if ($this->request->is('ajax') AND $this->request->is('post')) {
		
			if(isset($this->request->data['ShopBrochureProduct']['image_product'])){
				$image_product = $this->request->data['ShopBrochureProduct']['image_product'];
				$image_product_ext = strtolower(pathinfo($image_product['name'], PATHINFO_EXTENSION));
				$image_product_name = md5(uniqid(mt_rand())) . '.' . $image_product_ext;
				move_uploaded_file($image_product['tmp_name'], WWW_ROOT . 'images/brochure_'.$this->request->data['ShopBrochureProduct']['shop_brochure_id'].'/products/' . $image_product_name);
				$this->request->data['ShopBrochureProduct']['image_product'] = $image_product_name;
			}

			$product = $this->Product->find('first', array(
					'conditions' => array('description LIKE' => $this->request->data['ShopBrochureProduct']['product'])
			));

			$brand = $this->Brand->find('first', array(
					'conditions' => array('name LIKE' => $this->request->data['ShopBrochureProduct']['brand'])
			));

			$category = $this->Category->find('first', array(
					'conditions' => array('name LIKE' => $this->request->data['ShopBrochureProduct']['category'])
			));

			$this->request->data['ShopBrochureProduct']['product_id'] = $product['Product']['id'];
			$this->request->data['ShopBrochureProduct']['brand_id'] = $brand['Brand']['id'];
			$this->request->data['ShopBrochureProduct']['category_id'] = $category['Category']['id'];

			$this->ShopBrochureProduct->id = $id;
			if ($this->ShopBrochureProduct->exists()) {
				if ($this->ShopBrochureProduct->save($this->request->data)) {
					die(json_encode(array('result' => 1)));
				} else {
					die(json_encode(array('result' => 0)));
				}
			}
		} else {
			$this->request->data = $this->ShopBrochureProduct->read(null, $id);
			//var_dump($this->request->data);
			$this->request->data['ShopBrochureProduct']['product'] = $this->request->data['Product']['description'];
			$this->request->data['ShopBrochureProduct']['brand'] = $this->request->data['Brand']['name'];
			$this->request->data['ShopBrochureProduct']['category'] = $this->_category_tree($this->request->data['Category']['name']);
			//$products = $this->Product->find('list', array('fields' => array('id', 'description')));
			//$this->set('products', $products);
			$this->layout= 'ajax';
			$this->render('products');
		}
	}

	public function delete_product($id = null) {
		$data['result'] = null;
		$data['message'] = null;
		$this->autoRender = false;
        if($this->request->is('ajax') AND $id) {
        	$this->ShopBrochureProduct->id = $id;
			if (!$this->ShopBrochureProduct->exists()) {
				$data['message'] = 'Regitro Invalido';
			} else {
				$ShopBrochureProduct = $this->ShopBrochureProduct->findById($id)['ShopBrochureProduct'];
				@unlink(WWW_ROOT . 'images/brochure_' . $ShopBrochureProduct['shop_brochure_id'] . '/products/' . $ShopBrochureProduct['image']);

				$father_id = $ShopBrochureProduct['id'];
				if ($this->ShopBrochureProduct->delete()) {
					$ShopBrochureProducts = $this->ShopBrochureProduct->find('all', array(
						'conditions' => array('ShopBrochureProduct.father_id' => $father_id)
					));

					if (!empty($ShopBrochureProducts)) {
						foreach($ShopBrochureProducts as $ShopBrochureProduct) {
							$this->ShopBrochureProduct->delete($ShopBrochureProduct['ShopBrochureProduct']['id']);
						}
					}
					$data['result'] = 1;
				}
			}
        }
		die(json_encode($data));
	}
	
}