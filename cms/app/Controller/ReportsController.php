<?php

class ReportsController extends AppController
{
    public $uses = array('Report', 'ReportDetail', 'Campaign', 'UserCart', 'UserCartDetail', 'ShopBrochureProduct', 'Product', 'ShopBrochure', 'CampaignShopBrochure');

    public $components = array('Paginator');

    public function beforeRender()
    {
        $this->set('Module', 'Reportes');
    }

    public function pago()
    {
        $this->UserCart->Behaviors->load('Containable');

        $this->UserCart->contain(array(
                'UserCartDetail' => array(
                    'ShopBrochureProduct' => array(
                        'Product',
                        'ShopBrochure' => array(
                            'CampaignShopBrochure' => 'Campaign'
                        )
                    )
                )
            )
        );

        $UserCarts = $this->UserCart->find('all', array(
            'conditions' => array(
                'user_cart_status_flags' => 1,
                'not' => array('purchase_date' => null, 'order_date' => null),
            )
        ));

        $reports = array();

        foreach ($UserCarts as $UserCart) {
            foreach ($UserCart['UserCartDetail'] as $UserCartDetail) {
                if(!isset($UserCartDetail['ShopBrochureProduct']['ShopBrochure'])) continue;
                $end_date = DateTime::createFromFormat('d/m/Y', $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['end_date']);
                $today = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));

                if ($end_date->getTimestamp() >= $today->getTimestamp()) {
                    $campaign_id = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['id'];

                    if (!isset($reports[$campaign_id]['resellers'])) $reports[$campaign_id]['resellers'] = [];
                    if (!isset($reports[$campaign_id]['clients'])) $reports[$campaign_id]['clients'] = [];
                    if (!isset($reports[$campaign_id]['billing'])) $reports[$campaign_id]['billing'] = 0;
                    if (!isset($reports[$campaign_id]['clients'])) $reports[$campaign_id]['clients'] = 0;
                    if (!isset($reports[$campaign_id]['quantity'])) $reports[$campaign_id]['quantity'] = 0;

                    $reports[$campaign_id]['campaign_id'] = $campaign_id;
                    $reports[$campaign_id]['campaign_name'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['name'];

                    if (!in_array($UserCart['UserCart']['reseller_user_id'], $reports[$campaign_id]['resellers'])) {
                        $reports[$campaign_id]['resellers'][] = $UserCart['UserCart']['reseller_user_id'];
                    }

                    if (!in_array($UserCart['UserCart']['user_id'], $reports[$campaign_id]['clients'])) {
                        $reports[$campaign_id]['clients'][] = $UserCart['UserCart']['user_id'];
                    }

                    $reports[$campaign_id]['billing'] += $UserCartDetail['quantity'] * $UserCartDetail['ShopBrochureProduct']['price'];
                    $reports[$campaign_id]['quantity'] += $UserCartDetail['quantity'];
                }
            }
        }

        foreach ($reports as $report) {
            $this->Report->deleteAll(array('campaign_id' => $report['campaign_id']));

            $report['resellers'] = count($report['resellers']);
            $report['clients'] = count($report['clients']);

            $this->Report->create();
            $this->Report->save($report);
        }

        $this->Paginator->settings = array(
            'conditions' => array('historic' => false)
        );

        $this->set('rows', $this->paginate());
    }

    public function free()
    {
        $this->UserCart->Behaviors->load('Containable');

        $this->UserCart->contain(array(
                'UserCartDetail' => array(
                    'ShopBrochureProduct' => array(
                        'Product',
                        'ShopBrochure' => array(
                            'CampaignShopBrochure' => 'Campaign'
                        )
                    )
                )
            )
        );

        $UserCarts = $this->UserCart->find('all', array(
            'conditions' => array(
                'user_cart_status_flags' => 1,
                'not' => array('purchase_date' => null, 'order_date' => null),
            )
        ));

        $reports = array();

        foreach ($UserCarts as $UserCart) {
            foreach ($UserCart['UserCartDetail'] as $UserCartDetail) {
                if(!isset($UserCartDetail['ShopBrochureProduct']['ShopBrochure'])) continue;
                $end_date = DateTime::createFromFormat('d/m/Y', $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['end_date']);
                $today = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));

                if ($end_date->getTimestamp() >= $today->getTimestamp()) {
                    $campaign_id = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['id'];

                    if (!isset($reports[$campaign_id]['resellers'])) $reports[$campaign_id]['resellers'] = [];
                    if (!isset($reports[$campaign_id]['clients'])) $reports[$campaign_id]['clients'] = [];
                    if (!isset($reports[$campaign_id]['billing'])) $reports[$campaign_id]['billing'] = 0;
                    if (!isset($reports[$campaign_id]['clients'])) $reports[$campaign_id]['clients'] = 0;
                    if (!isset($reports[$campaign_id]['quantity'])) $reports[$campaign_id]['quantity'] = 0;

                    $reports[$campaign_id]['campaign_id'] = $campaign_id;
                    $reports[$campaign_id]['campaign_name'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['name'];

                    if (!in_array($UserCart['UserCart']['reseller_user_id'], $reports[$campaign_id]['resellers'])) {
                        $reports[$campaign_id]['resellers'][] = $UserCart['UserCart']['reseller_user_id'];
                    }

                    if (!in_array($UserCart['UserCart']['user_id'], $reports[$campaign_id]['clients'])) {
                        $reports[$campaign_id]['clients'][] = $UserCart['UserCart']['user_id'];
                    }

                    $reports[$campaign_id]['billing'] += $UserCartDetail['quantity'] * $UserCartDetail['ShopBrochureProduct']['price'];
                    $reports[$campaign_id]['quantity'] += $UserCartDetail['quantity'];
                }
            }
        }

        foreach ($reports as $report) {
            $this->Report->deleteAll(array('campaign_id' => $report['campaign_id']));

            $report['resellers'] = count($report['resellers']);
            $report['clients'] = count($report['clients']);

            $this->Report->create();
            $this->Report->save($report);
        }

        $this->Paginator->settings = array(
            'conditions' => array('historic' => false)
        );

        $this->set('rows', $this->paginate());
    }

    public function details()
    {
        $this->UserCart->Behaviors->load('Containable');

        $this->UserCart->contain(array(
                'UserCartDetail' => array(
                    'ShopBrochureProduct' => array(
                        'Product',
                        'ShopBrochure' => array(
                            'CampaignShopBrochure' => 'Campaign'
                        )
                    )
                )
            )
        );

        $UserCarts = $this->UserCart->find('all', array(
            'conditions' => array(
                'user_cart_status_flags' => 1,
                'not' => array('purchase_date' => null, 'order_date' => null)
            )
        ));

        $reports = array();

        foreach ($UserCarts as $UserCart) {
            foreach ($UserCart['UserCartDetail'] as $UserCartDetail) {
                if(!isset($UserCartDetail['ShopBrochureProduct']['ShopBrochure'])) continue;

                $campaign_id = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['id'];
                if ($this->request->pass[0] == $campaign_id) {
                    $shop_brochure_id = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['id'];
                    $product_code = $UserCartDetail['ShopBrochureProduct']['Product']['code'];

                    $id = $campaign_id.$shop_brochure_id.$product_code;

                    if(isset($reports[$id])) {
                        $reports[$id]['quantity'] += $UserCartDetail['quantity'];
                    } else {
                        $reports[$id]['campaign_id'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['id'];
                        $reports[$id]['campaign_name'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['name'];

                        $reports[$id]['shop_brochure_id'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['id'];
                        $reports[$id]['shop_brochure_name'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['name'];

                        $reports[$id]['fsc'] = $UserCartDetail['ShopBrochureProduct']['Product']['fsc'];
                        $reports[$id]['code'] = $UserCartDetail['ShopBrochureProduct']['Product']['code'];
                        $reports[$id]['description'] = $UserCartDetail['ShopBrochureProduct']['Product']['description'];

                        $reports[$id]['quantity'] = $UserCartDetail['quantity'];
                    }
                }
            }
        }

        $this->ReportDetail->deleteAll(array('campaign_id' => $this->request->pass[0]));

        foreach ($reports as $report) {
            $this->ReportDetail->create();
            $this->ReportDetail->save($report);
        }

        $this->Paginator->settings = array(
            'ReportDetail' => array(
                'order' => array(
                    //'campaign_id' => 'DESC',
                    //'shop_brochure_id' => 'DESC',
                    'quantity' => 'DESC'
                ),
                'conditions' => array(
                    'campaign_id' => $this->request->pass[0]
                )
            )
        );

        $this->set('rows', $this->paginate('ReportDetail'));
    }

    public function historic()
    {
        $this->UserCart->Behaviors->load('Containable');

        $this->UserCart->contain(array(
                'UserCartDetail' => array(
                    'ShopBrochureProduct' => array(
                        'Product',
                        'ShopBrochure' => array(
                            'CampaignShopBrochure' => 'Campaign'
                        )
                    )
                )
            )
        );

        $UserCarts = $this->UserCart->find('all', array(
            'conditions' => array(
                'user_cart_status_flags' => 1,
                'not' => array('purchase_date' => null, 'order_date' => null),
            )
        ));

        $reports = array();

        foreach ($UserCarts as $UserCart) {
            foreach ($UserCart['UserCartDetail'] as $UserCartDetail) {
                if(!isset($UserCartDetail['ShopBrochureProduct']['ShopBrochure'])) continue;
                $end_date = DateTime::createFromFormat('d/m/Y', $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['end_date']);
                $today = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));

                if ($end_date->getTimestamp() < $today->getTimestamp()) {
                    $campaign_id = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['id'];

                    if (!isset($reports[$campaign_id]['resellers'])) $reports[$campaign_id]['resellers'] = [];
                    if (!isset($reports[$campaign_id]['clients'])) $reports[$campaign_id]['clients'] = [];
                    if (!isset($reports[$campaign_id]['billing'])) $reports[$campaign_id]['billing'] = 0;
                    if (!isset($reports[$campaign_id]['clients'])) $reports[$campaign_id]['clients'] = 0;
                    if (!isset($reports[$campaign_id]['quantity'])) $reports[$campaign_id]['quantity'] = 0;

                    $reports[$campaign_id]['campaign_id'] = $campaign_id;
                    $reports[$campaign_id]['campaign_name'] = $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['name'];

                    if (!in_array($UserCart['UserCart']['reseller_user_id'], $reports[$campaign_id]['resellers'])) {
                        $reports[$campaign_id]['resellers'][] = $UserCart['UserCart']['reseller_user_id'];
                    }

                    if (!in_array($UserCart['UserCart']['user_id'], $reports[$campaign_id]['clients'])) {
                        $reports[$campaign_id]['clients'][] = $UserCart['UserCart']['user_id'];
                    }

                    $reports[$campaign_id]['billing'] += $UserCartDetail['quantity'] * $UserCartDetail['ShopBrochureProduct']['price'];
                    $reports[$campaign_id]['quantity'] += $UserCartDetail['quantity'];
                }
            }
        }

        foreach ($reports as $report) {
            $this->Report->deleteAll(array('campaign_id' => $report['campaign_id']));

            $report['resellers'] = count($report['resellers']);
            $report['clients'] = count($report['clients']);
            $report['historic'] = true;

            $this->Report->create();
            $this->Report->save($report);
        }

        $this->Paginator->settings = array(
            'conditions' => array('historic' => true)
        );

        $this->set('rows', $this->paginate());
    }

    public function excel (){
        $this->UserCart->Behaviors->load('Containable');

        $this->UserCart->contain(array(
                'UserCartDetail' => array(
                    'ShopBrochureProduct' => array(
                        'Product',
                        'ShopBrochure' => array(
                            'CampaignShopBrochure' => 'Campaign'
                        )
                    )
                )
            )
        );

        $UserCarts = $this->UserCart->find('all', array(
            'conditions' => array(
                'user_cart_status_flags' => 1,
                'not' => array('purchase_date' => null, 'order_date' => null)
            )
        ));

        $reports = array();

        foreach ($UserCarts as $UserCart) {
            foreach ($UserCart['UserCartDetail'] as $UserCartDetail) {
                if ($this->request->pass[0] == $UserCartDetail['ShopBrochureProduct']['ShopBrochure']['CampaignShopBrochure'][0]['Campaign']['id']) {
                    $user_cart_detail_id = $UserCartDetail['id'];

                    $reports[$user_cart_detail_id]['fsc'] = $UserCartDetail['ShopBrochureProduct']['Product']['fsc'];
                    $reports[$user_cart_detail_id]['code'] = $UserCartDetail['ShopBrochureProduct']['Product']['code'];
                    $reports[$user_cart_detail_id]['description'] = $UserCartDetail['ShopBrochureProduct']['Product']['description'];

                    $reports[$user_cart_detail_id]['quantity'] = $UserCartDetail['quantity'];
                }
            }
        }

        //var_dump($reports); die;

        App::uses('PHPExcel', 'Lib/PHPExcel');

        $this->set('reports', $reports);
    }
}