<?php

class Product extends AppModel
{
    public $belongsTo = array(
        'Campaign' => array(),
        'Brand' => array(),
        'Category' => array(),
    );

    public $hasMany = array(
        'ProductVersion' => array(
            'dependent'=> true,
        ),
    );
}