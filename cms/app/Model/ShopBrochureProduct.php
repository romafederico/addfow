<?php

class ShopBrochureProduct extends AppModel
{
    //public $actsAs = array('Containable');

    public $belongsTo = array(
        'Product' => array(
            'foreignKey' => 'product_id'
        ),
        'ShopBrochure' => array(
            'foreignKey' => 'shop_brochure_id'
        ),
        'Brand' => array(),
        'Category' => array(),
    );

}