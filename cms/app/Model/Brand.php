<?php

class Brand extends AppModel
{
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A email is required'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Name already taken'
            ),
        )
    );
}